package classign.election.kingvar.come.electionclasssign.injector.modules;

import classign.election.kingvar.come.electionclasssign.injector.PerFragment;
import classign.election.kingvar.come.electionclasssign.module.base.IBasePresenter;
import classign.election.kingvar.come.electionclasssign.module.main.studentmien.schoolreward.SchoolRewardFragment;
import classign.election.kingvar.come.electionclasssign.module.main.studentmien.schoolreward.SchoolRewardPresenter;
import dagger.Module;
import dagger.Provides;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/29 0029 17:24
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */
@Module
public class SchoolRewardModule {
    private final SchoolRewardFragment starFragment;
    private final String mStudentid;
    private final String mType;
    public SchoolRewardModule(SchoolRewardFragment starFragment,String mType, String studentid) {
        this.starFragment = starFragment;
        this.mType = mType;
        this.mStudentid = studentid;

    }

    @PerFragment
    @Provides
    public IBasePresenter provideMainPresenter(){
        return new SchoolRewardPresenter(starFragment,mType,mStudentid);
    }
}
