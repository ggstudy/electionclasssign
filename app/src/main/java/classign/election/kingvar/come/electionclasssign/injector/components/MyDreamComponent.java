package classign.election.kingvar.come.electionclasssign.injector.components;


import classign.election.kingvar.come.electionclasssign.injector.PerFragment;
import classign.election.kingvar.come.electionclasssign.module.main.studentmien.mydream.MyDreamFragment;
import classign.election.kingvar.come.electionclasssign.injector.modules.MyDreamModule;
import dagger.Component;

/**
 * Created by long on 2016/8/19.
 */
@PerFragment
@Component(dependencies = ApplicationComponent.class, modules = {MyDreamModule.class})
public interface MyDreamComponent {
  void inject(MyDreamFragment fragment);
}