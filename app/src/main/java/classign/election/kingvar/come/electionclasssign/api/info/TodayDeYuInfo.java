package classign.election.kingvar.come.electionclasssign.api.info;

import java.util.List;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/5/25 0025 17:50
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public class TodayDeYuInfo {

    /**
     * date : 2017-05-25
     * total : 10
     * qinjia : 0
     * code : 0
     * data : [{"reason":"学习氛围好!","num":4,"time":1495641600000,"type":0,"remarks":"测试加分"},{"reason":"实到","num":1,"time":1495641600000,"type":1,"remarks":""}]
     * classStu : 6
     */

    private String date;
    private int total;
    private int qinjia;
    private int code;
    private int classStu;
    private List<DataEntity> data;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getQinjia() {
        return qinjia;
    }

    public void setQinjia(int qinjia) {
        this.qinjia = qinjia;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getClassStu() {
        return classStu;
    }

    public void setClassStu(int classStu) {
        this.classStu = classStu;
    }

    public List<DataEntity> getData() {
        return data;
    }

    public void setData(List<DataEntity> data) {
        this.data = data;
    }

    public static class DataEntity {
        /**
         * reason : 学习氛围好!
         * num : 4
         * time : 1495641600000
         * type : 0
         * remarks : 测试加分
         */

        private String reason;
        private int num;
        private long time;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        private String title;
        private String remarks;

        public String getReason() {
            return reason;
        }

        public void setReason(String reason) {
            this.reason = reason;
        }

        public int getNum() {
            return num;
        }

        public void setNum(int num) {
            this.num = num;
        }

        public long getTime() {
            return time;
        }

        public void setTime(long time) {
            this.time = time;
        }

        public String getRemarks() {
            return remarks;
        }

        public void setRemarks(String remarks) {
            this.remarks = remarks;
        }
    }
}
