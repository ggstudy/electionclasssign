package classign.election.kingvar.come.electionclasssign.injector.components;


import classign.election.kingvar.come.electionclasssign.injector.PerFragment;
import classign.election.kingvar.come.electionclasssign.injector.modules.AttendanceRecoderEightModule;
import classign.election.kingvar.come.electionclasssign.module.main.moraleducation.eight.TodayAttendanceEightFragment;
import dagger.Component;

/**
 * Created by long on 2016/8/19.
 */
@PerFragment
@Component(dependencies = ApplicationComponent.class, modules = {AttendanceRecoderEightModule.class})
public interface AttendanceRecoderEightComponent {
  void inject(TodayAttendanceEightFragment fragment);
}