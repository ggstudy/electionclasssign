package classign.election.kingvar.come.electionclasssign.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import classign.election.kingvar.come.electionclasssign.R;

/**
 * 通知(班级,学校)
 * Created by Administrator on 2016/12/22 0022.
 */

public class ListAdapter extends BaseAdapter {
    private LayoutInflater mInflater;
    Context context;
    List timeList;
    List list;
    int type;  //type = 1 (为左边学校的通知) type = 2(为右边 班级的通知)

    public ListAdapter(Context context, List list, List timeList, int type) {
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        this.type = type;
        this.list = list;
        this.timeList = timeList;
    }

    @Override
    public int getCount() {
        if (list.size() <= 3) {
            return list.size();
        } else {
            return Integer.MAX_VALUE;
        }
    }

    @Override
    public Object getItem(int position) {
        return list.get(position % list.size());
    }

    @Override
    public long getItemId(int position) {
        return position % list.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHoler viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHoler();
            convertView = mInflater.inflate(R.layout.school_resource_item, null);
            viewHolder.tvText = (TextView) convertView.findViewById(R.id.tv_school_resoure_item);
            viewHolder.tv_time = (TextView) convertView.findViewById(R.id.tv_time);
            viewHolder.tv_empty = (TextView) convertView.findViewById(R.id.tv_empty);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHoler) convertView.getTag();
        }
        String count = list.get(position % list.size()) + "";
        viewHolder.tv_time.setText(timeList.get(position%timeList.size())+"");
        if (type == 1) {
            if (count.length() > 25) {
                viewHolder.tvText.setText(count.substring(0, 25) + "...");
            } else {
                viewHolder.tvText.setText(count);

            }
        } else if (type == 2) {
            viewHolder.tv_empty.setVisibility(View.VISIBLE);
            if (count.length() > 20) {
                viewHolder.tvText.setText(count.substring(0, 20) + "...");
            } else {
                viewHolder.tvText.setText(count);

            }

        }
        return convertView;
    }

    public void setDateList(List list, List timeList, int type) {
        this.list = list;
        this.timeList = timeList;
        this.type = type;
        notifyDataSetChanged();
    }

    static class ViewHoler {
        TextView tvText;
        TextView tv_time;
        TextView tv_empty;  //填充空白
    }
}
