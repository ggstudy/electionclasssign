package classign.election.kingvar.come.electionclasssign.injector.components;


import classign.election.kingvar.come.electionclasssign.injector.PerFragment;
import classign.election.kingvar.come.electionclasssign.module.main.studentmien.schoolreward.SchoolRewardFragment;
import classign.election.kingvar.come.electionclasssign.injector.modules.SchoolRewardModule;
import dagger.Component;

/**
 * Created by long on 2016/8/19.
 */
@PerFragment
@Component(dependencies = ApplicationComponent.class, modules = {SchoolRewardModule.class})
public interface SchooolRewardComponent {
  void inject(SchoolRewardFragment fragment);
}