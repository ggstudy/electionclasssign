package classign.election.kingvar.come.electionclasssign.injector.components;


import classign.election.kingvar.come.electionclasssign.injector.PerFragment;
import classign.election.kingvar.come.electionclasssign.injector.modules.AttendanceRecoderFourModule;
import classign.election.kingvar.come.electionclasssign.module.main.moraleducation.four.TodayAttendanceFourFragment;
import dagger.Component;

/**
 * Created by long on 2016/8/19.
 */
@PerFragment
@Component(dependencies = ApplicationComponent.class, modules = {AttendanceRecoderFourModule.class})
public interface AttendanceRecoderFourComponent {
  void inject(TodayAttendanceFourFragment fragment);
}