package classign.election.kingvar.come.electionclasssign.module.main.classmien.classinfo;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import classign.election.kingvar.come.electionclasssign.R;
import classign.election.kingvar.come.electionclasssign.activity.OfficeTeacherActivity;
import classign.election.kingvar.come.electionclasssign.api.info.ClassInfo;
import classign.election.kingvar.come.electionclasssign.base.BaseProtocol;
import classign.election.kingvar.come.electionclasssign.injector.components.DaggerClassInfoComponent;
import classign.election.kingvar.come.electionclasssign.injector.modules.ClassInfoModule;
import classign.election.kingvar.come.electionclasssign.module.base.BaseFragment;
import classign.election.kingvar.come.electionclasssign.module.base.IBasePresenter;
import classign.election.kingvar.come.electionclasssign.tools.bean.User;
import classign.election.kingvar.come.electionclasssign.tools.utils.TypeFaceUtil;

/**
 * 班级信息fragment
 * Created by Administrator on 2016/12/21 0021.
 */

public class ClassInfoFragment extends BaseFragment<IBasePresenter> implements View.OnClickListener, IClassInfoView {
    private static final String TAG = "ClassInfoFragment";
    private static final String IS_BIG = "is_big";
    @BindView(R.id.tv_class_motto_content)
    TextView tv_class_motto_content; //班级的座右铭

    @BindView(R.id.tv_class_leader_one)
    TextView tv_class_leader_one; //班主任

    @BindView(R.id.tv_class_leader_two)
    TextView tv_class_leader_two; //副班主任

    @BindView(R.id.tv_class_leader_three)
    TextView tv_class_leader_three; //班长

    @BindView(R.id.iv_class_picture)
    ImageView iv_class_picture; //班级集体照

    @BindView(R.id.relative_class_committee)
    RelativeLayout relative_class_committee; //班委会

    @BindView(R.id.tv_class_committee)
    TextView tv_class_committee;

    @BindView(R.id.relative_take_office_teacher)
    RelativeLayout relative_take_office_teacher; //任课老师

    @BindView(R.id.tv_take_office_teacher)
    TextView tv_take_office_teacher;

    private String mIsBig;

    public static ClassInfoFragment newInstance(String mIsBig) {
        ClassInfoFragment classInfoFragment = new ClassInfoFragment();
        Bundle bundle = new Bundle();
        bundle.putString(IS_BIG, mIsBig);
        classInfoFragment.setArguments(bundle);
        return classInfoFragment;

    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.fragment_class_info;
    }

    @Override
    protected void initInjector() {
        if (getArguments() != null) {
            mIsBig = getArguments().getString(IS_BIG);
        }
        DaggerClassInfoComponent.builder()
                .applicationComponent(getAppComponent())
                .classInfoModule(new ClassInfoModule(this, User.getClassId(mContext)))
                .build()
                .inject(this);
    }

    @Override
    protected void initViews() {
        if (mIsBig != null && "isBig".equals(mIsBig)) {
            setTextSize();
        }
        tv_class_motto_content.setTypeface(TypeFaceUtil.initTypeface(mContext));//设置字体
        tv_class_leader_one.setText("班主任 :" + User.getClassTeacherOne(mContext));
        tv_class_leader_two.setText("副班主任 :" + User.getClassTeacherTwo(mContext));
        tv_class_leader_three.setText("班长 :" + User.getClassMonitor(mContext));
        relative_class_committee.setOnClickListener(this);
        relative_take_office_teacher.setOnClickListener(this);
    }

    /**
     * 设置字体大小
     */
    private void setTextSize() {
        tv_class_leader_one.setTextSize(25);
        tv_class_leader_two.setTextSize(25);
        tv_class_leader_three.setTextSize(25);
        tv_class_committee.setTextSize(23);
        tv_take_office_teacher.setTextSize(23);
    }

    @Override
    public void updateViews(boolean isRefresh) {
        Log.i(TAG, "updateViews: mPresenter = " + mPresenter);
//        mPresenter.getData(isRefresh);
    }

    @Override
    public void onStart() {
        super.onStart();
        initInjector();
        initViews();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.relative_class_committee:
            case R.id.relative_take_office_teacher:
                Intent intent = new Intent(getActivity(), OfficeTeacherActivity.class);
                intent.putParcelableArrayListExtra("classroom",classroom);
                intent.putParcelableArrayListExtra("committeeEntities",committeeEntities);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void loadData(List<ClassInfo.InfoEntity> infoEntities) {
        if (infoEntities.size() > 0 && infoEntities != null) {
            Glide.with(mContext).load(BaseProtocol.IMG_BASE + infoEntities.get(0).getLogo()).error(R.drawable.miyue).into(iv_class_picture);
            int length = infoEntities.get(0).getMemo().length();
            if (mIsBig != null && "isBig".equals(mIsBig)) {
                isBigSize(length, 40);
            } else {
                isBigSize(length, 30);
            }
            tv_class_motto_content.setText(infoEntities.get(0).getMemo());
        }
    }

    private ArrayList<ClassInfo.ClassroomEntity> classroom;
    private ArrayList<ClassInfo.CommitteeEntity> committeeEntities;

    @Override
    public void loadClassroomDate(ArrayList<ClassInfo.ClassroomEntity> classroom) {
        this.classroom = classroom;
    }

    @Override
    public void loadCommitteeDate(ArrayList<ClassInfo.CommitteeEntity> committeeEntities) {
        this.committeeEntities = committeeEntities;
    }

    private void isBigSize(int length, int size) {
        if (length <= 15 && length > 0) {
            setMottoContent(size);
        } else if (length > 15 && length <= 18) {
            setMottoContent(size - 2);
        } else if (length > 18 && length <= 21) {
            setMottoContent(size - 4);
        } else if (length > 21 && length <= 24) {
            setMottoContent(size - 6);
        }
    }

    /**
     * 设置 座右铭的字体大小
     *
     * @param size
     */
    private void setMottoContent(int size) {
        tv_class_motto_content.setTextSize(size);
    }
}
