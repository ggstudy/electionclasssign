package classign.election.kingvar.come.electionclasssign.injector.modules;

import classign.election.kingvar.come.electionclasssign.injector.PerFragment;
import classign.election.kingvar.come.electionclasssign.module.base.IBasePresenter;
import classign.election.kingvar.come.electionclasssign.module.main.classmien.classinfo.ClassInfoFragment;
import classign.election.kingvar.come.electionclasssign.module.main.classmien.classinfo.ClassInfoPersenter;
import dagger.Module;
import dagger.Provides;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/16 0016 15:28
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */
@Module
public class ClassInfoModule {
    private static final String TAG = "AttendanceRecoderModule";
    private final ClassInfoFragment mClassInfoFragment;
    private final  String mClsid;
    public ClassInfoModule(ClassInfoFragment fragment, String clsid) {
        this.mClassInfoFragment = fragment;
        this.mClsid = clsid;
    }



    @PerFragment
    @Provides
    public IBasePresenter provideMainPresenter() {
        return new ClassInfoPersenter(mClassInfoFragment,mClsid);
    }

}
