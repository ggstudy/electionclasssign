package classign.election.kingvar.come.electionclasssign.module.main;

import java.util.ArrayList;
import java.util.List;

import classign.election.kingvar.come.electionclasssign.adapter.RecyclerAdapter;
import classign.election.kingvar.come.electionclasssign.api.info.AllStudentInfo;
import classign.election.kingvar.come.electionclasssign.api.info.BaseClassInfo;
import classign.election.kingvar.come.electionclasssign.api.info.ProClassAndSchoolNotice;
import classign.election.kingvar.come.electionclasssign.api.info.ProClassSchedule;
import classign.election.kingvar.come.electionclasssign.api.info.ProTemperature;
import classign.election.kingvar.come.electionclasssign.api.info.TodayDeYuInfo;
import classign.election.kingvar.come.electionclasssign.api.info.TodayDutyInfo;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/16 0016 14:56
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public interface MainActivityView {
    /**
     * 班级基本信息数据 回调
     * @param baseClassInfo   基本班级信息数据
     */
    void loadClassInfoData(BaseClassInfo baseClassInfo);
    /**
     * 获取学生全部信息 回调
     * @param allStudentInfo   基本班级信息数据
     */
    void loadAllStudentInfoData(AllStudentInfo allStudentInfo);

    /**
     * 学校公告信息数据 回调
     * @param schoolInfoEntityList
     * @param size
     */
    void setSchoolNoticeList(ArrayList<ProClassAndSchoolNotice.SchoolInfoEntity> schoolInfoEntityList, int size);
    /**
     * 班级公告信息数据 回调
     * @param clsNoticeEntityList
     * @param size
     */

    void setClassNoticeList(ArrayList<ProClassAndSchoolNotice.ClsNoticeEntity> clsNoticeEntityList, int size);

    /**
     * 日程管理信息的回调
     * @param info
     */
    void setClassScheduleData(List<ProClassSchedule.ScheduleEntity> info);
    /**
     * 本周晚自习回调
     * @param nightAdapter    adapter
     */
    void loadTodayProjectData(RecyclerAdapter nightAdapter);

    /**
     * 今天和明天的课程 回调
     */
    void loadTodayProjectList();

    /**
     * 今日值日回调
     * @param todayDutyInfo     今日值日数据
     * @param recyclerAdapter     adapter
     */
    void loadTodayDutyData(TodayDutyInfo todayDutyInfo, RecyclerAdapter recyclerAdapter,Boolean isNull);

    /**
     * 当前温度天气回调
     * @param forecastEntityList
     * @param wendu
     */
    void setTemperatureInfo(List<ProTemperature.DataEntity.ForecastEntity> forecastEntityList, String wendu);

    /**
     * 主页今日德育回调
     * @param todayDeYuInfo
     */
    void  setTodayDeYuInfoData(TodayDeYuInfo todayDeYuInfo);


    void setStudentMien(int checkedId);
}
