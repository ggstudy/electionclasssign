package classign.election.kingvar.come.electionclasssign.module.main.moraleducation.title;

import classign.election.kingvar.come.electionclasssign.api.info.TitleInfo;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/16 0016 14:56
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public interface IAttendanceTitleView {
    /**
     * 标题数据
     * @param titleInfo     选中栏目
     */

    void  loadTitleData(TitleInfo titleInfo);
}
