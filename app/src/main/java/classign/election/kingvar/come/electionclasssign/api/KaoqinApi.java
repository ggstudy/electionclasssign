package classign.election.kingvar.come.electionclasssign.api;


import classign.election.kingvar.come.electionclasssign.api.info.ProIsCodeSuccess;
import classign.election.kingvar.come.electionclasssign.api.info.TodayDeYuInfo;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

import static classign.election.kingvar.come.electionclasssign.api.RetrofitService.CACHE_CONTROL_NETWORK;


/**
 * Created by long on 2016/8/22.
 * API 接口
 */
public interface KaoqinApi {
    /**
     * 获取主页今日德育信息
     * @param classid
     * @return
     */
    @Headers(CACHE_CONTROL_NETWORK)
    @GET("ClassStatus")
    Observable<TodayDeYuInfo> getTodayDeYuInfo(@Query("classid") String classid);

    /**
     * 上传打卡数据
     * @param imei
     * @param schoolid
     * @param date
     * @param data
     * @return
     */
    @POST("KaoQinDaKa")
    Observable<ProIsCodeSuccess> postUpLoadData(@Query("imei")String imei, @Query("schoolid")String schoolid
            , @Query("date") String date, @Query("data") String data);
}
