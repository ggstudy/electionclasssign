package classign.election.kingvar.come.electionclasssign.module.main.moraleducation.one;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import butterknife.BindView;
import classign.election.kingvar.come.electionclasssign.R;
import classign.election.kingvar.come.electionclasssign.api.info.AttendanceInfo;
import classign.election.kingvar.come.electionclasssign.injector.components.DaggerAttendanceRecoderOneComponent;
import classign.election.kingvar.come.electionclasssign.injector.modules.AttendanceRecoderOneModule;
import classign.election.kingvar.come.electionclasssign.module.base.BaseFragment;
import classign.election.kingvar.come.electionclasssign.module.base.IBasePresenter;
import classign.election.kingvar.come.electionclasssign.module.main.IAttendanceRecoderView;
import classign.election.kingvar.come.electionclasssign.tools.bean.User;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/16 0016 19:34
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public class TodayAttendanceOneFragment extends BaseFragment<IBasePresenter> implements IAttendanceRecoderView {
    private static final String TAG = "TodayAttendanceOneFragm";
    private static final String NEWS_TYPE_KEY = "title_one_fragment";
    private String mTitleId;

    @BindView(R.id.tv_should_num)
    TextView mTvShouldNum;
    @BindView(R.id.tv_already_num)
    TextView mTvAlreadyNum;
    @BindView(R.id.tv_request_num)
    TextView mTvRequestNum;

    public static TodayAttendanceOneFragment newInstance(String titleId) {
        TodayAttendanceOneFragment fragment = new TodayAttendanceOneFragment();
        Bundle bundle = new Bundle();
        bundle.putString(NEWS_TYPE_KEY, titleId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.fragment_attendance_one;
    }

    @Override
    protected void initInjector() {
        if (getArguments() != null) {
            mTitleId = getArguments().getString(NEWS_TYPE_KEY);
        }
        DaggerAttendanceRecoderOneComponent.builder()
                .applicationComponent(getAppComponent())
                .attendanceRecoderOneModule(new AttendanceRecoderOneModule(this,User.getClassId(mContext),mTitleId))
                .build()
                .inject(this);
    }

    @Override
    protected void initViews() {
    }

    @Override
    protected void updateViews(boolean isRefresh) {
        mPresenter.getData(isRefresh);
    }

    @Override
    public void loadData(AttendanceInfo attendanceInfo) {
        Log.i(TAG, "loadData: size = " );
        if (attendanceInfo!=null){
            mTvRequestNum.setText(attendanceInfo.getInLeave()+"");
            mTvAlreadyNum.setText(attendanceInfo.getClassStu()+"");
            mTvShouldNum.setText(attendanceInfo.getSnum()+"");
            hideLoading();
        }
    }
}
