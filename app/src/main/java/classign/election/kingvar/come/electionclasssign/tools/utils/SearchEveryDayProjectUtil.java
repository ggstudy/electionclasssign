package classign.election.kingvar.come.electionclasssign.tools.utils;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import classign.election.kingvar.come.electionclasssign.activity.MyApplication;
import classign.election.kingvar.come.electionclasssign.api.info.AllProjectInfo;
import classign.election.kingvar.come.electionclasssign.greendao.ProjectFirdayInfo;
import classign.election.kingvar.come.electionclasssign.greendao.ProjectFirdayInfoDao;
import classign.election.kingvar.come.electionclasssign.greendao.ProjectInfo;
import classign.election.kingvar.come.electionclasssign.greendao.ProjectInfoDao;
import classign.election.kingvar.come.electionclasssign.greendao.ProjectSaturdayInfo;
import classign.election.kingvar.come.electionclasssign.greendao.ProjectSaturdayInfoDao;
import classign.election.kingvar.come.electionclasssign.greendao.ProjectSundayInfo;
import classign.election.kingvar.come.electionclasssign.greendao.ProjectSundayInfoDao;
import classign.election.kingvar.come.electionclasssign.greendao.ProjectTuesdayInfo;
import classign.election.kingvar.come.electionclasssign.greendao.ProjectTuesdayInfoDao;
import classign.election.kingvar.come.electionclasssign.greendao.ProjectTursdayInfo;
import classign.election.kingvar.come.electionclasssign.greendao.ProjectTursdayInfoDao;
import classign.election.kingvar.come.electionclasssign.greendao.ProjectWednesdayInfo;
import classign.election.kingvar.come.electionclasssign.greendao.ProjectWednesdayInfoDao;


/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/4/12 0012 16:17
 * @des ${查询课程}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public class SearchEveryDayProjectUtil {
    private static final String TAG = "SearchEveryDayProjectUt";

    /**
     * 查询今天和明天两天的课程
     *
     * @param weekday
     * @return
     */
    public static List<AllProjectInfo.Entity> getTodayWeek(String weekday) {

        Log.i(TAG, "getTodayWeek: weekday = " + weekday);
        List<AllProjectInfo.Entity> doubleProject = new ArrayList<>();
        if ("星期一".equals(weekday)) {
            List<ProjectInfo> mondayList = MyApplication.getAppComponent().getDaoSession().getProjectInfoDao().queryBuilder().where(ProjectInfoDao.Properties.Tag.eq("1s")).build().list();
            List<ProjectTuesdayInfo> tuesdayList = MyApplication.getAppComponent().getDaoSession().getProjectTuesdayInfoDao().queryBuilder().where(ProjectTuesdayInfoDao.Properties.Tag.eq("2s")).build().list();
            for (AllProjectInfo.Entity monday : mondayList) {
                doubleProject.add(monday);
            }
            for (AllProjectInfo.Entity tuesday : tuesdayList) {
                doubleProject.add(tuesday);
            }
        } else if ("星期二".equals(weekday)) {
            List<ProjectTuesdayInfo> tuesdayList = MyApplication.getAppComponent().getDaoSession().getProjectTuesdayInfoDao().queryBuilder().where(ProjectTuesdayInfoDao.Properties.Tag.eq("2s")).build().list();
            List<ProjectWednesdayInfo> wednesdayList = MyApplication.getAppComponent().getDaoSession().getProjectWednesdayInfoDao().queryBuilder().where(ProjectWednesdayInfoDao.Properties.Tag.eq("3s")).build().list();
            for (AllProjectInfo.Entity monday : tuesdayList) {
                doubleProject.add(monday);
            }
            for (AllProjectInfo.Entity tuesday : wednesdayList) {
                doubleProject.add(tuesday);
            }
        } else if ("星期三".equals(weekday)) {
            List<ProjectWednesdayInfo> wednesdayList = MyApplication.getAppComponent().getDaoSession().getProjectWednesdayInfoDao().queryBuilder().where(ProjectWednesdayInfoDao.Properties.Tag.eq("3s")).build().list();
            List<ProjectTursdayInfo> thursdayList = MyApplication.getAppComponent().getDaoSession().getProjectTursdayInfoDao().queryBuilder().where(ProjectTursdayInfoDao.Properties.Tag.eq("4s")).build().list();
            for (AllProjectInfo.Entity monday : wednesdayList) {
                doubleProject.add(monday);
            }
            for (AllProjectInfo.Entity tuesday : thursdayList) {
                doubleProject.add(tuesday);
            }
        } else if ("星期四".equals(weekday)) {
            List<ProjectTursdayInfo> thursdayList = MyApplication.getAppComponent().getDaoSession().getProjectTursdayInfoDao().queryBuilder().where(ProjectTursdayInfoDao.Properties.Tag.eq("4s")).build().list();
            List<ProjectFirdayInfo> fridayList = MyApplication.getAppComponent().getDaoSession().getProjectFirdayInfoDao().queryBuilder().where(ProjectFirdayInfoDao.Properties.Tag.eq("5s")).build().list();
            for (AllProjectInfo.Entity monday : thursdayList) {
                doubleProject.add(monday);
            }
            for (AllProjectInfo.Entity tuesday : fridayList) {
                doubleProject.add(tuesday);
            }
        } else if ("星期五".equals(weekday)) {
            List<ProjectFirdayInfo> fridayList = MyApplication.getAppComponent().getDaoSession().getProjectFirdayInfoDao().queryBuilder().where(ProjectFirdayInfoDao.Properties.Tag.eq("5s")).build().list();
            List<ProjectSaturdayInfo> saturdayList = MyApplication.getAppComponent().getDaoSession().getProjectSaturdayInfoDao().queryBuilder().where(ProjectSaturdayInfoDao.Properties.Tag.eq("6s")).build().list();
            for (AllProjectInfo.Entity monday : fridayList) {
                doubleProject.add(monday);
            }
            for (AllProjectInfo.Entity tuesday : saturdayList) {
                doubleProject.add(tuesday);
            }
        } else if ("星期六".equals(weekday)) {
            List<ProjectSaturdayInfo> saturdayList = MyApplication.getAppComponent().getDaoSession().getProjectSaturdayInfoDao().queryBuilder().where(ProjectSaturdayInfoDao.Properties.Tag.eq("6s")).build().list();
            List<ProjectSundayInfo> sundayList = MyApplication.getAppComponent().getDaoSession().getProjectSundayInfoDao().queryBuilder().where(ProjectSundayInfoDao.Properties.Tag.eq("7s")).build().list();
            for (AllProjectInfo.Entity monday : saturdayList) {
                doubleProject.add(monday);
            }
            for (AllProjectInfo.Entity tuesday : sundayList) {
                doubleProject.add(tuesday);
            }
        } else if ("星期日".equals(weekday)) {
            List<ProjectSundayInfo> sundayList = MyApplication.getAppComponent().getDaoSession().getProjectSundayInfoDao().queryBuilder().where(ProjectSundayInfoDao.Properties.Tag.eq("7s")).build().list();
            List<ProjectInfo> mondayList = MyApplication.getAppComponent().getDaoSession().getProjectInfoDao().queryBuilder().where(ProjectInfoDao.Properties.Tag.eq("1s")).build().list();
            for (AllProjectInfo.Entity monday : sundayList) {
                doubleProject.add(monday);
            }
            for (AllProjectInfo.Entity tuesday : mondayList) {
                doubleProject.add(tuesday);
            }
        }
        return doubleProject;
    }

    /**
     * 获取本周全部课程(从数据库)
     *
     * @return
     */
    public static List<AllProjectInfo.Entity> getAllProject() {
        List<AllProjectInfo.Entity> allProject = new ArrayList<>();

        List<ProjectInfo> mondayList = MyApplication.getAppComponent().getDaoSession().getProjectInfoDao().queryBuilder().where(ProjectInfoDao.Properties.Tag.eq("1s")).build().list();
        List<ProjectTuesdayInfo> tuesdayList = MyApplication.getAppComponent().getDaoSession().getProjectTuesdayInfoDao().queryBuilder().where(ProjectTuesdayInfoDao.Properties.Tag.eq("2s")).build().list();
        List<ProjectWednesdayInfo> wednesdayList = MyApplication.getAppComponent().getDaoSession().getProjectWednesdayInfoDao().queryBuilder().where(ProjectWednesdayInfoDao.Properties.Tag.eq("3s")).build().list();
        List<ProjectTursdayInfo> thursdayList = MyApplication.getAppComponent().getDaoSession().getProjectTursdayInfoDao().queryBuilder().where(ProjectTursdayInfoDao.Properties.Tag.eq("4s")).build().list();
        List<ProjectFirdayInfo> fridayList = MyApplication.getAppComponent().getDaoSession().getProjectFirdayInfoDao().queryBuilder().where(ProjectFirdayInfoDao.Properties.Tag.eq("5s")).build().list();
        List<ProjectSaturdayInfo> saturdayList = MyApplication.getAppComponent().getDaoSession().getProjectSaturdayInfoDao().queryBuilder().where(ProjectSaturdayInfoDao.Properties.Tag.eq("6s")).build().list();
        List<ProjectSundayInfo> sundayList = MyApplication.getAppComponent().getDaoSession().getProjectSundayInfoDao().queryBuilder().where(ProjectSundayInfoDao.Properties.Tag.eq("7s")).build().list();
        for (AllProjectInfo.Entity monday : mondayList) {
            allProject.add(monday);
        }
        for (AllProjectInfo.Entity tuesday : tuesdayList) {
            allProject.add(tuesday);
        }
        for (AllProjectInfo.Entity wednesday : wednesdayList) {
            allProject.add(wednesday);
        }
        for (AllProjectInfo.Entity thursday : thursdayList) {
            allProject.add(thursday);
        }
        for (AllProjectInfo.Entity friday : fridayList) {
            allProject.add(friday);
        }
        for (AllProjectInfo.Entity saturday : saturdayList) {
            allProject.add(saturday);
        }
        for (AllProjectInfo.Entity sunday : sundayList) {
            allProject.add(sunday);
        }

        return allProject;
    }

    /**
     * 获取当天的课程
     *
     * @param weekday
     * @return
     */
    public static List<String> getCurrentDayProject(String weekday) {

        Log.i(TAG, "getTodayWeek: weekday = " + weekday);
        List<String> currentDayProject = new ArrayList<>();
        currentDayProject.add("早自习");

        if ("星期一".equals(weekday)) {
            List<ProjectInfo> mondayList = MyApplication.getAppComponent().getDaoSession().getProjectInfoDao().queryBuilder().where(ProjectInfoDao.Properties.Tag.eq("1s")).build().list();
            for (int i = 0; i < mondayList.size(); i++) {
                if (!"".equals(mondayList.get(i).getCourse())) {
                    currentDayProject.add(mondayList.get(i).getCourse());
                }
            }
        } else if ("星期二".equals(weekday)) {
            List<ProjectTuesdayInfo> tuesdayList = MyApplication.getAppComponent().getDaoSession().getProjectTuesdayInfoDao().queryBuilder().where(ProjectTuesdayInfoDao.Properties.Tag.eq("2s")).build().list();
            for (int i = 0; i < tuesdayList.size(); i++) {
                if (!"".equals(tuesdayList.get(i).getCourse())) {
                    currentDayProject.add(tuesdayList.get(i).getCourse());
                }
            }
        } else if ("星期三".equals(weekday)) {
            List<ProjectWednesdayInfo> wednesdayList = MyApplication.getAppComponent().getDaoSession().getProjectWednesdayInfoDao().queryBuilder().where(ProjectWednesdayInfoDao.Properties.Tag.eq("3s")).build().list();
            for (int i = 0; i < wednesdayList.size(); i++) {
                Log.i(TAG, "getCurrentDayProject: course = " + wednesdayList.get(i).getCourse());
                if (!"".equals(wednesdayList.get(i).getCourse())) {
                    currentDayProject.add(wednesdayList.get(i).getCourse());
                }
            }
        } else if ("星期四".equals(weekday)) {
            List<ProjectTursdayInfo> thursdayList = MyApplication.getAppComponent().getDaoSession().getProjectTursdayInfoDao().queryBuilder().where(ProjectTursdayInfoDao.Properties.Tag.eq("4s")).build().list();
            for (int i = 0; i < thursdayList.size(); i++) {
                if (!"".equals(thursdayList.get(i).getCourse())) {
                    currentDayProject.add(thursdayList.get(i).getCourse());
                }
            }
        } else if ("星期五".equals(weekday)) {
            List<ProjectFirdayInfo> fridayList = MyApplication.getAppComponent().getDaoSession().getProjectFirdayInfoDao().queryBuilder().where(ProjectFirdayInfoDao.Properties.Tag.eq("5s")).build().list();
            for (int i = 0; i < fridayList.size(); i++) {
                if (!"".equals(fridayList.get(i).getCourse())) {
                    currentDayProject.add(fridayList.get(i).getCourse());
                }
            }
        } else if ("星期六".equals(weekday)) {
            List<ProjectSaturdayInfo> saturdayList = MyApplication.getAppComponent().getDaoSession().getProjectSaturdayInfoDao().queryBuilder().where(ProjectSaturdayInfoDao.Properties.Tag.eq("6s")).build().list();
            for (int i = 0; i < saturdayList.size(); i++) {
                if (!"".equals(saturdayList.get(i).getCourse())) {
                    currentDayProject.add(saturdayList.get(i).getCourse());
                }
            }
        } else if ("星期日".equals(weekday)) {
            List<ProjectSundayInfo> sundayList = MyApplication.getAppComponent().getDaoSession().getProjectSundayInfoDao().queryBuilder().where(ProjectSundayInfoDao.Properties.Tag.eq("7s")).build().list();
            for (int i = 0; i < sundayList.size(); i++) {
                if (!"".equals(sundayList.get(i).getCourse())) {
                    currentDayProject.add(sundayList.get(i).getCourse());
                }
            }
        }
        currentDayProject.add("晚自习");
        return currentDayProject;
    }

    /**
     * 获取当前课程开始时间
     *
     * @param weekday
     * @return
     */
    public static List<String> getCurrentProjectStartTime(String weekday) {

        Log.i(TAG, "getTodayWeek: weekday = " + weekday);
        List<String> currentDayStartTime = new ArrayList<>();
        if ("星期一".equals(weekday)) {
            List<ProjectInfo> mondayList = MyApplication.getAppComponent().getDaoSession().getProjectInfoDao().queryBuilder().where(ProjectInfoDao.Properties.Tag.eq("1s")).build().list();
            for (int i = 0; i < mondayList.size(); i++) {
                if (!"".equals(mondayList.get(i).getStart())) {
                    String startTime = mondayList.get(i).getStart();
                    if (startTime != null) {
                        currentDayStartTime.add(startTime.substring(0, 2) + startTime.substring(3, 5));
                    } else {
                        currentDayStartTime.add(startTime);
                    }
                }
            }
        } else if ("星期二".equals(weekday)) {
            List<ProjectTuesdayInfo> tuesdayList = MyApplication.getAppComponent().getDaoSession().getProjectTuesdayInfoDao().queryBuilder().where(ProjectTuesdayInfoDao.Properties.Tag.eq("2s")).build().list();
            for (int i = 0; i < tuesdayList.size(); i++) {
                if (!"".equals(tuesdayList.get(i).getStart())) {
                    String startTime = tuesdayList.get(i).getStart();
                    if (startTime != null) {
                        currentDayStartTime.add(startTime.substring(0, 2) + startTime.substring(3, 5));
                    } else {
                        currentDayStartTime.add(startTime);
                    }
                }
            }
        } else if ("星期三".equals(weekday)) {
            List<ProjectWednesdayInfo> wednesdayList = MyApplication.getAppComponent().getDaoSession().getProjectWednesdayInfoDao().queryBuilder().where(ProjectWednesdayInfoDao.Properties.Tag.eq("3s")).build().list();
            for (int i = 0; i < wednesdayList.size(); i++) {
                Log.i(TAG, "getCurrentDayProject: course = " + wednesdayList.get(i).getCourse());
                if (!"".equals(wednesdayList.get(i).getStart())) {
                    String startTime = wednesdayList.get(i).getStart();
                    if (startTime != null) {
                        currentDayStartTime.add(startTime.substring(0, 2) + startTime.substring(3, 5));
                    } else {
                        currentDayStartTime.add(startTime);
                    }
                }
            }
        } else if ("星期四".equals(weekday)) {
            List<ProjectTursdayInfo> thursdayList = MyApplication.getAppComponent().getDaoSession().getProjectTursdayInfoDao().queryBuilder().where(ProjectTursdayInfoDao.Properties.Tag.eq("4s")).build().list();
            for (int i = 0; i < thursdayList.size(); i++) {
                if (!"".equals(thursdayList.get(i).getStart())) {
                    String startTime = thursdayList.get(i).getStart();
                    if (startTime != null) {
                        currentDayStartTime.add(startTime.substring(0, 2) + startTime.substring(3, 5));
                    } else {
                        currentDayStartTime.add(startTime);
                    }
                }
            }
        } else if ("星期五".equals(weekday)) {
            List<ProjectFirdayInfo> fridayList = MyApplication.getAppComponent().getDaoSession().getProjectFirdayInfoDao().queryBuilder().where(ProjectFirdayInfoDao.Properties.Tag.eq("5s")).build().list();
            for (int i = 0; i < fridayList.size(); i++) {
                if (!"".equals(fridayList.get(i).getStart())) {
                    String startTime = fridayList.get(i).getStart();
                    if (startTime != null) {
                        currentDayStartTime.add(startTime.substring(0, 2) + startTime.substring(3, 5));
                    } else {
                        currentDayStartTime.add(startTime);
                    }
                }
            }
        } else if ("星期六".equals(weekday)) {
            List<ProjectSaturdayInfo> saturdayList = MyApplication.getAppComponent().getDaoSession().getProjectSaturdayInfoDao().queryBuilder().where(ProjectSaturdayInfoDao.Properties.Tag.eq("6s")).build().list();
            for (int i = 0; i < saturdayList.size(); i++) {
                if (!"".equals(saturdayList.get(i).getStart())) {
                    String startTime = saturdayList.get(i).getStart();
                    if (startTime != null) {
                        currentDayStartTime.add(startTime.substring(0, 2) + startTime.substring(3, 5));
                    } else {
                        currentDayStartTime.add(startTime);
                    }
                }
            }
        } else if ("星期日".equals(weekday)) {
            List<ProjectSundayInfo> sundayList = MyApplication.getAppComponent().getDaoSession().getProjectSundayInfoDao().queryBuilder().where(ProjectSundayInfoDao.Properties.Tag.eq("7s")).build().list();
            for (int i = 0; i < sundayList.size(); i++) {
                if (!"".equals(sundayList.get(i).getStart())) {
                    String startTime = sundayList.get(i).getStart();
                    if (startTime != null) {
                        currentDayStartTime.add(startTime.substring(0, 2) + startTime.substring(3, 5));
                    } else {
                        currentDayStartTime.add(startTime);
                    }
                }
            }
        }
        return currentDayStartTime;
    }

    /**
     * 获取当前课程结束时间
     *
     * @param weekday
     * @return
     */
    public static List<String> getCurrentProjectEndTime(String weekday) {

        Log.i(TAG, "getTodayWeek: weekday = " + weekday);
        List<String> currentDayEndTime = new ArrayList<>();
        if ("星期一".equals(weekday)) {
            List<ProjectInfo> mondayList = MyApplication.getAppComponent().getDaoSession().getProjectInfoDao().queryBuilder().where(ProjectInfoDao.Properties.Tag.eq("1s")).build().list();
            for (int i = 0; i < mondayList.size(); i++) {
                if (!"".equals(mondayList.get(i).getEnd())) {
                    String endTime = mondayList.get(i).getEnd();
                    if (endTime != null) {
                        currentDayEndTime.add(endTime.substring(0, 2) + endTime.substring(3, 5));
                    } else {
                        currentDayEndTime.add(endTime);
                    }
                }
            }
        } else if ("星期二".equals(weekday)) {
            List<ProjectTuesdayInfo> tuesdayList = MyApplication.getAppComponent().getDaoSession().getProjectTuesdayInfoDao().queryBuilder().where(ProjectTuesdayInfoDao.Properties.Tag.eq("2s")).build().list();
            for (int i = 0; i < tuesdayList.size(); i++) {
                if (!"".equals(tuesdayList.get(i).getEnd())) {
                    String endTime = tuesdayList.get(i).getEnd();
                    if (endTime != null) {
                        currentDayEndTime.add(endTime.substring(0, 2) + endTime.substring(3, 5));
                    } else {
                        currentDayEndTime.add(endTime);
                    }
                }
            }
        } else if ("星期三".equals(weekday)) {
            List<ProjectWednesdayInfo> wednesdayList = MyApplication.getAppComponent().getDaoSession().getProjectWednesdayInfoDao().queryBuilder().where(ProjectWednesdayInfoDao.Properties.Tag.eq("3s")).build().list();
            for (int i = 0; i < wednesdayList.size(); i++) {
                Log.i(TAG, "getCurrentDayProject: course = " + wednesdayList.get(i).getCourse());
                if (!"".equals(wednesdayList.get(i).getEnd())) {
                    String endTime = wednesdayList.get(i).getEnd();
                    if (endTime != null) {
                        currentDayEndTime.add(endTime.substring(0, 2) + endTime.substring(3, 5));
                    } else {
                        currentDayEndTime.add(endTime);
                    }
                }
            }
        } else if ("星期四".equals(weekday)) {
            List<ProjectTursdayInfo> thursdayList = MyApplication.getAppComponent().getDaoSession().getProjectTursdayInfoDao().queryBuilder().where(ProjectTursdayInfoDao.Properties.Tag.eq("4s")).build().list();
            for (int i = 0; i < thursdayList.size(); i++) {
                if (!"".equals(thursdayList.get(i).getEnd())) {
                    String endTime = thursdayList.get(i).getEnd();
                    if (endTime != null) {
                        currentDayEndTime.add(endTime.substring(0, 2) + endTime.substring(3, 5));
                    } else {
                        currentDayEndTime.add(endTime);
                    }
                }
            }
        } else if ("星期五".equals(weekday)) {
            List<ProjectFirdayInfo> fridayList = MyApplication.getAppComponent().getDaoSession().getProjectFirdayInfoDao().queryBuilder().where(ProjectFirdayInfoDao.Properties.Tag.eq("5s")).build().list();
            for (int i = 0; i < fridayList.size(); i++) {
                if (!"".equals(fridayList.get(i).getEnd())) {
                    String endTime = fridayList.get(i).getEnd();
                    if (endTime != null) {
                        currentDayEndTime.add(endTime.substring(0, 2) + endTime.substring(3, 5));
                    } else {
                        currentDayEndTime.add(endTime);
                    }
                }
            }
        } else if ("星期六".equals(weekday)) {
            List<ProjectSaturdayInfo> saturdayList = MyApplication.getAppComponent().getDaoSession().getProjectSaturdayInfoDao().queryBuilder().where(ProjectSaturdayInfoDao.Properties.Tag.eq("6s")).build().list();
            for (int i = 0; i < saturdayList.size(); i++) {
                if (!"".equals(saturdayList.get(i).getEnd())) {
                    String endTime = saturdayList.get(i).getEnd();
                    if (endTime != null) {
                        currentDayEndTime.add(endTime.substring(0, 2) + endTime.substring(3, 5));
                    } else {
                        currentDayEndTime.add(endTime);
                    }
                }
            }
        } else if ("星期日".equals(weekday)) {
            List<ProjectSundayInfo> sundayList = MyApplication.getAppComponent().getDaoSession().getProjectSundayInfoDao().queryBuilder().where(ProjectSundayInfoDao.Properties.Tag.eq("7s")).build().list();
            for (int i = 0; i < sundayList.size(); i++) {
                if (!"".equals(sundayList.get(i).getEnd())) {
                    String endTime = sundayList.get(i).getEnd();
                    if (endTime != null) {
                        currentDayEndTime.add(endTime.substring(0, 2) + endTime.substring(3, 5));
                    } else {
                        currentDayEndTime.add(endTime);
                    }
                }
            }
        }
        return currentDayEndTime;
    }

    /**
     * 获取当前课程开始时间
     *
     * @param weekday
     * @return
     */
    public static List<AllProjectInfo.Entity> getCurrentTeacherName(String weekday) {

        Log.i(TAG, "getTodayWeek: weekday = " + weekday);
        List<AllProjectInfo.Entity> currentDayStartTime = new ArrayList<>();
        if ("星期一".equals(weekday)) {
            List<ProjectInfo> mondayList = MyApplication.getAppComponent().getDaoSession().getProjectInfoDao().queryBuilder().where(ProjectInfoDao.Properties.Tag.eq("1s")).build().list();
            for (int i = 0; i <mondayList.size(); i++) {
                currentDayStartTime.add(mondayList.get(i));
            }
        } else if ("星期二".equals(weekday)) {
            List<ProjectTuesdayInfo> tuesdayList = MyApplication.getAppComponent().getDaoSession().getProjectTuesdayInfoDao().queryBuilder().where(ProjectTuesdayInfoDao.Properties.Tag.eq("2s")).build().list();
            for (int i = 0; i < tuesdayList.size(); i++) {
            currentDayStartTime.add(tuesdayList.get(i));
            }

        } else if ("星期三".equals(weekday)) {
            List<ProjectWednesdayInfo> wednesdayList = MyApplication.getAppComponent().getDaoSession().getProjectWednesdayInfoDao().queryBuilder().where(ProjectWednesdayInfoDao.Properties.Tag.eq("3s")).build().list();
            for (int i = 0; i < wednesdayList.size(); i++) {
                currentDayStartTime.add(wednesdayList.get(i));
            }

        } else if ("星期四".equals(weekday)) {
            List<ProjectTursdayInfo> thursdayList = MyApplication.getAppComponent().getDaoSession().getProjectTursdayInfoDao().queryBuilder().where(ProjectTursdayInfoDao.Properties.Tag.eq("4s")).build().list();
            for (int i = 0; i < thursdayList.size(); i++) {
                currentDayStartTime.add(thursdayList.get(i));
            }

        } else if ("星期五".equals(weekday)) {
            List<ProjectFirdayInfo> fridayList = MyApplication.getAppComponent().getDaoSession().getProjectFirdayInfoDao().queryBuilder().where(ProjectFirdayInfoDao.Properties.Tag.eq("5s")).build().list();
            for (int i = 0; i < fridayList.size(); i++) {
            currentDayStartTime.add(fridayList.get(i));
            }

        } else if ("星期六".equals(weekday)) {
            List<ProjectSaturdayInfo> saturdayList = MyApplication.getAppComponent().getDaoSession().getProjectSaturdayInfoDao().queryBuilder().where(ProjectSaturdayInfoDao.Properties.Tag.eq("6s")).build().list();
            for (int i = 0; i < saturdayList.size(); i++) {
                currentDayStartTime.add(saturdayList.get(i));
            }

        } else if ("星期日".equals(weekday)) {
            List<ProjectSundayInfo> sundayInfoList = MyApplication.getAppComponent().getDaoSession().getProjectSundayInfoDao().queryBuilder().where(ProjectSundayInfoDao.Properties.Tag.eq("7s")).build().list();
            for (int i = 0; i < sundayInfoList.size(); i++) {
                currentDayStartTime.add(sundayInfoList.get(i));
            }
        }
        return currentDayStartTime;
    }

}
