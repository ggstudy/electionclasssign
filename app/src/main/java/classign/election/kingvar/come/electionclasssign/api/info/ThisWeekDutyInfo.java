package classign.election.kingvar.come.electionclasssign.api.info;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/29 0029 20:03
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public class ThisWeekDutyInfo implements Parcelable{


    /**
     * 3s : {"date":"2017-03-29","clsid":"7","leader":"小候","stus":[{"sname":"里斯"},{"sname":"王五"}],"id":3}
     * 2s : {"date":"2017-03-28","clsid":"7","leader":"彭易星","stus":[{"sname":"理想"},{"sname":"老师"}],"id":2}
     * 1s : {"date":"2017-03-27","clsid":"7","leader":"小候","stus":[{"sname":"里斯"},{"sname":"王五"}],"id":1}
     * code : 0
     * 7s : {"date":"2017-04-02","clsid":"7","leader":"小候","stus":[{"sname":"里斯"},{"sname":"王五"}],"id":7}
     * 6s : {"date":"2017-04-01","clsid":"7","leader":"彭易星","stus":[{"sname":"理想"},{"sname":"老师"}],"id":6}
     * 5s : {"date":"2017-03-31","clsid":"7","leader":"小候","stus":[{"sname":"里斯"},{"sname":"王五"}],"id":5}
     * 4s : {"date":"2017-03-30","clsid":"7","leader":"彭易星","stus":[{"sname":"蓬蓬2"},{"sname":"玥儿"},{"sname":"小妮"},{"sname":"蓬蓬8"},{"sname":"宝宝体验03"},{"sname":"宝宝体验05"},{"sname":"宝宝体验07"},{"sname":"宝宝体验08"},{"sname":"宝宝2"}],"id":4}
     */

    @SerializedName("3s")
    private Entity _$3s;
    @SerializedName("2s")
    private Entity _$2s;
    @SerializedName("1s")
    private Entity _$1s;
    private int code;
    @SerializedName("7s")
    private Entity _$7s;
    @SerializedName("6s")
    private Entity _$6s;
    @SerializedName("5s")
    private Entity _$5s;
    @SerializedName("4s")
    private Entity _$4s;

    protected ThisWeekDutyInfo(Parcel in) {
        code = in.readInt();
    }

    public static final Creator<ThisWeekDutyInfo> CREATOR = new Creator<ThisWeekDutyInfo>() {
        @Override
        public ThisWeekDutyInfo createFromParcel(Parcel in) {
            return new ThisWeekDutyInfo(in);
        }

        @Override
        public ThisWeekDutyInfo[] newArray(int size) {
            return new ThisWeekDutyInfo[size];
        }
    };

    public Entity get_$3s() {
        return _$3s;
    }

    public void set_$3s(Entity _$3s) {
        this._$3s = _$3s;
    }

    public Entity get_$2s() {
        return _$2s;
    }

    public void set_$2s(Entity _$2s) {
        this._$2s = _$2s;
    }

    public Entity get_$1s() {
        return _$1s;
    }

    public void set_$1s(Entity _$1s) {
        this._$1s = _$1s;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Entity get_$7s() {
        return _$7s;
    }

    public void set_$7s(Entity _$7s) {
        this._$7s = _$7s;
    }

    public Entity get_$6s() {
        return _$6s;
    }

    public void set_$6s(Entity _$6s) {
        this._$6s = _$6s;
    }

    public Entity get_$5s() {
        return _$5s;
    }

    public void set_$5s(Entity _$5s) {
        this._$5s = _$5s;
    }

    public Entity get_$4s() {
        return _$4s;
    }

    public void set_$4s(Entity _$4s) {
        this._$4s = _$4s;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(code);
    }

    public static class Entity implements Parcelable{
        /**
         * date : 2017-03-29
         * clsid : 7
         * leader : 小候
         * stus : [{"sname":"里斯"},{"sname":"王五"}]
         * id : 3
         */

        private String date;
        private String clsid;
        private String leader;
        private int id;
        private List<StusEntity> stus;

        protected Entity(Parcel in) {
            date = in.readString();
            clsid = in.readString();
            leader = in.readString();
            id = in.readInt();
        }

        public static final Creator<Entity> CREATOR = new Creator<Entity>() {
            @Override
            public Entity createFromParcel(Parcel in) {
                return new Entity(in);
            }

            @Override
            public Entity[] newArray(int size) {
                return new Entity[size];
            }
        };

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getClsid() {
            return clsid;
        }

        public void setClsid(String clsid) {
            this.clsid = clsid;
        }

        public String getLeader() {
            return leader;
        }

        public void setLeader(String leader) {
            this.leader = leader;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public List<StusEntity> getStus() {
            return stus;
        }

        public void setStus(List<StusEntity> stus) {
            this.stus = stus;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(date);
            dest.writeString(clsid);
            dest.writeString(leader);
            dest.writeInt(id);
        }

        public static class StusEntity implements Parcelable{
            /**
             * sname : 里斯
             */

            private String sname;

            protected StusEntity(Parcel in) {
                sname = in.readString();
            }

            public static final Creator<StusEntity> CREATOR = new Creator<StusEntity>() {
                @Override
                public StusEntity createFromParcel(Parcel in) {
                    return new StusEntity(in);
                }

                @Override
                public StusEntity[] newArray(int size) {
                    return new StusEntity[size];
                }
            };

            public String getSname() {
                return sname;
            }

            public void setSname(String sname) {
                this.sname = sname;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(sname);
            }
        }
    }

}
