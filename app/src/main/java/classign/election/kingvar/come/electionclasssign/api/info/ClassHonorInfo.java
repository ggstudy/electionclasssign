package classign.election.kingvar.come.electionclasssign.api.info;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/23 0023 15:36
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public class ClassHonorInfo implements Parcelable{

    /**
     * honour : [{"date":"2017-03-22","img":"20170322152621354013.jpg","id":1}]
     * code : 0
     */

    private int code;
    private List<HonourEntity> honour;

    protected ClassHonorInfo(Parcel in) {
        code = in.readInt();
    }

    public static final Creator<ClassHonorInfo> CREATOR = new Creator<ClassHonorInfo>() {
        @Override
        public ClassHonorInfo createFromParcel(Parcel in) {
            return new ClassHonorInfo(in);
        }

        @Override
        public ClassHonorInfo[] newArray(int size) {
            return new ClassHonorInfo[size];
        }
    };

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<HonourEntity> getHonour() {
        return honour;
    }

    public void setHonour(List<HonourEntity> honour) {
        this.honour = honour;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(code);
    }

    public static class HonourEntity implements Parcelable{
        /**
         * date : 2017-03-22
         * img : 20170322152621354013.jpg
         * id : 1
         */

        private String date;
        private String img;
        private int id;

        protected HonourEntity(Parcel in) {
            date = in.readString();
            img = in.readString();
            id = in.readInt();
        }

        public static final Creator<HonourEntity> CREATOR = new Creator<HonourEntity>() {
            @Override
            public HonourEntity createFromParcel(Parcel in) {
                return new HonourEntity(in);
            }

            @Override
            public HonourEntity[] newArray(int size) {
                return new HonourEntity[size];
            }
        };

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(date);
            dest.writeString(img);
            dest.writeInt(id);
        }
    }
}
