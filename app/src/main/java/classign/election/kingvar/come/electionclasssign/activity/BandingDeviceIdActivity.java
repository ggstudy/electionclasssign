package classign.election.kingvar.come.electionclasssign.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.orhanobut.logger.Logger;

import java.util.concurrent.TimeUnit;

import classign.election.kingvar.come.electionclasssign.R;
import classign.election.kingvar.come.electionclasssign.api.RetrofitService;
import classign.election.kingvar.come.electionclasssign.api.info.HeartbeatCmd;
import classign.election.kingvar.come.electionclasssign.base.BaseActivity;
import classign.election.kingvar.come.electionclasssign.tools.bean.User;
import classign.election.kingvar.come.electionclasssign.tools.utils.ToastUtils;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;

import static com.orhanobut.logger.Logger.e;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/3 0003 10:19
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public class BandingDeviceIdActivity extends BaseActivity {
    private static final String TAG = "BandingDeviceIdActivity";
    private TextView tv_id;
    private Subscription subscription;

    @Override
    public void onRoot(Bundle savedInstanceState) {
        setContentView(R.layout.activity_banding_device);
    }

    @Override
    protected void initInjector() {
        subscription = Observable.interval(0, 60, TimeUnit.SECONDS)
                .subscribe(new Subscriber<Long>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e(e.toString());
                    }

                    @Override
                    public void onNext(Long aLong) {
                        Log.i(TAG, "onNext: along  = " + aLong);
                        Log.e(TAG, "imei = " + User.getImei(BandingDeviceIdActivity.this) + "");
                        RetrofitService.getHeartbeatCmd(User.getImei(BandingDeviceIdActivity.this))
                                .subscribe(new Subscriber<HeartbeatCmd>() {
                                    @Override
                                    public void onCompleted() {
                                        Log.i(TAG, "onCompleted: 456789136546");
                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        Logger.e(e.toString());
                                    }

                                    @Override
                                    public void onNext(HeartbeatCmd heartbeatCmd) {
                                        tv_id.setText("设备唯一标识符 :" + User.getImei(BandingDeviceIdActivity.this));
                                        if (heartbeatCmd.getCode() == 1) {
                                            tv_id.setText("设备唯一标识符 :" + heartbeatCmd.getImei());
                                            User.setImei(BandingDeviceIdActivity.this, heartbeatCmd.getImei());
                                        } else if (heartbeatCmd.getCode() == 2) {
                                            ToastUtils.showToast("设备已绑定成功,开始运行");
                                            finish();
                                            startActivity(new Intent(BandingDeviceIdActivity.this, SplashActivity.class));
                                            overridePendingTransition(R.anim.hold, R.anim.zoom_in_exit);
                                        }
                                    }
                                });

                    }
                });

    }

    @Override
    protected void initView() {
        tv_id = (TextView) findViewById(R.id.tv_id);
    }

    @Override
    public void initData() {
    }

    @Override
    public void initListener() {

    }

    @Override
    public void WidgetClick(View view) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (subscription != null) {
            subscription.unsubscribe();
        }
    }
}
