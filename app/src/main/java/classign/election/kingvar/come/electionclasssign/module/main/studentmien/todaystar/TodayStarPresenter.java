package classign.election.kingvar.come.electionclasssign.module.main.studentmien.todaystar;

import classign.election.kingvar.come.electionclasssign.api.RetrofitService;
import classign.election.kingvar.come.electionclasssign.api.info.ProToadyStar;
import classign.election.kingvar.come.electionclasssign.module.base.IBasePresenter;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/4/1 0001 16:44
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public class TodayStarPresenter implements IBasePresenter {
    private final ITodayStarView mView;
    private final String mClassid;

    public TodayStarPresenter(TodayStarFragment starFragment, String mClassid) {
        this.mView = starFragment;
        this.mClassid = mClassid;
    }

    @Override
    public void getData(boolean isRefresh) {
        RetrofitService.getTodayStarInfo(mClassid)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ProToadyStar>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(ProToadyStar proToadyStar) {
                        if (proToadyStar.getCode() == 0) {
                            mView.loadData(proToadyStar.getInfo());
                        }
                    }
                });
    }

    @Override
    public void getMoreData() {

    }
}
