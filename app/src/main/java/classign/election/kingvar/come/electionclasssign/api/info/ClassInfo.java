package classign.election.kingvar.come.electionclasssign.api.info;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/28 0028 17:40
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public class ClassInfo implements Parcelable {
    /**
     * committee : [{"title":"班长","clsid":"7","studentid":"2","id":"1","name":"哈哈","sname":"九年级八班","type":"2"},{"title":"学习委员","clsid":"7","studentid":"2","id":"2","name":"哈哈","sname":"九年级八班","type":"2"},{"title":"学生会部长","clsid":"7","studentid":"2","id":"3","name":"哈哈","sname":"九年级八班","type":"1"}]
     * code : 0
     * classroom : [{"flag":"0","name":"gasry1989","cContact":"13922222222","logo":"","title":"英语"},{"flag":"0","name":"提拉米苏1","cContact":"15898765432","logo":"","title":"数学"}]
     * info : [{"clsid":7,"clsname":"九年级八班","logo":"2017010410282092402.jpg","memo":"我自信，我努力，我出色，我成功"}]
     */

    private int code;
    private List<CommitteeEntity> committee;
    private List<ClassroomEntity> classroom;
    private List<InfoEntity> info;

    protected ClassInfo(Parcel in) {
        code = in.readInt();
        committee = in.createTypedArrayList(CommitteeEntity.CREATOR);
        classroom = in.createTypedArrayList(ClassroomEntity.CREATOR);
    }

    public static final Creator<ClassInfo> CREATOR = new Creator<ClassInfo>() {
        @Override
        public ClassInfo createFromParcel(Parcel in) {
            return new ClassInfo(in);
        }

        @Override
        public ClassInfo[] newArray(int size) {
            return new ClassInfo[size];
        }
    };

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<CommitteeEntity> getCommittee() {
        return committee;
    }

    public void setCommittee(List<CommitteeEntity> committee) {
        this.committee = committee;
    }

    public List<ClassroomEntity> getClassroom() {
        return classroom;
    }

    public void setClassroom(List<ClassroomEntity> classroom) {
        this.classroom = classroom;
    }

    public List<InfoEntity> getInfo() {
        return info;
    }

    public void setInfo(List<InfoEntity> info) {
        this.info = info;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(code);
        dest.writeTypedList(committee);
        dest.writeTypedList(classroom);
    }

    public static class CommitteeEntity implements Parcelable{
        /**
         * title : 班长
         * clsid : 7
         * studentid : 2
         * id : 1
         * name : 哈哈
         * sname : 九年级八班
         * type : 2
         */

        private String title;
        private String clsid;
        private String studentid;
        private String id;
        private String name;
        private String sname;
        private String type;

        protected CommitteeEntity(Parcel in) {
            title = in.readString();
            clsid = in.readString();
            studentid = in.readString();
            id = in.readString();
            name = in.readString();
            sname = in.readString();
            type = in.readString();
        }

        public static final Creator<CommitteeEntity> CREATOR = new Creator<CommitteeEntity>() {
            @Override
            public CommitteeEntity createFromParcel(Parcel in) {
                return new CommitteeEntity(in);
            }

            @Override
            public CommitteeEntity[] newArray(int size) {
                return new CommitteeEntity[size];
            }
        };

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getClsid() {
            return clsid;
        }

        public void setClsid(String clsid) {
            this.clsid = clsid;
        }

        public String getStudentid() {
            return studentid;
        }

        public void setStudentid(String studentid) {
            this.studentid = studentid;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSname() {
            return sname;
        }

        public void setSname(String sname) {
            this.sname = sname;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(title);
            dest.writeString(clsid);
            dest.writeString(studentid);
            dest.writeString(id);
            dest.writeString(name);
            dest.writeString(sname);
            dest.writeString(type);
        }
    }

    public static class ClassroomEntity implements Parcelable{
        /**
         * flag : 0
         * name : gasry1989
         * cContact : 13922222222
         * logo :
         * title : 英语
         */

        private String flag;
        private String name;
        private String cContact;
        private String logo;
        private String title;

        protected ClassroomEntity(Parcel in) {
            flag = in.readString();
            name = in.readString();
            cContact = in.readString();
            logo = in.readString();
            title = in.readString();
        }

        public static final Creator<ClassroomEntity> CREATOR = new Creator<ClassroomEntity>() {
            @Override
            public ClassroomEntity createFromParcel(Parcel in) {
                return new ClassroomEntity(in);
            }

            @Override
            public ClassroomEntity[] newArray(int size) {
                return new ClassroomEntity[size];
            }
        };

        public String getFlag() {
            return flag;
        }

        public void setFlag(String flag) {
            this.flag = flag;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCContact() {
            return cContact;
        }

        public void setCContact(String cContact) {
            this.cContact = cContact;
        }

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(flag);
            dest.writeString(name);
            dest.writeString(cContact);
            dest.writeString(logo);
            dest.writeString(title);
        }
    }

    public static class InfoEntity {
        /**
         * clsid : 7
         * clsname : 九年级八班
         * logo : 2017010410282092402.jpg
         * memo : 我自信，我努力，我出色，我成功
         */

        private int clsid;
        private String clsname;
        private String logo;
        private String memo;

        public int getClsid() {
            return clsid;
        }

        public void setClsid(int clsid) {
            this.clsid = clsid;
        }

        public String getClsname() {
            return clsname;
        }

        public void setClsname(String clsname) {
            this.clsname = clsname;
        }

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

        public String getMemo() {
            return memo;
        }

        public void setMemo(String memo) {
            this.memo = memo;
        }
    }

   /* *//**
     * committee : [{"title":"班长","clsid":"7","studentid":"2","id":"1","name":"哈哈","sname":"九年级八班","type":"2"},{"title":"学习委员","clsid":"7","studentid":"2","id":"2","name":"哈哈","sname":"九年级八班","type":"2"},{"title":"学生会部长","clsid":"7","studentid":"2","id":"3","name":"哈哈","sname":"九年级八班","type":"1"}]
     * code : 0
     * classroom : []
     * info : [{"clsid":7,"clsname":"九年级八班","logo":"2017010410282092402.jpg","memo":"我自信，我努力，我出色，我成功"}]
     *//*

    private int code;
    private List<CommitteeEntity> committee;
    private List<?> classroom;
    private List<InfoEntity> info;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<CommitteeEntity> getCommittee() {
        return committee;
    }

    public void setCommittee(List<CommitteeEntity> committee) {
        this.committee = committee;
    }

    public List<?> getClassroom() {
        return classroom;
    }

    public void setClassroom(List<?> classroom) {
        this.classroom = classroom;
    }

    public List<InfoEntity> getInfo() {
        return info;
    }

    public void setInfo(List<InfoEntity> info) {
        this.info = info;
    }

    public static class CommitteeEntity {
        *//**
         * title : 班长
         * clsid : 7
         * studentid : 2
         * id : 1
         * name : 哈哈
         * sname : 九年级八班
         * type : 2
         *//*

        private String title;
        private String clsid;
        private String studentid;
        private String id;
        private String name;
        private String sname;
        private String type;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getClsid() {
            return clsid;
        }

        public void setClsid(String clsid) {
            this.clsid = clsid;
        }

        public String getStudentid() {
            return studentid;
        }

        public void setStudentid(String studentid) {
            this.studentid = studentid;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSname() {
            return sname;
        }

        public void setSname(String sname) {
            this.sname = sname;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    public static class InfoEntity {
        *//**
         * clsid : 7
         * clsname : 九年级八班
         * logo : 2017010410282092402.jpg
         * memo : 我自信，我努力，我出色，我成功
         *//*

        private int clsid;
        private String clsname;
        private String logo;
        private String memo;

        public int getClsid() {
            return clsid;
        }

        public void setClsid(int clsid) {
            this.clsid = clsid;
        }

        public String getClsname() {
            return clsname;
        }

        public void setClsname(String clsname) {
            this.clsname = clsname;
        }

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

        public String getMemo() {
            return memo;
        }

        public void setMemo(String memo) {
            this.memo = memo;
        }
    }*/

}
