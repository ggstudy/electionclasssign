package classign.election.kingvar.come.electionclasssign.module.main.moraleducation.title;

import android.util.Log;

import com.orhanobut.logger.Logger;

import classign.election.kingvar.come.electionclasssign.activity.ToadyAttendanceActivity;
import classign.election.kingvar.come.electionclasssign.api.RetrofitService;
import classign.election.kingvar.come.electionclasssign.api.info.TitleInfo;
import classign.election.kingvar.come.electionclasssign.module.base.IBasePresenter;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/16 0016 15:05
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public class AttendanceTitlePersenter implements IBasePresenter {
    private static final String TAG = "AttendanceRecoderPersen";
    private final IAttendanceTitleView mView;
    private final String mSchoolid;
    private final String mClassId;

    public AttendanceTitlePersenter(ToadyAttendanceActivity toadyAttendanceActivity, String schoolId, String classId) {
        this.mView = toadyAttendanceActivity;
        this.mSchoolid = schoolId;
        this.mClassId = classId;
    }

    public void getTitleInfo() {
        RetrofitService.getTitleInfo(mSchoolid, mClassId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<TitleInfo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Logger.e(e.toString());
                    }

                    @Override
                    public void onNext(TitleInfo titleInfo) {
                        Logger.i(TAG,titleInfo.getData().size());
                        Log.i(TAG, "call: size = " + titleInfo.getData().size());
                        if (titleInfo.getCode() == 0) {
                            mView.loadTitleData(titleInfo);
                        }
                    }
                });
    }

    @Override
    public void getData(boolean isRefresh) {
        getTitleInfo();
    }

    @Override
    public void getMoreData() {

    }
}
