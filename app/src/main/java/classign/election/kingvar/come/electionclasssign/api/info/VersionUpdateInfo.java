package classign.election.kingvar.come.electionclasssign.api.info;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/22 0022 17:17
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public class VersionUpdateInfo implements Parcelable{

    /**
     * path : http://localhost:8081/brand/a?login
     * vertion : 11
     * code : 0
     * content : 第一版初始化
     */

    private String path;
    private int vertion;
    private int code;
    private String content;

    protected VersionUpdateInfo(Parcel in) {
        path = in.readString();
        vertion = in.readInt();
        code = in.readInt();
        content = in.readString();
    }

    public static final Creator<VersionUpdateInfo> CREATOR = new Creator<VersionUpdateInfo>() {
        @Override
        public VersionUpdateInfo createFromParcel(Parcel in) {
            return new VersionUpdateInfo(in);
        }

        @Override
        public VersionUpdateInfo[] newArray(int size) {
            return new VersionUpdateInfo[size];
        }
    };

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getVertion() {
        return vertion;
    }

    public void setVertion(int vertion) {
        this.vertion = vertion;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(path);
        dest.writeInt(vertion);
        dest.writeInt(code);
        dest.writeString(content);
    }
}
