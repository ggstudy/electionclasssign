package classign.election.kingvar.come.electionclasssign.adapter;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;

import classign.election.kingvar.come.electionclasssign.autolayout.utils.AutoUtils;


/**
 * Created by jay on 2015/11/23.
 */
public class RecyclerViewHolder extends RecyclerView.ViewHolder {

	ViewHolderImpl mViewHolder;

	public RecyclerViewHolder(View itemView) {
		super(itemView);
		mViewHolder = new ViewHolderImpl(itemView);
	}

	public Context getContext() {
		return mViewHolder.mItemView.getContext();
	}

	public <T extends View> T findViewById(int viewId) {
		AutoUtils.autoSize(mViewHolder.findViewById(viewId));
		return mViewHolder.findViewById(viewId);
	}

	public <T extends View> T findViewById(int viewId, int pos) {
		AutoUtils.autoSize(mViewHolder.findViewById(viewId, pos));
		return mViewHolder.findViewById(viewId, pos);
	}

	public View getItemView() {
		return mViewHolder.getItemView();
	}

	public void setEditText(int viewId, int stringId) {
		mViewHolder.setEditText(viewId, stringId);
	}

	public void setEditText(int viewId, String string) {
		mViewHolder.setEditText(viewId, string);
	}

	public void setEditHint(int viewId, int stringId) {
		mViewHolder.setEditHint(viewId, stringId);
	}

	public void setEditHint(int viewId, String str) {
		mViewHolder.setEditHint(viewId, str);
	}

	public RecyclerViewHolder setText(int viewId, int stringId) {
		mViewHolder.setText(viewId, stringId);
		return this;
	}

	public RecyclerViewHolder setText(int viewId, String text) {
		mViewHolder.setText(viewId, text);
		return this;
	}

	public RecyclerViewHolder setEditText(int viewId, String text, String tag) {
		mViewHolder.setEditText(viewId, text, tag);
		return this;
	}

	public RecyclerViewHolder setTextColor(int viewId, int color) {
		mViewHolder.setTextColor(viewId, color);
		return this;
	}

	public RecyclerViewHolder setBackgroundResource(int viewId, int resId) {
		mViewHolder.setBackgroundResource(viewId, resId);
		return this;
	}

	public RecyclerViewHolder setBackgroundDrawable(int viewId,
			Drawable drawable) {
		mViewHolder.setBackgroundDrawable(viewId, drawable);
		return this;
	}

	@TargetApi(16)
	public RecyclerViewHolder setBackground(int viewId, Drawable drawable) {
		mViewHolder.setBackground(viewId, drawable);
		return this;
	}

	public RecyclerViewHolder setImageBitmap(int viewId, Bitmap bitmap) {
		mViewHolder.setImageBitmap(viewId, bitmap);
		return this;
	}

	public RecyclerViewHolder setImageResource(int viewId, int resId) {
		mViewHolder.setImageResource(viewId, resId);
		return this;
	}

	public RecyclerViewHolder setImageDrawable(int viewId, Drawable drawable) {
		mViewHolder.setImageDrawable(viewId, drawable);
		return this;
	}

	public RecyclerViewHolder setImageDrawable(int viewId, Uri uri) {
		mViewHolder.setImageDrawable(viewId, uri);
		return this;
	}

	@TargetApi(16)
	public RecyclerViewHolder setImageAlpha(int viewId, float alpha) {
		mViewHolder.setImageAlpha(viewId, alpha);
		return this;
	}

	public RecyclerViewHolder setOnClickListener(int viewId,
			View.OnClickListener clickListener) {
		mViewHolder.setOnClickListener(viewId, clickListener);
		return this;
	}

	public RecyclerViewHolder setOnTouchListener(int viewId,
			View.OnTouchListener touchListener) {
		mViewHolder.setOnTouchListener(viewId, touchListener);
		return this;
	}

	public RecyclerViewHolder setOnLongClickListener(int viewId,
			View.OnLongClickListener longClickListener) {
		mViewHolder.setOnLongClickListener(viewId, longClickListener);
		return this;
	}

	public RecyclerViewHolder setOnItemClickListener(int viewId,
			AdapterView.OnItemClickListener itemClickListener) {
		mViewHolder.setOnItemClickListener(viewId, itemClickListener);
		return this;
	}

	public RecyclerViewHolder setOnItemSelectedClickListener(int viewId,
			AdapterView.OnItemSelectedListener listener) {
		mViewHolder.setOnItemSelectedClickListener(viewId, listener);
		return this;
	}
}
