package classign.election.kingvar.come.electionclasssign.module.main.studentmien.schoolreward;

import classign.election.kingvar.come.electionclasssign.api.RetrofitService;
import classign.election.kingvar.come.electionclasssign.api.info.RewardInfo;
import classign.election.kingvar.come.electionclasssign.module.base.IBasePresenter;
import rx.Subscriber;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/4/1 0001 16:44
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public class SchoolRewardPresenter implements IBasePresenter {
    private final ISchoolRewardView mView;
    private final String mStudentid;
    private final String mType;

    public SchoolRewardPresenter(SchoolRewardFragment starFragment,String mType, String mStudentid) {
        this.mView = starFragment;
        this.mStudentid = mStudentid;
        this.mType = mType;
    }

    @Override
    public void getData(boolean isRefresh) {
        RetrofitService.getRewardInfo(mType,mStudentid)
                .compose(mView.bindToLife())
                .subscribe(new Subscriber<RewardInfo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(RewardInfo rewardInfo) {
                        if (rewardInfo.getCode()==0){
                            mView.loadData(rewardInfo.getWinning());
                        }
                    }
                });
    }

    @Override
    public void getMoreData() {

    }

}
