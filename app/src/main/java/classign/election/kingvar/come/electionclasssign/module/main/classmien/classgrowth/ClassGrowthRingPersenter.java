package classign.election.kingvar.come.electionclasssign.module.main.classmien.classgrowth;

import android.util.Log;

import classign.election.kingvar.come.electionclasssign.api.RetrofitService;
import classign.election.kingvar.come.electionclasssign.api.info.ClassGrowthRingInfo;
import classign.election.kingvar.come.electionclasssign.module.base.IBasePresenter;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/16 0016 15:05
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public class ClassGrowthRingPersenter implements IBasePresenter {
    private static final String TAG = "ClassHonorPersenter";
    private final IClassGrowthRingView mView;
    private final String mClsid;

    public ClassGrowthRingPersenter(ClassGrowthRingFragment classHonourFragment, String clsid) {
        mView = classHonourFragment;
        mClsid = clsid;
        getData(true);
    }


    @Override
    public void getData(boolean isRefresh) {
        Log.i(TAG, "getData: mClsid =  " + mClsid);
       RetrofitService.getClassGrowthRingInfo(mClsid)
                .observeOn(AndroidSchedulers.mainThread())
               .subscribe(new Subscriber<ClassGrowthRingInfo>() {
                   @Override
                   public void onCompleted() {

                   }

                   @Override
                   public void onError(Throwable e) {

                   }

                   @Override
                   public void onNext(ClassGrowthRingInfo classGrowthRingInfo) {
                       if (classGrowthRingInfo!=null && classGrowthRingInfo.getCode()==0){
                           mView.loadClassGrowthRingData(classGrowthRingInfo.getClsring());
                       }
                   }
               });

    }

    @Override
    public void getMoreData() {

    }
}
