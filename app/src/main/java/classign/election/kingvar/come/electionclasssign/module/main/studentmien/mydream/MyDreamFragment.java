package classign.election.kingvar.come.electionclasssign.module.main.studentmien.mydream;


import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import classign.election.kingvar.come.electionclasssign.R;
import classign.election.kingvar.come.electionclasssign.api.info.ProMyDream;
import classign.election.kingvar.come.electionclasssign.base.BaseProtocol;
import classign.election.kingvar.come.electionclasssign.injector.components.DaggerMyDreamComponent;
import classign.election.kingvar.come.electionclasssign.injector.modules.MyDreamModule;
import classign.election.kingvar.come.electionclasssign.module.base.BaseFragment;
import classign.election.kingvar.come.electionclasssign.module.base.IBasePresenter;
import classign.election.kingvar.come.electionclasssign.tools.bean.User;

/**
 * 我的梦想
 * Created by Administrator on 2016/12/20 0020.
 */

public class MyDreamFragment extends BaseFragment<IBasePresenter> implements IMyDreamView {
    private static final String TAG = "MyDreamFragment";
    private static final int FINSH = 1;
    @BindView(R.id.iv_student_head)
    ImageView iv_student_head; //头像
    @BindView(R.id.iv_dream_pic)
    ImageView iv_dream_pic; //我的梦想图片
    @BindView(R.id.tv_student_name)
    TextView tv_student_name;  //姓名
    @BindView(R.id.tv_my_dream)
    TextView tv_my_dream;  //梦想宣言


    @Override
    protected int attachLayoutRes() {
        return R.layout.fragment_my_dream;
    }

    @Override
    protected void initInjector() {
        Log.i(TAG, "initInjector: studentid = " + User.getStudentId(getActivity()));
        DaggerMyDreamComponent.builder()
                .applicationComponent(getAppComponent())
                .myDreamModule(new MyDreamModule(this, User.getStudentId(getActivity())))
                .build()
                .inject(this);
    }

    @Override
    protected void initViews() {
    }

    @Override
    public void updateViews(boolean isRefresh) {
        mPresenter.getData(true);
    }

    @Override
    public void loadData(ProMyDream.DreamBean dreamBean) {
        Glide.with(getActivity()).load(BaseProtocol.IMG_BASE + User.getTodayStarLogo(getActivity()))
                .error(R.drawable.default_pic).into(iv_student_head);
        tv_student_name.setText(User.getTodayStarName(getActivity()));
        Glide.with(getActivity())
                .load(BaseProtocol.IMG_BASE + dreamBean.getImg())
                .error(R.drawable.default_pic)
                .into(iv_dream_pic);
        tv_my_dream.setText(dreamBean.getDetail());
    }
}
