package classign.election.kingvar.come.electionclasssign.injector.modules;

import android.util.Log;

import classign.election.kingvar.come.electionclasssign.activity.ToadyAttendanceActivity;
import classign.election.kingvar.come.electionclasssign.adapter.ViewPagerAdapter;
import classign.election.kingvar.come.electionclasssign.injector.PerActivity;
import classign.election.kingvar.come.electionclasssign.module.base.IBasePresenter;
import classign.election.kingvar.come.electionclasssign.module.main.moraleducation.title.AttendanceTitlePersenter;
import classign.election.kingvar.come.electionclasssign.tools.bean.User;
import dagger.Module;
import dagger.Provides;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/16 0016 15:28
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */
@Module
public class AttendanceTitleModule {
    private static final String TAG = "AttendanceRecoderModule";
    private final  ToadyAttendanceActivity mTodayAttendanceActivity;

    public AttendanceTitleModule(ToadyAttendanceActivity toadyAttendanceActivity) {
        this.mTodayAttendanceActivity=toadyAttendanceActivity;
    }

    @PerActivity
    @Provides
    public IBasePresenter provideTitlePresenter() {
        Log.i(TAG, "provideMainPresenter: 1231654646");
        return new AttendanceTitlePersenter(mTodayAttendanceActivity, User.getSchoolId(mTodayAttendanceActivity),User.getClassId(mTodayAttendanceActivity));
    }

    @PerActivity
    @Provides
    public ViewPagerAdapter provideViewPagerAdapter() {
        return new ViewPagerAdapter(mTodayAttendanceActivity.getSupportFragmentManager());
    }
}
