package classign.election.kingvar.come.electionclasssign.module.main.studentmien.todaystar;


import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import classign.election.kingvar.come.electionclasssign.R;
import classign.election.kingvar.come.electionclasssign.api.info.ProToadyStar;
import classign.election.kingvar.come.electionclasssign.base.BaseProtocol;
import classign.election.kingvar.come.electionclasssign.injector.components.DaggerTodayStarComponent;
import classign.election.kingvar.come.electionclasssign.injector.modules.TodayStarModule;
import classign.election.kingvar.come.electionclasssign.module.base.BaseFragment;
import classign.election.kingvar.come.electionclasssign.module.base.IBasePresenter;
import classign.election.kingvar.come.electionclasssign.tools.bean.User;
import classign.election.kingvar.come.electionclasssign.tools.utils.TypeFaceUtil;

/**
 * 今日之星
 * Created by Administrator on 2016/12/20 0020.
 */

public class TodayStarFragment extends BaseFragment<IBasePresenter> implements ITodayStarView {
	private static final String TAG = "TodayStarFragment";
	@BindView(R.id.tv_motto_content)
	TextView tv_motto_content; //座右铭
	@BindView(R.id.tv_student_name)
	TextView tv_student_name; //姓名
	@BindView(R.id.iv_student_head)
	ImageView iv_student_head;  //头像

	@Override
	protected int attachLayoutRes() {
		return R.layout.fragment_today_star;
	}

	@Override
	protected void initInjector() {
		Log.i(TAG, "initInjector: classid = "+ User.getClassId(getActivity()) );
		DaggerTodayStarComponent.builder()
				.applicationComponent(getAppComponent())
				.todayStarModule(new TodayStarModule(this,User.getClassId(getActivity())))
				.build()
				.inject(this);

	}

	@Override
	protected void initViews() {
		tv_motto_content.setTypeface(TypeFaceUtil.initTypeface(getActivity()));//设置字体

	}

	@Override
	public void updateViews(boolean isRefresh) {
		mPresenter.getData(isRefresh);
	}


	@Override
	public void loadData(ProToadyStar.InfoBean infoBean) {
		if (infoBean != null){
			User.setStudentId(getActivity(), infoBean.getStudentid());
			User.setTodayStarName(getActivity(), infoBean.getName());
			User.setTodayStarLogo(getActivity(), infoBean.getLogo());
			tv_student_name.setText(infoBean.getName());
			tv_motto_content.setText(infoBean.getMemo());
			Glide.with(getActivity()).load(BaseProtocol.IMG_BASE + infoBean.getLogo())
					.error(R.drawable.default_pic)
					.into(iv_student_head);
		}
	}
}
