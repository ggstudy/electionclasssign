package classign.election.kingvar.come.electionclasssign.module.main.classmien.classgrowth;

import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import classign.election.kingvar.come.electionclasssign.R;
import classign.election.kingvar.come.electionclasssign.api.info.ClassGrowthRingInfo;
import classign.election.kingvar.come.electionclasssign.module.base.BaseFragment;
import classign.election.kingvar.come.electionclasssign.module.base.IBasePresenter;
import classign.election.kingvar.come.electionclasssign.tools.utils.DateUtil;

/**
 * 班级年轮 的viewpager 的item的 fragment
 * Created by jyq-t112 on 2016/12/27.
 */

public class ClassRingViewPagerFragment extends BaseFragment<IBasePresenter> {
    private static final String TAG = "ClassFragment";
    @BindView(R.id.tv_term_one)
    TextView tv_term_one;

    @BindView(R.id.tv_term_two)
    TextView tv_term_two;

    @BindView(R.id.tv_term_three)
    TextView tv_term_three;

    @BindView(R.id.tv_term_four)
    TextView tv_term_four;

    @BindView(R.id.tv_term_five)
    TextView tv_term_five;

    @BindView(R.id.tv_year_one)
    TextView tv_year_one;

    @BindView(R.id.tv_year_two)
    TextView tv_year_two;

    @BindView(R.id.tv_year_three)
    TextView tv_year_three;

    @BindView(R.id.tv_year_four)
    TextView tv_year_four;

    @BindView(R.id.tv_year_five)
    TextView tv_year_five;

    @BindView(R.id.tv_detail_one)
    TextView tv_detail_one;

    @BindView(R.id.tv_detail_two)
    TextView tv_detail_two;

    @BindView(R.id.tv_detail_three)
    TextView tv_detail_three;

    @BindView(R.id.tv_detail_four)
    TextView tv_detail_four;

    @BindView(R.id.tv_detail_five)
    TextView tv_detail_five;

    private List<ClassGrowthRingInfo.ClsringEntity> clsringBeen;
    private int position;

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }


    public List<ClassGrowthRingInfo.ClsringEntity> getClsringBeen() {
        return clsringBeen;
    }

    public void setClsringBeen(List<ClassGrowthRingInfo.ClsringEntity> clsringBeen) {
        this.clsringBeen = clsringBeen;
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.fragment_class;
    }

    @Override
    protected void initInjector() {

    }

    @Override
    protected void initViews() {
        if (clsringBeen.size() > position * 5 + 0) {

            tv_term_one.setText(clsringBeen.get(position * 5+0).getTitle());
            tv_year_one.setText(DateUtil.getYearByTimeStamp(clsringBeen.get(position * 5+0).getDate())+"");
            tv_detail_one.setText(clsringBeen.get(position * 5+0).getDetail());
        }

        if (clsringBeen.size() > position * 5 + 1) {

            tv_term_two.setText(clsringBeen.get(position * 5 + 1).getTitle());
            tv_year_two.setText(DateUtil.getYearByTimeStamp(clsringBeen.get(position * 5 + 1).getDate())+"");
            tv_detail_two.setText(clsringBeen.get(position * 5 + 1).getDetail());
        }

        if (clsringBeen.size() > position * 5 + 2) {

            tv_term_three.setText(clsringBeen.get(position * 5 + 2).getTitle());
            tv_year_three.setText(DateUtil.getYearByTimeStamp(clsringBeen.get(position * 5 + 2).getDate())+"");
            tv_detail_three.setText(clsringBeen.get(position * 5 + 2).getDetail());
        }

        if (clsringBeen.size() > position * 5 + 3) {

            tv_term_four.setText(clsringBeen.get(position * 5 + 3).getTitle());
            tv_year_four.setText(DateUtil.getYearByTimeStamp(clsringBeen.get(position * 5 + 3).getDate())+"");
            tv_detail_four.setText(clsringBeen.get(position * 5 + 3).getDetail());
        }

        if (clsringBeen.size() > position * 5 + 4) {

            tv_term_five.setText(clsringBeen.get(position * 5 + 4).getTitle());
            tv_year_five.setText(DateUtil.getYearByTimeStamp(clsringBeen.get(position * 5 + 4).getDate())+"");
            tv_detail_five.setText(clsringBeen.get(position * 5 + 4).getDetail());
        }
    }

    @Override
    protected void updateViews(boolean isRefresh) {

    }
}
