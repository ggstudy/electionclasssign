package classign.election.kingvar.come.electionclasssign.module.main.studentmien.todaystar;

import classign.election.kingvar.come.electionclasssign.api.info.ProToadyStar;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/16 0016 14:56
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public interface ITodayStarView {
    /**
     * 显示数据
     */
    void loadData(ProToadyStar.InfoBean infoBean);

}
