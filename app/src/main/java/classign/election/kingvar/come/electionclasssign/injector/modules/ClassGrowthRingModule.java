package classign.election.kingvar.come.electionclasssign.injector.modules;

import classign.election.kingvar.come.electionclasssign.injector.PerFragment;
import classign.election.kingvar.come.electionclasssign.module.base.IBasePresenter;
import classign.election.kingvar.come.electionclasssign.module.main.classmien.classgrowth.ClassGrowthRingFragment;
import classign.election.kingvar.come.electionclasssign.module.main.classmien.classgrowth.ClassGrowthRingPersenter;
import dagger.Module;
import dagger.Provides;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/16 0016 15:28
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */
@Module
public class ClassGrowthRingModule {
    private static final String TAG = "AttendanceRecoderModule";
    private final ClassGrowthRingFragment mClassGrowthRingFragment;
    private final  String mClsid;
    public ClassGrowthRingModule(ClassGrowthRingFragment fragment, String clsid) {
        this.mClassGrowthRingFragment = fragment;
        this.mClsid = clsid;
    }



    @PerFragment
    @Provides
    public IBasePresenter provideMainPresenter() {
        return new ClassGrowthRingPersenter(mClassGrowthRingFragment,mClsid);
    }

}
