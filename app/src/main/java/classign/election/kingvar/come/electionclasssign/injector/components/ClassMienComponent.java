package classign.election.kingvar.come.electionclasssign.injector.components;


import classign.election.kingvar.come.electionclasssign.injector.PerActivity;
import classign.election.kingvar.come.electionclasssign.injector.modules.ClassMienActivityModule;
import classign.election.kingvar.come.electionclasssign.module.main.classmien.ClassMienActivity;
import dagger.Component;

/**
 * Created by long on 2016/8/19.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {ClassMienActivityModule.class})
public interface ClassMienComponent {
  void inject(ClassMienActivity classMienActivity);
}