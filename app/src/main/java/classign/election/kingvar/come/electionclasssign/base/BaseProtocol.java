package classign.election.kingvar.come.electionclasssign.base;


import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

/**
 * Created by Administrator on 2017/1/3 0003.
 */

public class BaseProtocol {

    public static final String HOST_BASE = "http://poopboo.com:8086/brand/api/";
    public static final String  TEST_BASE = "http://120.76.155.123:8081/brand/api/";
    //图片加载 接口
//    public static final String IMG_BASE = "http://120.76.155.123:8081/brand/uploads/normal/";
    public static final String IMG_BASE = "http://poopboo.com:8086/brand/uploads/normal/";

    //Query?key=ad4cff6691e245aaabc18f082fb62f3e
    public static final String LocationInfo = "http://wthrcdn.etouch.cn/";

    //考勤 上传数据接口
    public static final String KAO_QIN = "http://poopboo.com:8086/brand/kaoqin/";
//    public static final String KAO_QIN = "http://192.168.1.168:8081/brand/kaoqin/";

    /**
     * okhttp3 get异步
     *
     * @param url
     * @return
     */
    public static Call getAsynHttp(String url) {
        OkHttpClient mOkHttpClient = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .build();
        Call call = mOkHttpClient.newCall(request);
        return call;
    }

    /**
     * okhttp3 post异步
     * @param formBody
     * @param url
     * @return
     */
    public static Call postAsynHttp(String url, RequestBody formBody) {
        OkHttpClient okHttpClient = new OkHttpClient();
        Request request = new Request.Builder().url(url).post(formBody).build();
        Call call = okHttpClient.newCall(request);
        return call;
    }
}
