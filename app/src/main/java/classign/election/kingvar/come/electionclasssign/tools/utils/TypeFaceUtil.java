package classign.election.kingvar.come.electionclasssign.tools.utils;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;

/**
 * Created by Administrator on 2016/12/23 0023.
 */

public class TypeFaceUtil {
	/**
	 * 初始化字体库
	 * 叶根友毛笔行书
	 * @param context
	 */
	public static Typeface initTypeface(Context context) {
		AssetManager asset = context.getAssets();
		Typeface typeface = Typeface.createFromAsset(asset, "fonts/typeFaceTwo.ttf");//根据路径得到Typeface
		return typeface;
	}
}
