package classign.election.kingvar.come.electionclasssign.injector.modules;

import classign.election.kingvar.come.electionclasssign.injector.PerFragment;
import classign.election.kingvar.come.electionclasssign.module.base.IBasePresenter;
import classign.election.kingvar.come.electionclasssign.module.main.moraleducation.one.AttendanceRecoderOnePersenter;
import classign.election.kingvar.come.electionclasssign.module.main.moraleducation.one.TodayAttendanceOneFragment;
import dagger.Module;
import dagger.Provides;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/16 0016 15:28
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */
@Module
public class AttendanceRecoderOneModule {
    private static final String TAG = "AttendanceRecoderModule";
    private final TodayAttendanceOneFragment mTodayAttendanceFragment;
    private final  String mClsid;
    private final  String mTid;
    public AttendanceRecoderOneModule(TodayAttendanceOneFragment fragment, String clsid, String tid) {
        this.mTodayAttendanceFragment = fragment;
        this.mClsid = clsid;
        this.mTid = tid;
    }



    @PerFragment
    @Provides
    public IBasePresenter provideMainPresenter() {
        return new AttendanceRecoderOnePersenter(mTodayAttendanceFragment,mClsid,mTid);
    }

}
