package classign.election.kingvar.come.electionclasssign.tools.bean;

import android.content.Context;

import classign.election.kingvar.come.electionclasssign.tools.utils.SPUtil;


public class User {
	public static final boolean isTest = true;
	private static final String isOnline = "isOnline";
	private static final String userId = "userId";

	private static final String schoolId = "schoolId";  //学校id
	private static final String classId = "classId"; //班级id

	private static final String imei = "imei"; //设备唯一编码号
	private static final String classTeacherOne = "classTeacherOne"; //班主任
	private static final String classTeacherTwo = "classTeacherTwo"; //副班主任
	private static final String classMonitor = "classMonitor"; //班长
	private static final String className = "className"; //班级名称
	private static final String studentId = "studentId";
	private static final String todayStarName = "todayStarName";
	private static final String todayStarLogo = "todayStarLogo";
	private static final String location = "location";


	/**
	 * 设备唯一编码号
	 */
	public static String getImei(Context context) {
		return SPUtil.getString(context, User.imei, null);
	}

	/**
	 * 设备唯一编码号
	 */
	public static void setImei(Context context, String imei) {
		SPUtil.putString(context, User.imei, imei);
	}
	/**
	 * 今日之星的Logo
	 */
	public static String getTodayStarLogo(Context context) {
		return SPUtil.getString(context, User.todayStarLogo, null);
	}

	/**
	 * 今日之星的Logo
	 */
	public static void setTodayStarLogo(Context context, String todayStarLogo) {
		SPUtil.putString(context, User.todayStarLogo, todayStarLogo);
	}
	/**
	 * 今日之星的名字
	 */
	public static String getTodayStarName(Context context) {
		return SPUtil.getString(context, User.todayStarName, null);
	}

	/**
	 * 今日之星的名字
	 */
	public static void setTodayStarName(Context context, String todayStarName) {
		SPUtil.putString(context, User.todayStarName, todayStarName);
	}
	/**
	 * 获取地理位置
	 */
	public static String getLocation(Context context) {
		return SPUtil.getString(context, User.location, null);
	}

	/**
	 * 设置地理位置
	 */
	public static void setLocation(Context context, String location) {
		SPUtil.putString(context, User.location, location);
	}
	/**
	 * 班主任
	 */
	public static String getClassTeacherOne(Context context) {
		return SPUtil.getString(context, User.classTeacherOne, null);
	}

	/**
	 * 班主任
	 */
	public static void setClassTeacherOne(Context context, String classTeacherOne) {
		SPUtil.putString(context, User.classTeacherOne, classTeacherOne);
	}

	/**
	 * 副班主任
	 */
	public static String getClassTeacherTwo(Context context) {
		return SPUtil.getString(context, User.classTeacherTwo, null);
	}

	/**
	 * 副班主任
	 */
	public static void setClassTeacherTwo(Context context, String classTeacherTwo) {
		SPUtil.putString(context, User.classTeacherTwo, classTeacherTwo);
	}

	/**
	 * 班长
	 */
	public static String getClassMonitor(Context context) {
		return SPUtil.getString(context, User.classMonitor, null);
	}

	/**
	 * 班长
	 */
	public static void setClassMonitor(Context context, String classMonitor) {
		SPUtil.putString(context, User.classMonitor, classMonitor);
	}

	/**
	 * 班级名称
	 */
	public static String getClassName(Context context) {
		return SPUtil.getString(context, User.className, null);
	}

	/**
	 * 班级名称
	 */
	public static void setClassName(Context context, String className) {
		SPUtil.putString(context, User.className, className);
	}

	/**
	 * 保存学校id
	 **/
	public static boolean setSchoolId(Context context, String schoolId) {
		return SPUtil.putString(context, User.schoolId, schoolId);
	}

	/**
	 * 得到学校id
	 **/
	public static String getSchoolId(Context context) {
		return SPUtil.getString(context, User.schoolId);
	}

	/**
	 * 保存班级id
	 **/
	public static boolean setClassId(Context context, String classid) {
		return SPUtil.putString(context, User.classId, classid);
	}

	/**
	 * 得到班级id
	 **/
	public static String getClassId(Context context) {
		return SPUtil.getString(context, User.classId);
	}

	/**
	 * 学生id
	 **/
	public static boolean setStudentId(Context context, String studentId) {
		return SPUtil.putString(context, User.studentId, studentId);
	}

	/**
	 * 学生id
	 **/
	public static String getStudentId(Context context) {
		return SPUtil.getString(context, User.studentId);
	}

	/**
	 * 用户id
	 **/
	public static boolean setUserID(Context context, String userId) {
		return SPUtil.putString(context, User.userId, userId);
	}

	/**
	 * 用户id
	 **/
	public static String getUserId(Context context) {
		return SPUtil.getString(context, User.userId);
	}

	/**
	 * 当前用户在线
	 */
	public static boolean getIsOnline(Context context) {
		return SPUtil.getBoolean(context, User.isOnline, false);
	}

	/**
	 * 当前用户在线
	 */
	public static boolean setIsOnline(Context context, boolean isOnline) {
		return SPUtil.putBoolean(context, User.isOnline, isOnline);
	}

	/**
	 * 清空所有 共享存储数据
	 */
	public static void logout(Context context) {

		SPUtil.remove(context, User.classId);
		SPUtil.remove(context, User.className);
		SPUtil.remove(context, User.isOnline);
		SPUtil.remove(context, User.schoolId);
		SPUtil.remove(context, User.classTeacherOne);
		SPUtil.remove(context, User.classTeacherTwo);
		SPUtil.remove(context, User.userId);
		SPUtil.remove(context, User.imei);
		SPUtil.remove(context, User.classMonitor);
		/*SPUtil.remove(context, User.userNickName);
		SPUtil.remove(context, User.userPhoto);
		SPUtil.remove(context, User.userPwd);
		SPUtil.remove(context, User.userSex);
		SPUtil.remove(context, User.userSign);
		SPUtil.remove(context, User.userMiMa);*/
	}
/*
	*/
/**保存老师id**//*

	public static boolean setTeacherId(Context context, String teacherid){
		return SPUtil.putString(context, User.teacherId, teacherid);
	}

	*/
/**得到老师id**//*

	public static String getTeacherId(Context context){
		return SPUtil.getString(context, User.teacherId);
	}

	*/
/**保存强制更新**//*

	public static boolean setIsforceFlag(Context context, boolean isforce){
		return SPUtil.putBoolean(context, User.forceFlag, isforce);
	}
	*/
/**保存老师id**//*

	public static boolean getIsforceFlag(Context context){
		return SPUtil.getBoolean(context, User.forceFlag);
	}
	*/
/**用户登录账号userid*//*

	public static String getUserLoginAccount(Context context){
		return SPUtil.getString(context, User.loginAccount,null);
	}
	*/
/**用户登录账号userid*//*

	public static void setUserLoginAccount(Context context, String pwd){
		SPUtil.putString(context, User.loginAccount, pwd);
	}
	*/
/**用户登录账号 手机号码*//*

	public static String getUserPhone(Context context){
		return SPUtil.getString(context, User.userPhone,null);
	}
	*/
/**用户登录账号手机号码*//*

	public static void setUserPhone(Context context, String phone){
		SPUtil.putString(context, User.userPhone, phone);
	}

	*/
/**用户 密码*//*

	public static String getUserPwd(Context context){
		return SPUtil.getString(context, User.userPwd,null);
	}
	*/
/**用户 密码*//*

	public static void setUserPwd(Context context, String pwd){
		SPUtil.putString(context, User.userPwd, pwd);
	}
	*/
/**手机号*//*

	public static String getUserPhoneNum(Context context){
		return SPUtil.getString(context, User.userPhoneNum,null);
	}
	*/
/**手机号*//*

	public static void setUserPhoneNum(Context context, String phoneNum){
		SPUtil.putString(context, User.userPhoneNum, phoneNum);
	}
	
	
	*/
/**车型号 *//*

	public static String getUserCarStyle(Context context){
		return SPUtil.getString(context, User.userCarStyle,null);
	}
	*/
/**车型号 *//*

	public static void setUserCarStyle(Context context, String userToken){
		SPUtil.putString(context, User.userCarStyle, userToken);
	}
	

	*/
/**车牌号 *//*

	public static String getUserCarNum(Context context){
		return SPUtil.getString(context, User.userCarNum,null);
	}
	*/
/**车牌号 *//*

	public static void setUserCarNum(Context context, String userToken){
		SPUtil.putString(context, User.userCarNum, userToken);
	}
	
	*/
/**用户昵称*//*

	public static String getUserNickName(Context context){
		return SPUtil.getString(context, User.userNickName,null);
	}
	*/
/**用户昵称*//*

	public static void setUserNickName(Context context, String nickName){
		SPUtil.putString(context, User.userNickName, nickName);
	}
	*/
/**用户头像*//*

	public static String getUserPhoto(Context context){
		return SPUtil.getString(context, User.userPhoto,null);
	}
	*/
/**用户头像*//*

	public static void setUserPhoto(Context context, String nickName){
		SPUtil.putString(context, User.userPhoto, nickName);
	}
	*/
/**性别*//*

	public static String getUserSex(Context context){
		return SPUtil.getString(context, User.userSex,null);
	}
	*/
/**性别*//*

	public static void setUserSex(Context context, String gender){
		SPUtil.putString(context, User.userSex, gender);
	}
	*/
/**Cookies*//*

	public static String getUserSign(Context context){
		return SPUtil.getString(context, User.userSign,null);
	}
	*/
/**Cookies*//*

	public static void setUserSign(Context context, String gender){
		SPUtil.putString(context, User.userSign, gender);
	}
*/


}
