package classign.election.kingvar.come.electionclasssign.injector.components;


import classign.election.kingvar.come.electionclasssign.injector.PerActivity;
import classign.election.kingvar.come.electionclasssign.injector.modules.StudentMienModule;
import classign.election.kingvar.come.electionclasssign.module.main.studentmien.StudentMienActivity;
import dagger.Component;

/**
 * Created by long on 2016/8/19.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {StudentMienModule.class})
public interface StudentMienComponent {
  void inject(StudentMienActivity studentMienActivity);
}