package classign.election.kingvar.come.electionclasssign.adapter;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.github.ybq.android.spinkit.SpinKitView;

import java.util.ArrayList;
import java.util.List;

import classign.election.kingvar.come.electionclasssign.R;
import classign.election.kingvar.come.electionclasssign.api.info.ClassHonorInfo;
import classign.election.kingvar.come.electionclasssign.base.BaseProtocol;



/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/23 0023 14:56
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public class ClassHonorPagerAdapter extends PagerAdapter {
    private static final String TAG = "ClassHonorPagerAdapter";
    Context activity;
    private List<ClassHonorInfo.HonourEntity> mHonorList = new ArrayList<ClassHonorInfo.HonourEntity>();

    public ClassHonorPagerAdapter(List<ClassHonorInfo.HonourEntity> honour, Context activity) {
        this.mHonorList = honour;
        this.activity = activity;
    }

    public void setItemsInfo(List<ClassHonorInfo.HonourEntity> honour, FragmentActivity activity) {
        if (honour.size() <= 0) {
            mHonorList.clear();
            notifyDataSetChanged();
            return;
        }
        mHonorList.clear();
        mHonorList.addAll(honour);
        notifyDataSetChanged();
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return mHonorList.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ViewHolder viewHolder = null;
        View view =null;
        if (view ==null){
            view = LayoutInflater.from(activity).inflate(R.layout.layout_monthly_bills_item, null);
        }
        if (viewHolder == null) {
            viewHolder = new ViewHolder();
        }
        viewHolder.welfareImage = (ImageView) view.findViewById(R.id.welfare_image);
        viewHolder.loading_view = (SpinKitView) view.findViewById(R.id.loading_view);
        ViewHolder finalViewHolder = viewHolder;
        Glide.with(activity).load(BaseProtocol.IMG_BASE + mHonorList.get(position).getImg()).into(new GlideDrawableImageViewTarget(finalViewHolder.welfareImage){
            @Override
            public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> animation) {
                finalViewHolder.loading_view.setVisibility(View.GONE);
                finalViewHolder.welfareImage.setImageDrawable(resource);
                super.onResourceReady(resource, animation);
            }
        });
        container.addView(view);
        return view;
    }

    class ViewHolder {
        ImageView welfareImage;
        SpinKitView loading_view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
}
