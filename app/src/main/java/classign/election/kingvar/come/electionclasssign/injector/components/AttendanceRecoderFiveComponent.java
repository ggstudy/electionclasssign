package classign.election.kingvar.come.electionclasssign.injector.components;


import classign.election.kingvar.come.electionclasssign.injector.PerFragment;
import classign.election.kingvar.come.electionclasssign.injector.modules.AttendanceRecoderFiveModule;
import classign.election.kingvar.come.electionclasssign.module.main.moraleducation.five.TodayAttendanceFiveFragment;
import dagger.Component;

/**
 * Created by long on 2016/8/19.
 */
@PerFragment
@Component(dependencies = ApplicationComponent.class, modules = {AttendanceRecoderFiveModule.class})
public interface AttendanceRecoderFiveComponent {
  void inject(TodayAttendanceFiveFragment fragment);
}