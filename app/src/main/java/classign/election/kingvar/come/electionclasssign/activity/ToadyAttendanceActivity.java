package classign.election.kingvar.come.electionclasssign.activity;


import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import classign.election.kingvar.come.electionclasssign.R;
import classign.election.kingvar.come.electionclasssign.adapter.ViewPagerAdapter;
import classign.election.kingvar.come.electionclasssign.api.info.TitleInfo;
import classign.election.kingvar.come.electionclasssign.injector.components.DaggerAttendanceTitleComponent;
import classign.election.kingvar.come.electionclasssign.module.base.BaseActivity;
import classign.election.kingvar.come.electionclasssign.injector.modules.AttendanceTitleModule;
import classign.election.kingvar.come.electionclasssign.module.main.moraleducation.title.IAttendanceTitleView;


/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/15 0015 10:28
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public class ToadyAttendanceActivity extends BaseActivity implements IAttendanceTitleView, View.OnClickListener {
    private static final String TAG = "ToadyAttendanceActivity";
    @BindView(R.id.tab_layout)
    TabLayout mTabLayout;
    @BindView(R.id.view_pager)
    ViewPager mViewPager;
    @BindView(R.id.tv_line_2)
    TextView mTvGetScore;
    @BindView(R.id.tv_line_3)
    TextView mEvaluate;  //评价
    @BindView(R.id.btn_close)
    Button mBtnClose;
    @BindView(R.id.tv_close)
    TextView mTvClose;
    @Inject
    ViewPagerAdapter mPagerAdapter;

    @Override
    protected int attachLayoutRes() {
        return R.layout.activity_today_attendance;
    }

    @Override
    protected void initInjector() {
        DaggerAttendanceTitleComponent.builder()
                .applicationComponent(getAppComponent())
                .attendanceTitleModule(new AttendanceTitleModule(this))
                .build()
                .inject(this);
    }

    @Override
    protected void initViews() {
        mBtnClose.setOnClickListener(this);
        mTvClose.setOnClickListener(this);
    }

    @Override
    protected void updateViews(boolean isRefresh) {
        mPresenter.getData(isRefresh);
    }

    @Override
    public void loadTitleData(TitleInfo titleInfo) {
        Log.i(TAG, "loadTitleData: id = " + titleInfo.getData().size());
        mTvGetScore.setText("本周得分 :" + titleInfo.getSum() + "分");
        setEvaluate(titleInfo.getSum());
        List<String> titleList = new ArrayList();
        List<String> mTitleIdList = new ArrayList<>();
        for (int i = 0; i < titleInfo.getData().size(); i++) {
            titleList.add(titleInfo.getData().get(i).getTitle());
            mTitleIdList.add(titleInfo.getData().get(i).getId() + "");
        }
        mPagerAdapter.setItemsTitle(titleList, mTitleIdList);
        mViewPager.setAdapter(mPagerAdapter);
        mTabLayout.setupWithViewPager(mViewPager);
    }

    /**
     * 设置评价内容
     *
     * @param sum
     */
    private void setEvaluate(int sum) {
        if (sum >= 90) {
            mEvaluate.setText("德育处评价 :本周表现优，希望下周继续保持!");
        } else if (sum >= 80 && sum < 90) {
            mEvaluate.setText("德育处评价 :本周表现良,希望下周更努力!");
        } else if (sum >= 60 && sum < 80) {
            mEvaluate.setText("德育处评价 :本周表现中,希望下周更努力!");
        } else if (sum < 60 && sum >= 0) {
            mEvaluate.setText("德育处评价 :本周表现差,希望下周更努力!");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_close:
            case R.id.tv_close:
                finish();
                break;

            default:
                break;
        }
    }
}
