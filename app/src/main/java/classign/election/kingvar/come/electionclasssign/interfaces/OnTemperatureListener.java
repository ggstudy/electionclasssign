package classign.election.kingvar.come.electionclasssign.interfaces;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/2/28 0028 10:58
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public interface OnTemperatureListener {

    void onTemperature(String img, String temperature);
}
