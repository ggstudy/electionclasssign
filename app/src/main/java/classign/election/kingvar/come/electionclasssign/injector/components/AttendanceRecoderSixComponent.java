package classign.election.kingvar.come.electionclasssign.injector.components;


import classign.election.kingvar.come.electionclasssign.injector.PerFragment;
import classign.election.kingvar.come.electionclasssign.injector.modules.AttendanceRecoderSixModule;
import classign.election.kingvar.come.electionclasssign.module.main.moraleducation.six.TodayAttendanceSixFragment;
import dagger.Component;

/**
 * Created by long on 2016/8/19.
 */
@PerFragment
@Component(dependencies = ApplicationComponent.class, modules = {AttendanceRecoderSixModule.class})
public interface AttendanceRecoderSixComponent {
  void inject(TodayAttendanceSixFragment fragment);
}