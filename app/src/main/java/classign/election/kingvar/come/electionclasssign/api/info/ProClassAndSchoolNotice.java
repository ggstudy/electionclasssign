package classign.election.kingvar.come.electionclasssign.api.info;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/6 0006 11:07
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public class ProClassAndSchoolNotice implements  Parcelable{
    /**
     * schoolInfo : [{"picturepath":"","uname":"快来","logo":"20160925122729431573.jpg","lmfTm":1490349598000,"detail":"&lt;span style=&quot;font-family:黑体;&quot;&gt;&lt;span style=&quot;font-size: 25px;&quot;&gt;好&lt;/span&gt;&lt;/span&gt;","time":1483411701000,"title":"调查报告显示：校外辅导教师亟待&ldquo;标准&rdquo;认定","noticeId":6}]
     * code : 0
     * clsNotice : [{"uname":"快来","logo":"20160925122729431573.jpg","lmfTm":1490869318000,"detail":"\t\t\t\t\t\t天气转凉了，请家长及时为孩子更换被褥。<br />","time":1474978829000,"title":"班级通知","noticeId":1},{"uname":"快来","logo":"20160925122729431573.jpg","lmfTm":1490869313000,"detail":"\t\t\t\t\t\t没有安装娃娃的家长请安装一下<br />","time":1474980468000,"title":"通知","noticeId":2},{"uname":"快来","logo":"20160925122729431573.jpg","lmfTm":1490869310000,"detail":"\t\t\t\t\t\t9月28日全区学校停课一天，请互相转告<br />","time":1474981641000,"title":"台风放假   通知","noticeId":3},{"uname":"快来","logo":"20160925122729431573.jpg","lmfTm":1490869307000,"detail":"\t\t\t\t\t\t各位家长朋友注意：今日开始，金娃娃里面的\u201c家园沟通\u201d开始使用，若孩子有什么特殊情况，家长可以跟老师直接单独交流，沟通。（上班时间请勿打扰）。<br />各位家长朋友注意：今日开始，金娃娃里面的\u201c家园沟通\u201d开始使用，若孩子有什么特殊情况，家长可以跟老师直接单独交流，沟通。（上班时间请勿打扰）。<br />","time":1478572287000,"title":"班级通知","noticeId":4},{"picturepath":"","uname":"快来","logo":"20160925122729431573.jpg","lmfTm":1490869303000,"detail":"\t\t\t\t\t\t<span style=\"font-family:黑体;font-size: 25px; line-height: 40px; text-align: center; widows: 1;\">深圳新招中小学教师或提高门槛：中学至少硕士<\/span>","time":1483411728000,"title":"深圳新招中小学教师或提高门槛：中学至少硕士","noticeId":7}]
     */

    private int code;
    private List<SchoolInfoEntity> schoolInfo;
    private List<ClsNoticeEntity> clsNotice;

    protected ProClassAndSchoolNotice(Parcel in) {
        code = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(code);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ProClassAndSchoolNotice> CREATOR = new Creator<ProClassAndSchoolNotice>() {
        @Override
        public ProClassAndSchoolNotice createFromParcel(Parcel in) {
            return new ProClassAndSchoolNotice(in);
        }

        @Override
        public ProClassAndSchoolNotice[] newArray(int size) {
            return new ProClassAndSchoolNotice[size];
        }
    };

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<SchoolInfoEntity> getSchoolInfo() {
        return schoolInfo;
    }

    public void setSchoolInfo(List<SchoolInfoEntity> schoolInfo) {
        this.schoolInfo = schoolInfo;
    }

    public List<ClsNoticeEntity> getClsNotice() {
        return clsNotice;
    }

    public void setClsNotice(List<ClsNoticeEntity> clsNotice) {
        this.clsNotice = clsNotice;
    }

    public static class SchoolInfoEntity implements Parcelable{
        /**
         * picturepath :
         * uname : 快来
         * logo : 20160925122729431573.jpg
         * lmfTm : 1490349598000
         * detail : &lt;span style=&quot;font-family:黑体;&quot;&gt;&lt;span style=&quot;font-size: 25px;&quot;&gt;好&lt;/span&gt;&lt;/span&gt;
         * time : 1483411701000
         * title : 调查报告显示：校外辅导教师亟待&ldquo;标准&rdquo;认定
         * noticeId : 6
         */

        private String picturepath;
        private String uname;
        private String logo;
        private long lmfTm;
        private String detail;
        private long time;
        private String title;
        private int noticeId;

        protected SchoolInfoEntity(Parcel in) {
            picturepath = in.readString();
            uname = in.readString();
            logo = in.readString();
            lmfTm = in.readLong();
            detail = in.readString();
            time = in.readLong();
            title = in.readString();
            noticeId = in.readInt();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(picturepath);
            dest.writeString(uname);
            dest.writeString(logo);
            dest.writeLong(lmfTm);
            dest.writeString(detail);
            dest.writeLong(time);
            dest.writeString(title);
            dest.writeInt(noticeId);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<SchoolInfoEntity> CREATOR = new Creator<SchoolInfoEntity>() {
            @Override
            public SchoolInfoEntity createFromParcel(Parcel in) {
                return new SchoolInfoEntity(in);
            }

            @Override
            public SchoolInfoEntity[] newArray(int size) {
                return new SchoolInfoEntity[size];
            }
        };

        public String getPicturepath() {
            return picturepath;
        }

        public void setPicturepath(String picturepath) {
            this.picturepath = picturepath;
        }

        public String getUname() {
            return uname;
        }

        public void setUname(String uname) {
            this.uname = uname;
        }

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

        public long getLmfTm() {
            return lmfTm;
        }

        public void setLmfTm(long lmfTm) {
            this.lmfTm = lmfTm;
        }

        public String getDetail() {
            return detail;
        }

        public void setDetail(String detail) {
            this.detail = detail;
        }

        public long getTime() {
            return time;
        }

        public void setTime(long time) {
            this.time = time;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getNoticeId() {
            return noticeId;
        }

        public void setNoticeId(int noticeId) {
            this.noticeId = noticeId;
        }
    }

    public static class ClsNoticeEntity implements Parcelable{
        /**
         * uname : 快来
         * logo : 20160925122729431573.jpg
         * lmfTm : 1490869318000
         * detail : 						天气转凉了，请家长及时为孩子更换被褥。<br />
         * time : 1474978829000
         * title : 班级通知
         * noticeId : 1
         * picturepath :
         */

        private String uname;
        private String logo;
        private long lmfTm;
        private String detail;
        private long time;
        private String title;
        private int noticeId;
        private String picturepath;

        protected ClsNoticeEntity(Parcel in) {
            uname = in.readString();
            logo = in.readString();
            lmfTm = in.readLong();
            detail = in.readString();
            time = in.readLong();
            title = in.readString();
            noticeId = in.readInt();
            picturepath = in.readString();
        }

        public static final Creator<ClsNoticeEntity> CREATOR = new Creator<ClsNoticeEntity>() {
            @Override
            public ClsNoticeEntity createFromParcel(Parcel in) {
                return new ClsNoticeEntity(in);
            }

            @Override
            public ClsNoticeEntity[] newArray(int size) {
                return new ClsNoticeEntity[size];
            }
        };

        public String getUname() {
            return uname;
        }

        public void setUname(String uname) {
            this.uname = uname;
        }

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

        public long getLmfTm() {
            return lmfTm;
        }

        public void setLmfTm(long lmfTm) {
            this.lmfTm = lmfTm;
        }

        public String getDetail() {
            return detail;
        }

        public void setDetail(String detail) {
            this.detail = detail;
        }

        public long getTime() {
            return time;
        }

        public void setTime(long time) {
            this.time = time;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getNoticeId() {
            return noticeId;
        }

        public void setNoticeId(int noticeId) {
            this.noticeId = noticeId;
        }

        public String getPicturepath() {
            return picturepath;
        }

        public void setPicturepath(String picturepath) {
            this.picturepath = picturepath;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(uname);
            dest.writeString(logo);
            dest.writeLong(lmfTm);
            dest.writeString(detail);
            dest.writeLong(time);
            dest.writeString(title);
            dest.writeInt(noticeId);
            dest.writeString(picturepath);
        }
    }


  /*  *//**
     * schoolInfo : [{"picturepath":"","uname":"快来","logo":"20160925122729431573.jpg","detail":"&lt;span style=&quot;font-family:黑体;font-size: 25px; line-height: 40px; text-align: center; widows: 1;&quot;&gt;调查报告显示：校外辅导教师亟待&ldquo;标准&rdquo;认定&lt;/span&gt;","title":"调查报告显示：校外辅导教师亟待&ldquo;标准&rdquo;认定","noticeId":1718}]
     * code : 0
     * clsNotice : [{"picturepath":"","uname":"快来","logo":"20160925122729431573.jpg","detail":"&lt;span style=&quot;font-family:黑体;font-size: 25px; line-height: 40px; text-align: center; widows: 1;&quot;&gt;深圳新招中小学教师或提高门槛：中学至少硕士&lt;/span&gt;","title":"深圳新招中小学教师或提高门槛：中学至少硕士","noticeId":1719}]
     *//*

    public int code;
    public List<SchoolInfoEntity> schoolInfo;
    public List<ClsNoticeEntity> clsNotice;

    protected ProClassAndSchoolNotice(Parcel in) {
        code = in.readInt();
        schoolInfo = in.createTypedArrayList(SchoolInfoEntity.CREATOR);
        clsNotice = in.createTypedArrayList(ClsNoticeEntity.CREATOR);
    }

    public static final Creator<ProClassAndSchoolNotice> CREATOR = new Creator<ProClassAndSchoolNotice>() {
        @Override
        public ProClassAndSchoolNotice createFromParcel(Parcel in) {
            return new ProClassAndSchoolNotice(in);
        }

        @Override
        public ProClassAndSchoolNotice[] newArray(int size) {
            return new ProClassAndSchoolNotice[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(code);
        dest.writeTypedList(schoolInfo);
        dest.writeTypedList(clsNotice);
    }

    public static class SchoolInfoEntity implements Parcelable{
        *//**
         * picturepath :
         * uname : 快来
         * logo : 20160925122729431573.jpg
         * detail : &lt;span style=&quot;font-family:黑体;font-size: 25px; line-height: 40px; text-align: center; widows: 1;&quot;&gt;调查报告显示：校外辅导教师亟待&ldquo;标准&rdquo;认定&lt;/span&gt;
         * title : 调查报告显示：校外辅导教师亟待&ldquo;标准&rdquo;认定
         * noticeId : 1718
         *//*

        public String picturepath;
        public String uname;
        public String logo;
        public String detail;
        public String title;
        public int noticeId;

        protected SchoolInfoEntity(Parcel in) {
            picturepath = in.readString();
            uname = in.readString();
            logo = in.readString();
            detail = in.readString();
            title = in.readString();
            noticeId = in.readInt();
        }

        public static final Creator<SchoolInfoEntity> CREATOR = new Creator<SchoolInfoEntity>() {
            @Override
            public SchoolInfoEntity createFromParcel(Parcel in) {
                return new SchoolInfoEntity(in);
            }

            @Override
            public SchoolInfoEntity[] newArray(int size) {
                return new SchoolInfoEntity[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(picturepath);
            dest.writeString(uname);
            dest.writeString(logo);
            dest.writeString(detail);
            dest.writeString(title);
            dest.writeInt(noticeId);
        }
    }

    public static class ClsNoticeEntity implements Parcelable{
        *//**
         * picturepath :
         * uname : 快来
         * logo : 20160925122729431573.jpg
         * detail : &lt;span style=&quot;font-family:黑体;font-size: 25px; line-height: 40px; text-align: center; widows: 1;&quot;&gt;深圳新招中小学教师或提高门槛：中学至少硕士&lt;/span&gt;
         * title : 深圳新招中小学教师或提高门槛：中学至少硕士
         * noticeId : 1719
         *//*

        public String picturepath;
        public String uname;
        public String logo;
        public String detail;
        public String title;
        public int noticeId;

        protected ClsNoticeEntity(Parcel in) {
            picturepath = in.readString();
            uname = in.readString();
            logo = in.readString();
            detail = in.readString();
            title = in.readString();
            noticeId = in.readInt();
        }

        public static final Creator<ClsNoticeEntity> CREATOR = new Creator<ClsNoticeEntity>() {
            @Override
            public ClsNoticeEntity createFromParcel(Parcel in) {
                return new ClsNoticeEntity(in);
            }

            @Override
            public ClsNoticeEntity[] newArray(int size) {
                return new ClsNoticeEntity[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(picturepath);
            dest.writeString(uname);
            dest.writeString(logo);
            dest.writeString(detail);
            dest.writeString(title);
            dest.writeInt(noticeId);
        }
    }*/
}
