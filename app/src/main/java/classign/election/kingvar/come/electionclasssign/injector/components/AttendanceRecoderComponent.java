package classign.election.kingvar.come.electionclasssign.injector.components;


import classign.election.kingvar.come.electionclasssign.injector.PerFragment;
import classign.election.kingvar.come.electionclasssign.injector.modules.AttendanceRecoderModule;
import classign.election.kingvar.come.electionclasssign.module.main.moraleducation.two.TodayAttendanceTwoFragment;
import dagger.Component;

/**
 * Created by long on 2016/8/19.
 */
@PerFragment
@Component(dependencies = ApplicationComponent.class, modules = {AttendanceRecoderModule.class})
public interface AttendanceRecoderComponent {
  void inject(TodayAttendanceTwoFragment fragment);
}