package classign.election.kingvar.come.electionclasssign.injector.modules;

import android.util.Log;

import classign.election.kingvar.come.electionclasssign.injector.PerFragment;
import classign.election.kingvar.come.electionclasssign.module.base.IBasePresenter;
import classign.election.kingvar.come.electionclasssign.module.main.moraleducation.seven.AttendanceRecoderSevenPersenter;
import classign.election.kingvar.come.electionclasssign.module.main.moraleducation.seven.TodayAttendanceSevenFragment;
import dagger.Module;
import dagger.Provides;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/16 0016 15:28
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */
@Module
public class AttendanceRecoderSevenModule {
    private static final String TAG = "AttendanceRecoderModule";
    private final TodayAttendanceSevenFragment mTodayAttendanceFragment;
    private final  String mClsid;
    private final  String mTid;
    public AttendanceRecoderSevenModule(TodayAttendanceSevenFragment fragment, String clsid, String tid) {
        this.mTodayAttendanceFragment = fragment;
        this.mClsid = clsid;
        this.mTid = tid;
    }



    @PerFragment
    @Provides
    public IBasePresenter provideMainPresenter() {
        Log.i(TAG, "provideMainPresenter: 1231654646");
        Log.i(TAG, "provideMainPresenter: mClsid = " + mClsid);
        Log.i(TAG, "provideMainPresenter: mTid = " + mTid);
        return new AttendanceRecoderSevenPersenter(mTodayAttendanceFragment,mClsid,mTid);
    }

}
