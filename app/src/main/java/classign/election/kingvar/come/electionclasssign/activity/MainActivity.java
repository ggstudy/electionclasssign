package classign.election.kingvar.come.electionclasssign.activity;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import classign.election.kingvar.come.electionclasssign.R;
import classign.election.kingvar.come.electionclasssign.adapter.RecyclerAdapter;
import classign.election.kingvar.come.electionclasssign.adapter.RecyclerViewHolder;
import classign.election.kingvar.come.electionclasssign.api.info.AllProjectInfo;
import classign.election.kingvar.come.electionclasssign.api.info.AllStudentInfo;
import classign.election.kingvar.come.electionclasssign.api.info.BaseClassInfo;
import classign.election.kingvar.come.electionclasssign.api.info.ProClassAndSchoolNotice;
import classign.election.kingvar.come.electionclasssign.api.info.ProClassSchedule;
import classign.election.kingvar.come.electionclasssign.api.info.ProTemperature;
import classign.election.kingvar.come.electionclasssign.api.info.TodayDeYuInfo;
import classign.election.kingvar.come.electionclasssign.api.info.TodayDutyInfo;
import classign.election.kingvar.come.electionclasssign.base.BaseActivity;
import classign.election.kingvar.come.electionclasssign.broadcastreceiver.CommentEvent;
import classign.election.kingvar.come.electionclasssign.broadcastreceiver.TimeChangeReceiver;
import classign.election.kingvar.come.electionclasssign.greendao.StudentInfo;
import classign.election.kingvar.come.electionclasssign.greendao.StudentInfoDao;
import classign.election.kingvar.come.electionclasssign.injector.components.DaggerMainActivityComponent;
import classign.election.kingvar.come.electionclasssign.injector.modules.MainActivityModule;
import classign.election.kingvar.come.electionclasssign.interfaces.MyLocationListener;
import classign.election.kingvar.come.electionclasssign.interfaces.OnCardSlotListener;
import classign.election.kingvar.come.electionclasssign.module.lessonmodule.ModeOfAttendanceActivity;
import classign.election.kingvar.come.electionclasssign.module.main.MainActivityView;
import classign.election.kingvar.come.electionclasssign.module.main.classmien.ClassMienActivity;
import classign.election.kingvar.come.electionclasssign.module.main.punchcard.PunchCardActivity;
import classign.election.kingvar.come.electionclasssign.module.main.studentmien.StudentMienActivity;
import classign.election.kingvar.come.electionclasssign.module.main.weekdays.IMainActivityPresenter;
import classign.election.kingvar.come.electionclasssign.tools.bean.ActivityStatusBean;
import classign.election.kingvar.come.electionclasssign.tools.bean.User;
import classign.election.kingvar.come.electionclasssign.tools.utils.Constants;
import classign.election.kingvar.come.electionclasssign.tools.utils.DateUtil;
import classign.election.kingvar.come.electionclasssign.tools.utils.LocationUtil;
import classign.election.kingvar.come.electionclasssign.tools.utils.NetUtil;
import classign.election.kingvar.come.electionclasssign.tools.utils.SearchEveryDayProjectUtil;
import classign.election.kingvar.come.electionclasssign.tools.utils.TimeTaskScroll;
import classign.election.kingvar.come.electionclasssign.tools.utils.ToastUtils;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

import static classign.election.kingvar.come.electionclasssign.tools.utils.LocationUtil.initLocation;

public class MainActivity extends BaseActivity<IMainActivityPresenter> implements MainActivityView, RadioGroup.OnCheckedChangeListener, TimeChangeReceiver.TimeEvent {
    private static final String TAG = "MainActivity";
    private FragmentManager manager;
    public LocationClient mLocationClient = null;
    public BDLocationListener myListener = new MyLocationListener(this);
    String detail;//班级事件
    TimeChangeReceiver receiver;

    private static String content = "";
    private OnCardSlotListener listner;

    Timer timer_school;  //学校公告 timer
    Timer timer_class;  //班级公告timer
    TimeTaskScroll timeTaskScroll_school;  //学校TimeTask
    TimeTaskScroll timeTaskScroll_class; //班级TimeTask

    @BindView(R.id.tv_today_deyu)
    TextView mTvTodayDeYu;   //今日德育人数

    @BindView(R.id.tv_qualit)
    TextView mTvQualit;  //今日德育加减分

    @BindView(R.id.tv_class_name)
    TextView tv_class_name; //班级名称

    @BindView(R.id.tv_class_teacher)
    TextView tv_class_teacher; //班主任  副班主任 和 班长

    @BindView(R.id.rv_today_duty)
    RecyclerView rv_today_duty;  //今日值日recycleview

    @BindView(R.id.tv_group_leader)
    TextView mGroupLeader;

    @BindView(R.id.lv_scroll_left)
    ListView lv_scroll_left_resource;//校园通知listview

    @BindView(R.id.lv_scroll_right)
    ListView lv_scroll_right_resource;//班级通知listview

    @BindView(R.id.iv_class_album)
    ImageView iv_class_album; //班级相册

    @BindView(R.id.floating_action_btn)
    ImageButton floating_action_btn; //悬浮按钮

    @BindView(R.id.include_student_mien)
    LinearLayout include_student_mien; //学生风采 整体布局

    @BindView(R.id.include_class_status)
    LinearLayout include_class_status; //班级风采 整体布局

    @BindView(R.id.include_today_curriculum)
    LinearLayout include_today_curriculum; //本周课程

    @BindView(R.id.include_today_duty)
    LinearLayout include_today_duty;  //本周值日

    @BindView(R.id.rg_class_status_content)
    RadioGroup rg_class_status_content; //class_status 的Radiogroup

    @BindView(R.id.tv_title)
    TextView tv_title;  //学生风采标题

    @BindView(R.id.rg_student_mien_content)
    RadioGroup rg_student_mien_content; //student_mien 的Radiogroup

    @BindView(R.id.tv_year)
    TextView tv_year;  //时间戳

    @BindView(R.id.tv_day)
    TextView tv_day;

    @BindView(R.id.tv_week)
    TextView tv_week;

    @BindView(R.id.tv_schedule)
    TextView tv_schedule; //班级事件

    @BindView(R.id.tv_current_temperature)
    TextView tv_current_temperature;  //当前温度

    @BindView(R.id.tv_today_temperature)
    TextView tv_today_temperature;  //今天温度

    @BindView(R.id.tv_tommorow_temperature)
    TextView tv_tommorow_temperature;  //明天温度

    @BindView(R.id.tv_address)
    TextView tv_address; //定位的位置

    @BindView(R.id.iv_weather_pic)
    ImageView iv_weather_pic; //天气的图片

    @BindView(R.id.relative_today_attendance)
    RelativeLayout relative_today_attendance;  //今日考勤 模块(点击变大)

    @BindView(R.id.rv_today_project)
    RecyclerView mRvTodayProject; //今日和明日的课程

    @BindView(R.id.rv_morning_night)
    RecyclerView mRvMorningNight; //早晚自习的课程

    private StudentInfoDao mStudentInfoDao;
    private RecyclerAdapter mDoubleProjectAdapter;

    private ArrayList<Integer> radio_btn_arrayList = new ArrayList();
    int i = 0;
    private int totalNum;
    private int shouldNum;
    private int requestNum;


    @Override
    public void onRoot(Bundle savedInstanceState) {
        setContentView(R.layout.activity_main);
        //注册EventBus
        EventBus.getDefault().register(this);
        mLocationClient = new LocationClient(getApplicationContext());
        initLocation();
        mLocationClient.setLocOption(LocationUtil.initLocation());
        mLocationClient.start();
        mLocationClient.registerLocationListener(myListener);
    }

    @Override
    protected void initInjector() {
        DaggerMainActivityComponent.builder()
                .applicationComponent(getAppComponent())
                .mainActivityModule(new MainActivityModule(this))
                .build()
                .inject(this);
    }

    @Override
    protected void initView() {
        setDate(System.currentTimeMillis());
        //注册 时间变化广播接受者
        receiver = new TimeChangeReceiver();
        receiver.setOnTimeEventListener(this);
        registerReceiver(receiver, new IntentFilter(Intent.ACTION_TIME_TICK));
    }

    @Override
    public void initData() {
        showRadiobtnLunbo();
        manager = this.getSupportFragmentManager();
//        设置默认显示的fragment
        mNewPresenter.showStudentMien(this, R.id.radio_btn_one, manager);
        mNewPresenter.showClassStatus(this, R.id.radio_btn_class_info, manager,null);
    }

    /**
     * 设置班级主要信息
     */
    private void setClassInfo() {
        tv_class_name.setText(User.getClassName(this));
        tv_class_teacher.setText("班主任 :" + User.getClassTeacherOne(this) +
                "   副班主任 :" + User.getClassTeacherTwo(this) +
                "   班长 :" + User.getClassMonitor(this));
    }

    /**
     * 设置时间
     */
    private void setDate(long time) {
        tv_year.setText(DateUtil.getYearByTimeStamp(time) + "年" + DateUtil.getMonthByTimeStamp(time)
                + "月");
        tv_day.setText(DateUtil.getDayByTimeStamp(time) + "");
        tv_week.setText(DateUtil.getWeekOfDate(new Date()));
    }

    /**
     * 个人风采自动轮播
     */
    private void showRadiobtnLunbo() {
        radio_btn_arrayList.add(R.id.radio_btn_one);
        radio_btn_arrayList.add(R.id.radio_btn_two);
        radio_btn_arrayList.add(R.id.radio_btn_three);
        radio_btn_arrayList.add(R.id.radio_btn_four);
        radio_btn_arrayList.add(R.id.radio_btn_five);
        Observable.interval(30,30, TimeUnit.SECONDS)
                .compose(this.bindToLifecycle())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Long>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Long aLong) {
                        rg_student_mien_content.check(radio_btn_arrayList.get(i));
                        mNewPresenter.showStudentMien(MainActivity.this, radio_btn_arrayList.get(i), manager);
                        if (i < 4) {
                            i++;
                        } else {
                            i = 0;
                        }
                    }
                });
    }

    @Override
    public void initListener() {
        rg_student_mien_content.setOnCheckedChangeListener(this);
        rg_class_status_content.setOnCheckedChangeListener(this);
        iv_class_album.setOnClickListener(this);
        floating_action_btn.setOnClickListener(this);
        include_student_mien.setOnClickListener(this);
        include_class_status.setOnClickListener(this);
        include_today_curriculum.setOnClickListener(this);
        include_today_duty.setOnClickListener(this);
        relative_today_attendance.setOnClickListener(this);
    }

    @Override
    public void WidgetClick(View view) {
        switch (view.getId()) {
            case R.id.iv_class_album:
                Intent intent = new Intent(this, ClassAlbumActivity.class);
                startActivity(intent);
                break;
            case R.id.floating_action_btn:
                intent = new Intent(this, MenuActivity.class);
                startActivity(intent);
                break;
            case R.id.include_student_mien:  //点击查看学生风采大界面
                intent = new Intent(this, StudentMienActivity.class);
                startActivity(intent);
                break;
            case R.id.include_class_status:  //点击查看班级风采大界面
                intent = new Intent(this, ClassMienActivity.class);
                intent.putExtra("isBig", "isBig");
                startActivity(intent);
                break;
            case R.id.relative_today_attendance://点击查看 今日德育大界面
                intent = new Intent(this, ToadyAttendanceActivity.class);
                startActivity(intent);
                break;
            case R.id.include_today_curriculum:  //点击查看本周课程
                intent = new Intent(this, ThisWeekProjectActivity.class);
                startActivity(intent);
                break;
            case R.id.include_today_duty:  //点击查看本周值日
                intent = new Intent(this, ThisWeekDutyActivity.class);
                startActivity(intent);
                break;
        }
    }

    /**
     * 当前天气和气温回调
     *
     * @param forecastEntityList
     * @param wendu
     */
    @Override
    public void setTemperatureInfo(List<ProTemperature.DataEntity.ForecastEntity> forecastEntityList, String wendu) {
        tv_address.setText(User.getLocation(MainActivity.this));
        tv_current_temperature.setText(wendu + "℃");
        Log.i(TAG, "setTemperatureInfo: size= " + forecastEntityList.get(0).getLow().length());
        if (forecastEntityList.get(0).getLow().length() == 5) {
            tv_today_temperature.setText(forecastEntityList.get(0).getLow().substring(2, 5) + "/" + forecastEntityList.get(0).getHigh().substring(2, 5));
            tv_tommorow_temperature.setText(forecastEntityList.get(1).getLow().substring(2, 5) + "/" + forecastEntityList.get(1).getHigh().substring(2, 5));
        } else {
            tv_today_temperature.setText(forecastEntityList.get(0).getLow().substring(2, 6) + "/" + forecastEntityList.get(0).getHigh().substring(2, 6));
            tv_tommorow_temperature.setText(forecastEntityList.get(1).getLow().substring(2, 6) + "/" + forecastEntityList.get(1).getHigh().substring(2, 6));
        }
        switch (forecastEntityList.get(0).getType()) {
            case "晴":
                iv_weather_pic.setImageResource(R.drawable.weather_one);
                break;
            case "多云":
                iv_weather_pic.setImageResource(R.drawable.weather_three);
                break;
            case "中到大雨":
                iv_weather_pic.setImageResource(R.drawable.weather_four);
                break;
            case "大雨":
                iv_weather_pic.setImageResource(R.drawable.weather_five);
                break;
            case "中雨":
                iv_weather_pic.setImageResource(R.drawable.weather_nine);
                break;
            case "雨夹雪":
                iv_weather_pic.setImageResource(R.drawable.weather_six);
                break;
            case "雾霾":
                iv_weather_pic.setImageResource(R.drawable.weather_seven);
                break;
            case "小雨":
                iv_weather_pic.setImageResource(R.drawable.weather_two);
                break;
            default:
                break;
        }
    }

    /**
     * 主页今日德育回调
     *
     * @param todayDeYuInfo
     */
    @Override
    public void setTodayDeYuInfoData(TodayDeYuInfo todayDeYuInfo) {
        //总人数
        totalNum = todayDeYuInfo.getTotal();
        //实到人数
        shouldNum = todayDeYuInfo.getClassStu();
        //请假人数
        requestNum = todayDeYuInfo.getQinjia();
        int late = totalNum - shouldNum - requestNum;  //迟到人数
        mTvTodayDeYu.setText("应到人数:" + totalNum + "  实到:" +
                shouldNum + "  未到:" + late + "  请假:" + requestNum);
        int deyuSize = todayDeYuInfo.getData().size();
        if (deyuSize == 0) {
            mTvQualit.setText("今日德育:");
        } else if (deyuSize == 1) {
            mTvQualit.setText("今日德育:" + todayDeYuInfo.getData().get(0).getTitle() + todayDeYuInfo.getData().get(0).getNum());
        } else if (deyuSize == 2) {
            mTvQualit.setText("今日德育:" + todayDeYuInfo.getData().get(0).getTitle() + todayDeYuInfo.getData().get(0).getNum() +
                    "," + todayDeYuInfo.getData().get(1).getTitle() + todayDeYuInfo.getData().get(1).getNum());

        }

    }


    @Override
    public void setStudentMien(int checkId) {
        if (checkId == radio_btn_arrayList.get(0)) {
            tv_title.setText(getString(R.string.today_star));
        } else if (checkId == radio_btn_arrayList.get(1)) {
            tv_title.setText(getString(R.string.class_reward));
        } else if (checkId == radio_btn_arrayList.get(2)) {
            tv_title.setText(getString(R.string.school_reward));
        } else if (checkId == radio_btn_arrayList.get(3)) {
            tv_title.setText(getString(R.string.my_dream));
        } else if (checkId == radio_btn_arrayList.get(4)) {
            tv_title.setText(getString(R.string.encourage_talk));
        }

    }


    /**
     * 校园公告
     *
     * @param schoolInfoEntityList
     * @param size
     */
    @Override
    public void setSchoolNoticeList(final ArrayList<ProClassAndSchoolNotice.SchoolInfoEntity> schoolInfoEntityList, final int size) {
        final List schoolNoticeList = new ArrayList();
        final List timeList = new ArrayList();
        for (int j = 0; j < size; j++) {
            schoolNoticeList.add(schoolInfoEntityList.get(j).getTitle());
            timeList.add("[" + DateUtil.getMonthByTimeStamp(schoolInfoEntityList.get(j).getTime()) + "/"
                    + DateUtil.getDayByTimeStamp(schoolInfoEntityList.get(j).getTime()) + "]");
        }
        if (timer_school == null) {
            timer_school = new Timer();
        }
        if (timeTaskScroll_school != null) {
            timeTaskScroll_school.cancel();
        }
        timeTaskScroll_school = new TimeTaskScroll(this, lv_scroll_left_resource, schoolNoticeList, timeList, 1);
        timer_school.schedule(timeTaskScroll_school, 0, 3000);

        lv_scroll_left_resource.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, ScholNoticeDetailActivity.class);
                intent.putParcelableArrayListExtra("schoolInfoEntityList", schoolInfoEntityList);
                intent.putExtra("position", position);
                intent.putExtra("type", "1");
                startActivity(intent);
            }
        });

    }

    /**
     * 班级公告
     *
     * @param clsNoticeEntityList
     * @param size
     */
    @Override
    public void setClassNoticeList(final ArrayList<ProClassAndSchoolNotice.ClsNoticeEntity> clsNoticeEntityList, int size) {
        final List ClassNoticeList = new ArrayList();
        final List timeList = new ArrayList();

        for (int j = 0; j < size; j++) {
            ClassNoticeList.add(clsNoticeEntityList.get(j).getTitle());
            timeList.add("[" + DateUtil.getMonthByTimeStamp(clsNoticeEntityList.get(j).getTime()) + "/"
                    + DateUtil.getDayByTimeStamp(clsNoticeEntityList.get(j).getTime()) + "]");
        }
        if (timer_class == null) {
            timer_class = new Timer();
        }
        if (timeTaskScroll_class != null) {
            timeTaskScroll_class.cancel();
        }
        timeTaskScroll_class = new TimeTaskScroll(this, lv_scroll_right_resource, ClassNoticeList, timeList, 2);
        timer_class.schedule(timeTaskScroll_class, 0, 3000);

        lv_scroll_right_resource.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, ScholNoticeDetailActivity.class);
                intent.putParcelableArrayListExtra("clsNoticeEntityList", clsNoticeEntityList);
                intent.putExtra("position", position);
                intent.putExtra("type", "2");
                startActivity(intent);

            }
        });

    }

    /**
     * 日程管理信息回调
     *
     * @param info
     */
    @Override
    public void setClassScheduleData(final List<ProClassSchedule.ScheduleEntity> info) {
        if (info != null && info.size() > 0) {
            detail = info.get(0).getDetal();
            tv_schedule.setText(detail);
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (group.getId()) {
            case R.id.rg_student_mien_content:
                mNewPresenter.showStudentMien(this, checkedId, manager);
                break;
            case R.id.rg_class_status_content:
                mNewPresenter.showClassStatus(this, checkedId, manager,null);
                break;
        }

    }


    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_UP)
            return true;
        if (KeyEvent.KEYCODE_ENTER == event.getKeyCode()) {

            if (listner != null) {
                listner.onCardSlot(content);
            }
            Log.i(TAG, "dispatchKeyEvent:content = " + content);
            mStudentInfoDao = MyApplication.getAppComponent().getDaoSession().getStudentInfoDao();
            if (mStudentInfoDao.queryBuilder().where(StudentInfoDao.Properties.RegisterID.eq(content)).build().list().size() <= 0) {
                ToastUtils.showToast("卡号未绑定或者不是本班级学生");
            } else {
                Log.i(TAG, "dispatchKeyEvent: status = " + new ActivityStatusBean().isStatus());
                if (!new ActivityStatusBean().isStatus()) {
                    Intent intent = new Intent(this, PunchCardActivity.class);
                    intent.putExtra("registerID", content);
                    startActivity(intent);
                }
            }
            content = "";
            return true;
        }
        String str = String.valueOf((char) event.getUnicodeChar());
        if (!TextUtils.isEmpty(str))
            content += str;
        return true;
    }

    /**
     * 基本班级信息数据
     *
     * @param baseClassInfo 基本班级信息数据
     */
    @Override
    public void loadClassInfoData(BaseClassInfo baseClassInfo) {
        if (baseClassInfo.getCode() == 0) {
            BaseClassInfo.InfoEntity infoBeen = baseClassInfo.getInfo();
            User.setClassId(this, infoBeen.getClsid());
            User.setClassMonitor(this, infoBeen.getMonitor());
            User.setClassTeacherOne(this, infoBeen.getName());
            User.setClassTeacherTwo(this, infoBeen.getViceName());
            User.setSchoolId(this, infoBeen.getSchoolid() + "");
            User.setClassName(this, infoBeen.getCname());
//            User.setImei(this, infoBeen.getImei());
            User.setUserID(this, infoBeen.getId() + "");
            mStudentInfoDao = MyApplication.getAppComponent().getDaoSession().getStudentInfoDao();
            setClassInfo();

        }

    }

    /**
     * 班级学生数据
     *
     * @param allStudentInfo
     */
    @Override
    public void loadAllStudentInfoData(final AllStudentInfo allStudentInfo) {
        mStudentInfoDao.getSession().runInTx(new Runnable() {
            @Override
            public void run() {
                List<AllStudentInfo.InfoBean> infoBeen = allStudentInfo.getInfo();

                for (int i = 0; i < infoBeen.size(); i++) {
                    StudentInfo studentInfo = new StudentInfo(null
                            , infoBeen.get(i).getStudentid()
                            , infoBeen.get(i).getNo()
                            , infoBeen.get(i).getRegisterID()
                            , infoBeen.get(i).getName()
                            , infoBeen.get(i).getLogo()
                            , infoBeen.get(i).getMemo()
                            , infoBeen.get(i).getSchoolDuty()
                            , infoBeen.get(i).getDuty()
                            , 0);
                    mStudentInfoDao.insertOrReplace(studentInfo);
                }
            }
        });
        mLocationClient.stop();
    }

    /**
     * 早晚自习回调
     *
     * @param nightAdapter adapter
     */
    @Override
    public void loadTodayProjectData(RecyclerAdapter nightAdapter) {
        mRvMorningNight.setLayoutManager(new GridLayoutManager(this, 7));
        mRvMorningNight.setAdapter(nightAdapter);
    }

    /**
     * 今天和明天两天课程回调
     */
    @Override
    public void loadTodayProjectList() {
        final List<AllProjectInfo.Entity> doubleProject = SearchEveryDayProjectUtil.getTodayWeek(DateUtil.getWeekOfDate(new Date()));
        mRvTodayProject.setLayoutManager(new GridLayoutManager(this, 8));
        if (mDoubleProjectAdapter == null) {
            mDoubleProjectAdapter = new RecyclerAdapter(R.layout.today_project_item, doubleProject) {
                @Override
                protected void onBindData(RecyclerViewHolder holder, int position, Object item) {
                    holder.setText(R.id.tv_duty_name, doubleProject.get(position).getCourse());
                }
            };
            mRvTodayProject.setAdapter(mDoubleProjectAdapter);
        } else {
            mDoubleProjectAdapter.setItemDate(doubleProject);
        }
    }

    /**
     * @param todayDutyInfo 今日值日回调
     */
    @Override
    public void loadTodayDutyData(TodayDutyInfo todayDutyInfo, RecyclerAdapter recyclerAdapter, Boolean isNull) {
        mGroupLeader.setText("组长 :" + todayDutyInfo.getOnduty().getLeader());
        rv_today_duty.setLayoutManager(new GridLayoutManager(this, 3));
        if (isNull) {
            rv_today_duty.setAdapter(recyclerAdapter);
        } else {
            recyclerAdapter.setItemDate(todayDutyInfo.getOnduty().getStus());
        }
    }

    /**
     * 网络监听
     *
     * @param commentEvent
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNetChange(CommentEvent commentEvent) {
        if (commentEvent.eventType == CommentEvent.NETWORkSTATE) {
            //网络状态变化时的操作
            if (commentEvent.netWorkState == NetUtil.NETWORK_NONE) {
                floating_action_btn.setImageResource(R.drawable.press_btn_suspension);
                floating_action_btn.setClickable(false);
            } else {
                floating_action_btn.setImageResource(R.drawable.normal_btn_suspension);
                floating_action_btn.setClickable(true);
                mNewPresenter.getData(true);

            }
        }

    }

    /**
     * 不让MainActivity销毁
     */
    @Override
    public void finish() {
        moveTaskToBack(true);
    }

    /**
     * 对当前时间的变化进行监听
     *
     * @param time
     */
    @Override
    public void getCurrentTime(long time) {
        setDate(time);
        String currentTime = String.valueOf(DateUtil.getHourByTimeStamp(time)) + DateUtil.getMinuteTime(time);
        List startTimeList = SearchEveryDayProjectUtil.getCurrentProjectStartTime(DateUtil.getWeekOfDate(new Date()));
        Log.i(TAG, "getCurrentTime: startTimeList =" + startTimeList);
        Log.i(TAG, "getCurrentTime: currentTime =" + currentTime);
        if (startTimeList.size() > 0) {
            for (int j = 0; j < startTimeList.size(); j++) {
                if (startTimeList.get(j).equals(currentTime) && !Constants.isProjectModule) {
                    Constants.isProjectModule = true;
                    Intent intent = new Intent(this, ModeOfAttendanceActivity.class);
                    intent.putExtra("totalNum", totalNum);
                    intent.putExtra("shouldNum", shouldNum);
                    intent.putExtra("index", j);
                    intent.putExtra("detail", detail);
                    startActivity(intent);
                }
            }
        }


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        receiver.removeTimeEventListener();
        unregisterReceiver(receiver);
    }
}

