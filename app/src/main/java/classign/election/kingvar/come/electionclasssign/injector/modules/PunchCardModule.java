package classign.election.kingvar.come.electionclasssign.injector.modules;

import android.util.Log;

import classign.election.kingvar.come.electionclasssign.injector.PerActivity;
import classign.election.kingvar.come.electionclasssign.module.main.punchcard.PunchCardActivity;
import classign.election.kingvar.come.electionclasssign.module.main.punchcard.PunchCardPersenter;
import classign.election.kingvar.come.electionclasssign.module.main.punchcard.PunchCardPresenter;
import dagger.Module;
import dagger.Provides;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/16 0016 15:28
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */
@Module
public class PunchCardModule {
    private static final String TAG = "AttendanceRecoderModule";
    private final PunchCardActivity mActivity;

    public PunchCardModule(PunchCardActivity mActivity) {
        this.mActivity=mActivity;
    }

    @PerActivity
    @Provides
    public PunchCardPresenter providePresenter() {
        Log.i(TAG, "provideMainPresenter: 1231654646");
        return new PunchCardPersenter(mActivity);
    }

}
