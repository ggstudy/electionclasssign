package classign.election.kingvar.come.electionclasssign.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import classign.election.kingvar.come.electionclasssign.api.info.ThisWeekDutyInfo;
import classign.election.kingvar.come.electionclasssign.module.main.weekdays.ThisWeekDutyFragment;

/**
 * Created by long on 2016/6/2.
 * ViewPager适配器
 */
public class DutyViewPagerAdapter extends FragmentStatePagerAdapter {
    List mTitles;

    private static final String TAG = "ViewPagerAdapter";
    List<String> mId;
    List<Fragment> fragments;
    ThisWeekDutyInfo weekDutyInfo;
    private ThisWeekDutyFragment weekDutyFragment;

    public DutyViewPagerAdapter(FragmentManager fm) {
        super(fm);
        fragments = new ArrayList<>();
        mTitles = new ArrayList<String>();
        mId = new ArrayList<String>();
    }

    public DutyViewPagerAdapter(FragmentManager fm, List mTitles) {
        super(fm);
        this.mTitles = mTitles;
    }

    @Override
    public Fragment getItem(int position) {
        Log.i(TAG, "getItem: 11111 = " + weekDutyInfo.get_$1s());
        weekDutyFragment = ThisWeekDutyFragment.newInstance(weekDutyInfo,position);
        return weekDutyFragment;
    }


    @Override
    public int getCount() {
        return mTitles.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return (CharSequence) mTitles.get(position);
    }

    @Override
    public int getItemPosition(Object object) {
        return PagerAdapter.POSITION_NONE;
    }

    public void setItemsTitle(List<String> mTitles, List<String> mId) {
        this.mTitles = mTitles;
        this.mId = mId;
        notifyDataSetChanged();
    }

    public void setItems(List<Fragment> fragments, List<String> mTitles) {
        this.fragments = fragments;
        this.mTitles = mTitles;
        notifyDataSetChanged();
    }

    public void setItems(List<Fragment> fragments, String[] mTitles) {
        this.fragments = fragments;
        this.mTitles = Arrays.asList(mTitles);
        notifyDataSetChanged();
    }

    public void addItem(Fragment fragment, String title) {
        fragments.add(fragment);
        mTitles.add(title);
        notifyDataSetChanged();
    }

    public void delItem(int position) {
        mTitles.remove(position);
        fragments.remove(position);
        notifyDataSetChanged();
    }

    public int delItem(String title) {
        int index = mTitles.indexOf(title);
        if (index != -1) {
            delItem(index);
        }
        return index;
    }

    public void swapItems(int fromPos, int toPos) {
        Collections.swap(mTitles, fromPos, toPos);
        Collections.swap(fragments, fromPos, toPos);
        notifyDataSetChanged();
    }

    public void modifyTitle(int position, String title) {
        mTitles.set(position, title);
        notifyDataSetChanged();
    }


    public void setItems(ThisWeekDutyInfo weekDutyInfo) {
        this.weekDutyInfo = weekDutyInfo;
    }
}
