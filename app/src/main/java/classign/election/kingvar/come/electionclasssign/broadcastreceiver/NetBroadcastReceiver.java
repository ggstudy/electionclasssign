package classign.election.kingvar.come.electionclasssign.broadcastreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;

import org.greenrobot.eventbus.EventBus;

import classign.election.kingvar.come.electionclasssign.tools.utils.NetUtil;
import classign.election.kingvar.come.electionclasssign.tools.utils.ToastUtils;

/**
 * Created by cheng on 2016/11/28.
 */
public class NetBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
            int netWorkState = NetUtil.getNetWorkState(context);
            if (netWorkState == NetUtil.NETWORK_WIFI ||netWorkState == NetUtil.NETWORK_MOBILE) {
                ToastUtils.showToast("当前网络良好");
            } else if (netWorkState == NetUtil.NETWORK_NONE) {
                ToastUtils.showToast("当前无网络");
            }
            EventBus.getDefault().post(new CommentEvent(CommentEvent.NETWORkSTATE,netWorkState));
        }
    }

}
