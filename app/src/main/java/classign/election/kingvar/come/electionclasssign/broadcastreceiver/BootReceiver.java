package classign.election.kingvar.come.electionclasssign.broadcastreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;


/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/5/22 0022 16:08
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public class BootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent intentForPackage = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName());
        context.startActivity(intentForPackage);
    }
}
