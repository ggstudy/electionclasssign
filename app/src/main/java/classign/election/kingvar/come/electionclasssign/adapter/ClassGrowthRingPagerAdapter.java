package classign.election.kingvar.come.electionclasssign.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;

import java.util.ArrayList;
import java.util.List;

import classign.election.kingvar.come.electionclasssign.api.info.ClassGrowthRingInfo;
import classign.election.kingvar.come.electionclasssign.module.main.classmien.classgrowth.ClassRingViewPagerFragment;

/**
 * Created by long on 2016/6/2.
 * ViewPager适配器
 */
public class ClassGrowthRingPagerAdapter extends FragmentStatePagerAdapter {
    private static final String TAG = "ViewPagerAdapter";
    List<String> mTitles;
    List<String> mId;
    List<Fragment> fragments;
    List<ClassGrowthRingInfo.ClsringEntity> clsringEntityList;

    public ClassGrowthRingPagerAdapter(FragmentManager fm) {
        super(fm);
        fragments = new ArrayList<>();
        mTitles = new ArrayList<String>();
        mId = new ArrayList<String>();
    }

    public ClassGrowthRingPagerAdapter(FragmentManager fm, List<ClassGrowthRingInfo.ClsringEntity> clsringEntityList) {
        super(fm);
        this.clsringEntityList = clsringEntityList;
    }

    @Override
    public Fragment getItem(int position) {
        ClassRingViewPagerFragment   classFragment = new ClassRingViewPagerFragment();
        classFragment.setPosition(position);
        classFragment.setClsringBeen(clsringEntityList);
        return classFragment;
    }


    @Override
    public int getCount() {
        if (clsringEntityList.size() % 5 == 0) {
            return clsringEntityList.size() / 5;
        } else {
            return clsringEntityList.size() / 5 + 1;
        }
    }

    @Override
    public int getItemPosition(Object object) {
        return PagerAdapter.POSITION_NONE;
    }

    public void setData(List<ClassGrowthRingInfo.ClsringEntity> clsringEntityList) {
        this.clsringEntityList = clsringEntityList;
        notifyDataSetChanged();
    }
}
