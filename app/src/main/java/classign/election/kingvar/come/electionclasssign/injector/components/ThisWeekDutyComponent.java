package classign.election.kingvar.come.electionclasssign.injector.components;


import classign.election.kingvar.come.electionclasssign.activity.ThisWeekDutyActivity;
import classign.election.kingvar.come.electionclasssign.injector.PerActivity;
import classign.election.kingvar.come.electionclasssign.injector.modules.ThisWeekDutyModule;
import dagger.Component;

/**
 * Created by long on 2016/8/19.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {ThisWeekDutyModule.class})
public interface ThisWeekDutyComponent {
  void inject(ThisWeekDutyActivity activity);
}