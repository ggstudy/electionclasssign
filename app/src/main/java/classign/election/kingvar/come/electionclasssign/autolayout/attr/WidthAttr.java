package classign.election.kingvar.come.electionclasssign.autolayout.attr;

import android.view.View;
import android.view.ViewGroup;

/**
 * Created by jyq on 16/12/4.
 */
public class WidthAttr extends AutoAttr {
	public WidthAttr(int pxVal, int baseWidth, int baseHeight) {
		super(pxVal, baseWidth, baseHeight);
	}

	@Override
	protected int attrVal() {
		return Attrs.WIDTH;
	}

	@Override
	protected boolean defaultBaseWidth() {
		return true;
	}

	@Override
	protected void execute(View view, int val) {
		ViewGroup.LayoutParams lp = view.getLayoutParams();
		lp.width = val;
	}

}
