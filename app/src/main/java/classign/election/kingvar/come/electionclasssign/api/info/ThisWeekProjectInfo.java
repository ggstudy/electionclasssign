package classign.election.kingvar.come.electionclasssign.api.info;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/31 0031 10:08
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public class ThisWeekProjectInfo {

    /**
     * 3s : [{"name":"英语","tname":"(gasry1989)"},{"name":"语文","tname":"(李老师)"},{"name":"语文","tname":"(李老师)"},{"name":"数学","tname":"(提拉米苏1)"},{"name":"数学","tname":"(提拉米苏1)"},{"name":"英语","tname":"(gasry1989)"},{"name":"英语","tname":"(gasry1989)"},{"name":"","tname":""}]
     * 2s : [{"name":"数学","tname":"(提拉米苏1)"},{"name":"","tname":""},{"name":"","tname":""},{"name":"","tname":""},{"name":"","tname":""},{"name":"","tname":""},{"name":"","tname":""},{"name":"","tname":""}]
     * 1s : [{"name":"语文","tname":"(李老师)"},{"name":"","tname":""},{"name":"","tname":""},{"name":"","tname":""},{"name":"","tname":""},{"name":"","tname":""},{"name":"","tname":""},{"name":"","tname":""}]
     * code : 0
     * 7s : [{"name":"英语","tname":"(gasry1989)"},{"name":"","tname":""},{"name":"","tname":""},{"name":"","tname":""},{"name":"","tname":""},{"name":"","tname":""},{"name":"","tname":""},{"name":"","tname":""}]
     * 6s : [{"name":"英语","tname":"(gasry1989)"},{"name":"","tname":""},{"name":"","tname":""},{"name":"","tname":""},{"name":"","tname":""},{"name":"","tname":""},{"name":"","tname":""},{"name":"","tname":""}]
     * 5s : [{"name":"英语","tname":"(gasry1989)"},{"name":"","tname":""},{"name":"","tname":""},{"name":"","tname":""},{"name":"","tname":""},{"name":"","tname":""},{"name":"","tname":""},{"name":"","tname":""}]
     * 4s : [{"name":"英语","tname":"(gasry1989)"},{"name":"","tname":""},{"name":"","tname":""},{"name":"","tname":""},{"name":"","tname":""},{"name":"","tname":""},{"name":"","tname":""},{"name":"","tname":""}]
     */

    private int code;
    @SerializedName("3s")
    private List<Entity> _$3s;
    @SerializedName("2s")
    private List<Entity> _$2s;
    @SerializedName("1s")
    private List<Entity> _$1s;
    @SerializedName("7s")
    private List<Entity> _$7s;
    @SerializedName("6s")
    private List<Entity> _$6s;
    @SerializedName("5s")
    private List<Entity> _$5s;
    @SerializedName("4s")
    private List<Entity> _$4s;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<Entity> get_$3s() {
        return _$3s;
    }

    public void set_$3s(List<Entity> _$3s) {
        this._$3s = _$3s;
    }

    public List<Entity> get_$2s() {
        return _$2s;
    }

    public void set_$2s(List<Entity> _$2s) {
        this._$2s = _$2s;
    }

    public List<Entity> get_$1s() {
        return _$1s;
    }

    public void set_$1s(List<Entity> _$1s) {
        this._$1s = _$1s;
    }

    public List<Entity> get_$7s() {
        return _$7s;
    }

    public void set_$7s(List<Entity> _$7s) {
        this._$7s = _$7s;
    }

    public List<Entity> get_$6s() {
        return _$6s;
    }

    public void set_$6s(List<Entity> _$6s) {
        this._$6s = _$6s;
    }

    public List<Entity> get_$5s() {
        return _$5s;
    }

    public void set_$5s(List<Entity> _$5s) {
        this._$5s = _$5s;
    }

    public List<Entity> get_$4s() {
        return _$4s;
    }

    public void set_$4s(List<Entity> _$4s) {
        this._$4s = _$4s;
    }

    public static class Entity {
        /**
         * name : 英语
         * tname : (gasry1989)
         */

        private String name;
        private String tname;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getTname() {
            return tname;
        }

        public void setTname(String tname) {
            this.tname = tname;
        }
    }

}
