package classign.election.kingvar.come.electionclasssign.module.main.studentmien;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import classign.election.kingvar.come.electionclasssign.R;
import classign.election.kingvar.come.electionclasssign.module.main.studentmien.classreward.ClassRewardFragment;
import classign.election.kingvar.come.electionclasssign.module.main.studentmien.encourage.EncourageTalkFragment;
import classign.election.kingvar.come.electionclasssign.module.main.studentmien.mydream.MyDreamFragment;
import classign.election.kingvar.come.electionclasssign.module.main.studentmien.schoolreward.SchoolRewardFragment;
import classign.election.kingvar.come.electionclasssign.module.main.studentmien.todaystar.TodayStarFragment;
import classign.election.kingvar.come.electionclasssign.module.main.weekdays.IMainActivityPresenter;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/7/18 0018 15:21
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public class StudentMienPersenter implements IMainActivityPresenter {
    private final StudentMienInterfaceView mView;

    private TodayStarFragment todayStarFragment;
    private ClassRewardFragment classRewardFragment;
    private EncourageTalkFragment encourageTalkFragment;
    private MyDreamFragment myDreamFragment;
    private SchoolRewardFragment schoolRewardFragment;
    public StudentMienPersenter(StudentMienActivity studentMienActivity) {
        this.mView = studentMienActivity;
    }

    @Override
    public void getData(boolean isRefresh) {

    }

    @Override
    public void getMoreData() {

    }

    @Override
    public void showClassStatus(Context context, int checkedId, FragmentManager manager, String mIsBig) {

    }

    @Override
    public void showStudentMien(Context context, int checkedId, FragmentManager manager) {
        FragmentTransaction transaction = manager.beginTransaction();
        if (checkedId == R.id.radio_btn_one) {
            if (todayStarFragment == null) {
                todayStarFragment = new TodayStarFragment();
            }
            transaction.replace(R.id.fl_student_mien_content, todayStarFragment);
        } else if (checkedId == R.id.radio_btn_two) {
            if (classRewardFragment == null) {
                classRewardFragment = new ClassRewardFragment();
            }
            transaction.replace(R.id.fl_student_mien_content, classRewardFragment);
        } else if (checkedId == R.id.radio_btn_three) {
            if (schoolRewardFragment == null) {
                schoolRewardFragment = new SchoolRewardFragment();
            }

            transaction.replace(R.id.fl_student_mien_content, schoolRewardFragment);
        } else if (checkedId == R.id.radio_btn_four) {
            if (myDreamFragment == null) {
                myDreamFragment = new MyDreamFragment();
            }
            transaction.replace(R.id.fl_student_mien_content, myDreamFragment);
        } else if (checkedId == R.id.radio_btn_five) {
            if (encourageTalkFragment == null) {
                encourageTalkFragment = new EncourageTalkFragment();
            }

            transaction.replace(R.id.fl_student_mien_content, encourageTalkFragment);
        }
        transaction.commitAllowingStateLoss();
        mView.setStudentMien(checkedId);
    }
}
