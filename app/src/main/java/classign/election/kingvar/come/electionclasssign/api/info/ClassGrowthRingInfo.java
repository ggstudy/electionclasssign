package classign.election.kingvar.come.electionclasssign.api.info;

import java.util.List;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/29 0029 15:51
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public class ClassGrowthRingInfo {


    /**
     * code : 0
     * clsring : [{"date":1482919588000,"clsid":"7","name":"九年级八班","id":5,"detail":"班级照了毕业照，我们即将进入中考，即将毕业","title":"第二学期"},{"date":1459159547000,"clsid":"7","name":"九年级八班","id":4,"detail":"全校师生展开\u201c红色革命文化\u201d给抗战老兵扫墓","title":"第一学期"},{"date":1459159440000,"clsid":"7","name":"九年级八班","id":3,"detail":"班级在校运动会上取得优异的成绩","title":"第一学期"},{"date":1448704950000,"clsid":"7","name":"九年级八班","id":2,"detail":"小明同学的\u201c我的父情节\u201d获得了第十四届少年作家杯作文比赛的银奖！","title":"第二学期"},{"date":1430215246000,"clsid":"7","name":"九年级八班","id":1,"detail":"欢迎新生报到，学校对我们年纪举办了欢迎典礼","title":"第一学期"}]
     */

    private int code;
    private List<ClsringEntity> clsring;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<ClsringEntity> getClsring() {
        return clsring;
    }

    public void setClsring(List<ClsringEntity> clsring) {
        this.clsring = clsring;
    }

    public static class ClsringEntity {
        /**
         * date : 1482919588000
         * clsid : 7
         * name : 九年级八班
         * id : 5
         * detail : 班级照了毕业照，我们即将进入中考，即将毕业
         * title : 第二学期
         */

        private long date;
        private String clsid;
        private String name;
        private int id;
        private String detail;
        private String title;

        public long getDate() {
            return date;
        }

        public void setDate(long date) {
            this.date = date;
        }

        public String getClsid() {
            return clsid;
        }

        public void setClsid(String clsid) {
            this.clsid = clsid;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getDetail() {
            return detail;
        }

        public void setDetail(String detail) {
            this.detail = detail;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }
}
