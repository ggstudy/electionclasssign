package classign.election.kingvar.come.electionclasssign.api.info;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/17 0017 14:24
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public class AttendanceInfo  implements Parcelable{


    /**
     * code : 0
     * inLeave : 0
     * data : [{"reason":"应到","num":44,"time":1489994761000,"type":1},{"reason":"实到","num":1,"time":1489994763000,"type":1},{"reason":"动作不规范","num":-2,"time":1489999592000,"type":0}]
     * snum : 29
     * classStu : 0
     */

    private int code;
    private int inLeave;
    private int snum;
    private int classStu;
    private List<DataEntity> data;

    protected AttendanceInfo(Parcel in) {
        code = in.readInt();
        inLeave = in.readInt();
        snum = in.readInt();
        classStu = in.readInt();
        data = in.createTypedArrayList(DataEntity.CREATOR);
    }

    public static final Creator<AttendanceInfo> CREATOR = new Creator<AttendanceInfo>() {
        @Override
        public AttendanceInfo createFromParcel(Parcel in) {
            return new AttendanceInfo(in);
        }

        @Override
        public AttendanceInfo[] newArray(int size) {
            return new AttendanceInfo[size];
        }
    };

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getInLeave() {
        return inLeave;
    }

    public void setInLeave(int inLeave) {
        this.inLeave = inLeave;
    }

    public int getSnum() {
        return snum;
    }

    public void setSnum(int snum) {
        this.snum = snum;
    }

    public int getClassStu() {
        return classStu;
    }

    public void setClassStu(int classStu) {
        this.classStu = classStu;
    }

    public List<DataEntity> getData() {
        return data;
    }

    public void setData(List<DataEntity> data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(code);
        dest.writeInt(inLeave);
        dest.writeInt(snum);
        dest.writeInt(classStu);
        dest.writeTypedList(data);
    }

    public static class DataEntity implements Parcelable{
        /**
         * reason : 应到
         * num : 44
         * time : 1489994761000
         * type : 1
         */

        private String reason;
        private int num;
        private long time;
        private int type;

        protected DataEntity(Parcel in) {
            reason = in.readString();
            num = in.readInt();
            time = in.readLong();
            type = in.readInt();
        }

        public static final Creator<DataEntity> CREATOR = new Creator<DataEntity>() {
            @Override
            public DataEntity createFromParcel(Parcel in) {
                return new DataEntity(in);
            }

            @Override
            public DataEntity[] newArray(int size) {
                return new DataEntity[size];
            }
        };

        public String getReason() {
            return reason;
        }

        public void setReason(String reason) {
            this.reason = reason;
        }

        public int getNum() {
            return num;
        }

        public void setNum(int num) {
            this.num = num;
        }

        public long getTime() {
            return time;
        }

        public void setTime(long time) {
            this.time = time;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(reason);
            dest.writeInt(num);
            dest.writeLong(time);
            dest.writeInt(type);
        }
    }
}
