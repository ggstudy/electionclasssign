package classign.election.kingvar.come.electionclasssign.module.main.punchcard;

import com.orhanobut.logger.Logger;

import classign.election.kingvar.come.electionclasssign.api.RetrofitService;
import classign.election.kingvar.come.electionclasssign.api.info.ProIsCodeSuccess;
import classign.election.kingvar.come.electionclasssign.api.info.ProMessageContent;
import rx.Subscriber;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/7/7 0007 14:42
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public class PunchCardPersenter implements PunchCardPresenter {
    private final IPunchCardView mView;

    public PunchCardPersenter(PunchCardActivity mActivity) {
        this.mView = mActivity;
    }

    @Override
    public void getData(boolean isRefresh) {

    }

    @Override
    public void getMoreData() {

    }

    @Override
    public void getMessageContent(int studentid) {
        RetrofitService.getMessageContent(studentid)
                .compose(mView.bindToLife())
                .subscribe(new Subscriber<ProMessageContent>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(ProMessageContent proMessageContent) {
                        if (proMessageContent.getCode() == 0) {
                            mView.loadMessageData(proMessageContent.getData());
                            mView.loadTimeTableData(proMessageContent.getTList());
                        }
                    }
                });
    }

    @Override
    public void sendMessageContent(int studentId, int messagetID, String content) {
        RetrofitService.sendMessageContent(studentId, messagetID, content)
                .compose(mView.bindToLife())
                .subscribe(new Subscriber<ProIsCodeSuccess>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(ProIsCodeSuccess proIsCodeSuccess) {
                        if (proIsCodeSuccess.getCode() == 0) {
                            mView.sendMessageSuccess();
                        }
                    }
                });
    }

    @Override
    public void postUpLoadData(String imei, String schoolid, String date, String data) {
        RetrofitService.postUpLoadData(imei, schoolid, date, data)
                .compose(mView.bindToLife())
                .subscribe(new Subscriber<ProIsCodeSuccess>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Logger.e(e.toString());
                        mView.postUploadDataFail();
                    }

                    @Override
                    public void onNext(ProIsCodeSuccess proIsCodeSuccess) {
                        if (proIsCodeSuccess.getCode() == 0) {
                            mView.postUploadDataSuccess();
                        } else {
                            mView.postUploadDataFail();
                        }
                    }
                });
    }
}
