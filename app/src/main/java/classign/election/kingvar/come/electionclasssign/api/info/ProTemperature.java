package classign.election.kingvar.come.electionclasssign.api.info;

import java.util.List;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/2/27 0027 17:27
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public class ProTemperature {

    /**
     * data : {"yesterday":{"date":"23日星期二","high":"高温 30℃","fx":"无持续风向","low":"低温 24℃","fl":"微风","type":"小雨"},"city":"深圳","aqi":"22","forecast":[{"date":"24日星期三","high":"高温 28℃","fengli":"微风级","low":"低温 25℃","fengxiang":"无持续风向","type":"中到大雨"},{"date":"25日星期四","high":"高温 30℃","fengli":"微风级","low":"低温 24℃","fengxiang":"无持续风向","type":"多云"},{"date":"26日星期五","high":"高温 29℃","fengli":"微风级","low":"低温 24℃","fengxiang":"无持续风向","type":"小雨"},{"date":"27日星期六","high":"高温 32℃","fengli":"微风级","low":"低温 25℃","fengxiang":"无持续风向","type":"多云"},{"date":"28日星期天","high":"高温 31℃","fengli":"微风级","low":"低温 25℃","fengxiang":"无持续风向","type":"多云"}],"ganmao":"各项气象条件适宜，无明显降温过程，发生感冒机率较低。","wendu":"25"}
     * status : 1000
     * desc : OK
     */

    private DataEntity data;
    private int status;
    private String desc;

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public static class DataEntity {
        /**
         * yesterday : {"date":"23日星期二","high":"高温 30℃","fx":"无持续风向","low":"低温 24℃","fl":"微风","type":"小雨"}
         * city : 深圳
         * aqi : 22
         * forecast : [{"date":"24日星期三","high":"高温 28℃","fengli":"微风级","low":"低温 25℃","fengxiang":"无持续风向","type":"中到大雨"},{"date":"25日星期四","high":"高温 30℃","fengli":"微风级","low":"低温 24℃","fengxiang":"无持续风向","type":"多云"},{"date":"26日星期五","high":"高温 29℃","fengli":"微风级","low":"低温 24℃","fengxiang":"无持续风向","type":"小雨"},{"date":"27日星期六","high":"高温 32℃","fengli":"微风级","low":"低温 25℃","fengxiang":"无持续风向","type":"多云"},{"date":"28日星期天","high":"高温 31℃","fengli":"微风级","low":"低温 25℃","fengxiang":"无持续风向","type":"多云"}]
         * ganmao : 各项气象条件适宜，无明显降温过程，发生感冒机率较低。
         * wendu : 25
         */

        private YesterdayEntity yesterday;
        private String city;
        private String aqi;
        private String ganmao;
        private String wendu;
        private List<ForecastEntity> forecast;

        public YesterdayEntity getYesterday() {
            return yesterday;
        }

        public void setYesterday(YesterdayEntity yesterday) {
            this.yesterday = yesterday;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getAqi() {
            return aqi;
        }

        public void setAqi(String aqi) {
            this.aqi = aqi;
        }

        public String getGanmao() {
            return ganmao;
        }

        public void setGanmao(String ganmao) {
            this.ganmao = ganmao;
        }

        public String getWendu() {
            return wendu;
        }

        public void setWendu(String wendu) {
            this.wendu = wendu;
        }

        public List<ForecastEntity> getForecast() {
            return forecast;
        }

        public void setForecast(List<ForecastEntity> forecast) {
            this.forecast = forecast;
        }

        public static class YesterdayEntity {
            /**
             * date : 23日星期二
             * high : 高温 30℃
             * fx : 无持续风向
             * low : 低温 24℃
             * fl : 微风
             * type : 小雨
             */

            private String date;
            private String high;
            private String fx;
            private String low;
            private String fl;
            private String type;

            public String getDate() {
                return date;
            }

            public void setDate(String date) {
                this.date = date;
            }

            public String getHigh() {
                return high;
            }

            public void setHigh(String high) {
                this.high = high;
            }

            public String getFx() {
                return fx;
            }

            public void setFx(String fx) {
                this.fx = fx;
            }

            public String getLow() {
                return low;
            }

            public void setLow(String low) {
                this.low = low;
            }

            public String getFl() {
                return fl;
            }

            public void setFl(String fl) {
                this.fl = fl;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }
        }

        public static class ForecastEntity {
            /**
             * date : 24日星期三
             * high : 高温 28℃
             * fengli : 微风级
             * low : 低温 25℃
             * fengxiang : 无持续风向
             * type : 中到大雨
             */

            private String date;
            private String high;
            private String fengli;
            private String low;
            private String fengxiang;
            private String type;

            public String getDate() {
                return date;
            }

            public void setDate(String date) {
                this.date = date;
            }

            public String getHigh() {
                return high;
            }

            public void setHigh(String high) {
                this.high = high;
            }

            public String getFengli() {
                return fengli;
            }

            public void setFengli(String fengli) {
                this.fengli = fengli;
            }

            public String getLow() {
                return low;
            }

            public void setLow(String low) {
                this.low = low;
            }

            public String getFengxiang() {
                return fengxiang;
            }

            public void setFengxiang(String fengxiang) {
                this.fengxiang = fengxiang;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }
        }
    }
}
