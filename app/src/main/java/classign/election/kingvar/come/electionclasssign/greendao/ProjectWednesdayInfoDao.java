package classign.election.kingvar.come.electionclasssign.greendao;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.Property;
import org.greenrobot.greendao.internal.DaoConfig;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.database.DatabaseStatement;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table "PROJECT_WEDNESDAY_INFO".
*/
public class ProjectWednesdayInfoDao extends AbstractDao<ProjectWednesdayInfo, Long> {

    public static final String TABLENAME = "PROJECT_WEDNESDAY_INFO";

    /**
     * Properties of entity ProjectWednesdayInfo.<br/>
     * Can be used for QueryBuilder and for referencing column names.
     */
    public static class Properties {
        public final static Property Id = new Property(0, Long.class, "id", true, "_id");
        public final static Property Tag = new Property(1, String.class, "tag", false, "TAG");
        public final static Property Num = new Property(2, String.class, "num", false, "NUM");
        public final static Property Course = new Property(3, String.class, "course", false, "COURSE");
        public final static Property Tname = new Property(4, String.class, "tname", false, "TNAME");
        public final static Property Logo = new Property(5, String.class, "logo", false, "LOGO");
        public final static Property Start = new Property(6, String.class, "start", false, "START");
        public final static Property End = new Property(7, String.class, "end", false, "END");
    }


    public ProjectWednesdayInfoDao(DaoConfig config) {
        super(config);
    }
    
    public ProjectWednesdayInfoDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(Database db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "\"PROJECT_WEDNESDAY_INFO\" (" + //
                "\"_id\" INTEGER PRIMARY KEY AUTOINCREMENT ," + // 0: id
                "\"TAG\" TEXT," + // 1: tag
                "\"NUM\" TEXT UNIQUE ," + // 2: num
                "\"COURSE\" TEXT," + // 3: course
                "\"TNAME\" TEXT," + // 4: tname
                "\"LOGO\" TEXT," + // 5: logo
                "\"START\" TEXT," + // 6: start
                "\"END\" TEXT);"); // 7: end
    }

    /** Drops the underlying database table. */
    public static void dropTable(Database db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "\"PROJECT_WEDNESDAY_INFO\"";
        db.execSQL(sql);
    }

    @Override
    protected final void bindValues(DatabaseStatement stmt, ProjectWednesdayInfo entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        String tag = entity.getTag();
        if (tag != null) {
            stmt.bindString(2, tag);
        }
 
        String num = entity.getNum();
        if (num != null) {
            stmt.bindString(3, num);
        }
 
        String course = entity.getCourse();
        if (course != null) {
            stmt.bindString(4, course);
        }
 
        String tname = entity.getTname();
        if (tname != null) {
            stmt.bindString(5, tname);
        }
 
        String logo = entity.getLogo();
        if (logo != null) {
            stmt.bindString(6, logo);
        }
 
        String start = entity.getStart();
        if (start != null) {
            stmt.bindString(7, start);
        }
 
        String end = entity.getEnd();
        if (end != null) {
            stmt.bindString(8, end);
        }
    }

    @Override
    protected final void bindValues(SQLiteStatement stmt, ProjectWednesdayInfo entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        String tag = entity.getTag();
        if (tag != null) {
            stmt.bindString(2, tag);
        }
 
        String num = entity.getNum();
        if (num != null) {
            stmt.bindString(3, num);
        }
 
        String course = entity.getCourse();
        if (course != null) {
            stmt.bindString(4, course);
        }
 
        String tname = entity.getTname();
        if (tname != null) {
            stmt.bindString(5, tname);
        }
 
        String logo = entity.getLogo();
        if (logo != null) {
            stmt.bindString(6, logo);
        }
 
        String start = entity.getStart();
        if (start != null) {
            stmt.bindString(7, start);
        }
 
        String end = entity.getEnd();
        if (end != null) {
            stmt.bindString(8, end);
        }
    }

    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    @Override
    public ProjectWednesdayInfo readEntity(Cursor cursor, int offset) {
        ProjectWednesdayInfo entity = new ProjectWednesdayInfo( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // id
            cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1), // tag
            cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2), // num
            cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3), // course
            cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4), // tname
            cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5), // logo
            cursor.isNull(offset + 6) ? null : cursor.getString(offset + 6), // start
            cursor.isNull(offset + 7) ? null : cursor.getString(offset + 7) // end
        );
        return entity;
    }
     
    @Override
    public void readEntity(Cursor cursor, ProjectWednesdayInfo entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setTag(cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1));
        entity.setNum(cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2));
        entity.setCourse(cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3));
        entity.setTname(cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4));
        entity.setLogo(cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5));
        entity.setStart(cursor.isNull(offset + 6) ? null : cursor.getString(offset + 6));
        entity.setEnd(cursor.isNull(offset + 7) ? null : cursor.getString(offset + 7));
     }
    
    @Override
    protected final Long updateKeyAfterInsert(ProjectWednesdayInfo entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    @Override
    public Long getKey(ProjectWednesdayInfo entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    @Override
    public boolean hasKey(ProjectWednesdayInfo entity) {
        return entity.getId() != null;
    }

    @Override
    protected final boolean isEntityUpdateable() {
        return true;
    }
    
}
