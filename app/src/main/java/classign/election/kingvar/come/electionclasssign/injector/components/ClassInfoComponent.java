package classign.election.kingvar.come.electionclasssign.injector.components;


import classign.election.kingvar.come.electionclasssign.injector.PerFragment;
import classign.election.kingvar.come.electionclasssign.module.main.classmien.classinfo.ClassInfoFragment;
import classign.election.kingvar.come.electionclasssign.injector.modules.ClassInfoModule;
import dagger.Component;

/**
 * Created by long on 2016/8/19.
 */
@PerFragment
@Component(dependencies = ApplicationComponent.class, modules = {ClassInfoModule.class})
public interface ClassInfoComponent {
  void inject(ClassInfoFragment fragment);
}