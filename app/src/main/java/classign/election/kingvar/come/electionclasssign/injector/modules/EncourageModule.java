package classign.election.kingvar.come.electionclasssign.injector.modules;

import classign.election.kingvar.come.electionclasssign.injector.PerFragment;
import classign.election.kingvar.come.electionclasssign.module.base.IBasePresenter;
import classign.election.kingvar.come.electionclasssign.module.main.studentmien.encourage.EncouragePresenter;
import classign.election.kingvar.come.electionclasssign.module.main.studentmien.encourage.EncourageTalkFragment;
import dagger.Module;
import dagger.Provides;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/29 0029 17:24
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */
@Module
public class EncourageModule {
    private final EncourageTalkFragment encourageTalkFragment;
    private final String mStudentid;
    public EncourageModule(EncourageTalkFragment encourageTalkFragment, String studentid) {
        this.encourageTalkFragment = encourageTalkFragment;
        this.mStudentid = studentid;

    }

    @PerFragment
    @Provides
    public IBasePresenter provideMainPresenter(){
        return new EncouragePresenter(encourageTalkFragment,mStudentid);
    }
}
