package classign.election.kingvar.come.electionclasssign.api.info;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/31 0031 14:34
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public class BaseClassInfo {

    /**
     * code : 0
     * info : {"clsid":"7","schoolid":6,"cname":"九年级八班","imei":"6410503444044026078e","memo":"A班咯咯了","monitor":"哈哈","id":1}
     */

    private int code;
    private InfoEntity info;
    public int getCode() {
        return code;
    }
    public void setCode(int code) {
        this.code = code;
    }
    public InfoEntity getInfo() {
        return info;
    }
    public void setInfo(InfoEntity info) {
        this.info = info;
    }
    public static class InfoEntity {
        /**
         * clsid : 7
         * schoolid : 6
         * cname : 九年级八班
         * name : 李老师
         * imei : 201612271830
         * memo : A班咯咯了
         * monitor : 孙颖慧
         * id : 1
         * viceName : gasry1989
         */

        private String clsid;
        private int schoolid;
        private String cname;
        private String name;
        private String imei;
        private String memo;
        private String monitor;
        private int id;
        private String viceName;

        public String getClsid() {
            return clsid;
        }

        public void setClsid(String clsid) {
            this.clsid = clsid;
        }

        public int getSchoolid() {
            return schoolid;
        }

        public void setSchoolid(int schoolid) {
            this.schoolid = schoolid;
        }

        public String getCname() {
            return cname;
        }

        public void setCname(String cname) {
            this.cname = cname;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImei() {
            return imei;
        }

        public void setImei(String imei) {
            this.imei = imei;
        }

        public String getMemo() {
            return memo;
        }

        public void setMemo(String memo) {
            this.memo = memo;
        }

        public String getMonitor() {
            return monitor;
        }

        public void setMonitor(String monitor) {
            this.monitor = monitor;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getViceName() {
            return viceName;
        }

        public void setViceName(String viceName) {
            this.viceName = viceName;
        }
    }
}
