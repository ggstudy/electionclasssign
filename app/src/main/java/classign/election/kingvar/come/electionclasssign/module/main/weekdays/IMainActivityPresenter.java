package classign.election.kingvar.come.electionclasssign.module.main.weekdays;

import android.content.Context;
import android.support.v4.app.FragmentManager;

import classign.election.kingvar.come.electionclasssign.module.base.IBasePresenter;


/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/7/18 0018 11:33
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public interface IMainActivityPresenter extends IBasePresenter {
    void showClassStatus(Context context, int checkedId, FragmentManager manager, String mIsBig);

    void showStudentMien(Context context, int checkedId, FragmentManager manager);
}
