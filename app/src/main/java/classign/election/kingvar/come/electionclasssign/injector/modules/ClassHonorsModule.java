package classign.election.kingvar.come.electionclasssign.injector.modules;

import android.util.Log;

import classign.election.kingvar.come.electionclasssign.injector.PerFragment;
import classign.election.kingvar.come.electionclasssign.module.base.IBasePresenter;
import classign.election.kingvar.come.electionclasssign.module.main.classmien.classhonor.ClassHonorPersenter;
import classign.election.kingvar.come.electionclasssign.module.main.classmien.classhonor.ClassHonourFragment;
import classign.election.kingvar.come.electionclasssign.tools.bean.User;
import dagger.Module;
import dagger.Provides;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/16 0016 15:28
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */
@Module
public class ClassHonorsModule {
    private  final String TAG = "AttendanceRecoderModule";
    private final ClassHonourFragment mClassHonourFragment;

    public ClassHonorsModule(ClassHonourFragment classHonourFragment) {
        this.mClassHonourFragment=classHonourFragment;
    }

    @PerFragment
    @Provides
    public IBasePresenter provideTitlePresenter() {
        Log.i(TAG, "provideMainPresenter: 1231654646");
        return new ClassHonorPersenter(mClassHonourFragment,User.getClassId(mClassHonourFragment.getContext()));
    }

  /*  @Provides
    public ClassHonorPagerAdapter provideViewPagerAdapter() {
        return new ClassHonorPagerAdapter(mClassHonourFragment.getFragmentManager());
    }*/
}
