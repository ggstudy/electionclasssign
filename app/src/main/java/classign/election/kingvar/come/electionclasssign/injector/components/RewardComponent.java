package classign.election.kingvar.come.electionclasssign.injector.components;


import classign.election.kingvar.come.electionclasssign.injector.PerFragment;
import classign.election.kingvar.come.electionclasssign.module.main.studentmien.classreward.ClassRewardFragment;
import classign.election.kingvar.come.electionclasssign.injector.modules.ClassRewardModule;
import dagger.Component;

/**
 * Created by long on 2016/8/19.
 */
@PerFragment
@Component(dependencies = ApplicationComponent.class, modules = {ClassRewardModule.class})
public interface RewardComponent {
  void inject(ClassRewardFragment fragment);
}