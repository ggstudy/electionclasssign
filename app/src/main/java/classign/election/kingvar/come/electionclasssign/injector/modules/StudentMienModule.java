package classign.election.kingvar.come.electionclasssign.injector.modules;

import classign.election.kingvar.come.electionclasssign.injector.PerActivity;
import classign.election.kingvar.come.electionclasssign.module.main.studentmien.StudentMienActivity;
import classign.election.kingvar.come.electionclasssign.module.main.studentmien.StudentMienPersenter;
import classign.election.kingvar.come.electionclasssign.module.main.weekdays.IMainActivityPresenter;
import dagger.Module;
import dagger.Provides;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/16 0016 15:28
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */
@Module
public class StudentMienModule {
    private final StudentMienActivity studentMienActivity;

    public StudentMienModule(StudentMienActivity studentMienActivity) {
        this.studentMienActivity = studentMienActivity;
    }

    @PerActivity
    @Provides
    public IMainActivityPresenter providePresenter() {
        return new StudentMienPersenter(studentMienActivity);
    }


}
