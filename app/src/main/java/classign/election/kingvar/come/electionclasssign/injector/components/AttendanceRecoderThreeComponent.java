package classign.election.kingvar.come.electionclasssign.injector.components;


import classign.election.kingvar.come.electionclasssign.injector.PerFragment;
import classign.election.kingvar.come.electionclasssign.injector.modules.AttendanceRecoderThreeModule;
import classign.election.kingvar.come.electionclasssign.module.main.moraleducation.three.TodayAttendanceThreeFragment;
import dagger.Component;

/**
 * Created by long on 2016/8/19.
 */
@PerFragment
@Component(dependencies = ApplicationComponent.class, modules = {AttendanceRecoderThreeModule.class})
public interface AttendanceRecoderThreeComponent {
  void inject(TodayAttendanceThreeFragment fragment);
}