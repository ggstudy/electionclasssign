package classign.election.kingvar.come.electionclasssign.broadcastreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;


/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/4/18 0018 9:59
 * @des ${监听时间的变化  每一分钟监听一次}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public class TimeChangeReceiver extends BroadcastReceiver{
    private TimeEvent mTimeEventListener ;
    public void setOnTimeEventListener(TimeEvent mTimeEventListener){
        this.mTimeEventListener = mTimeEventListener;
    }
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction() == Intent.ACTION_TIME_TICK){
            long time = System.currentTimeMillis();
            mTimeEventListener.getCurrentTime(time);
        }
    }
    public void removeTimeEventListener(){
        mTimeEventListener = null;
    }
    public interface TimeEvent{
        void getCurrentTime(long time);
    }
}
