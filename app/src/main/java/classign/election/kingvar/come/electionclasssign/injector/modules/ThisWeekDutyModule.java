package classign.election.kingvar.come.electionclasssign.injector.modules;

import android.util.Log;

import java.util.ArrayList;

import classign.election.kingvar.come.electionclasssign.activity.ThisWeekDutyActivity;
import classign.election.kingvar.come.electionclasssign.adapter.DutyViewPagerAdapter;
import classign.election.kingvar.come.electionclasssign.injector.PerActivity;
import classign.election.kingvar.come.electionclasssign.module.base.IBasePresenter;
import classign.election.kingvar.come.electionclasssign.module.main.weekdays.ThisWeekDutyPersenter;
import classign.election.kingvar.come.electionclasssign.tools.bean.User;
import dagger.Module;
import dagger.Provides;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/16 0016 15:28
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */
@Module
public class ThisWeekDutyModule {
    private static final String TAG = "AttendanceRecoderModule";
    private final ThisWeekDutyActivity mThisWeekDutyActivity;
    private final ArrayList mTitles;

    public ThisWeekDutyModule(ThisWeekDutyActivity thisWeekDutyActivity, ArrayList mTitles) {
        this.mThisWeekDutyActivity = thisWeekDutyActivity;
        this.mTitles = mTitles;
    }

    @PerActivity
    @Provides
    public IBasePresenter provideTitlePresenter() {
        Log.i(TAG, "provideMainPresenter: 1231654646");
        return new ThisWeekDutyPersenter(mThisWeekDutyActivity, User.getClassId(mThisWeekDutyActivity));
    }

    @PerActivity
    @Provides
    public DutyViewPagerAdapter provideViewPagerAdapter() {
        return new DutyViewPagerAdapter(mThisWeekDutyActivity.getSupportFragmentManager(),mTitles );
    }
}
