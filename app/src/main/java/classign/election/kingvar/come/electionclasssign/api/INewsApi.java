package classign.election.kingvar.come.electionclasssign.api;


import classign.election.kingvar.come.electionclasssign.api.info.AllProjectInfo;
import classign.election.kingvar.come.electionclasssign.api.info.AllStudentInfo;
import classign.election.kingvar.come.electionclasssign.api.info.AttendanceInfo;
import classign.election.kingvar.come.electionclasssign.api.info.BaseClassInfo;
import classign.election.kingvar.come.electionclasssign.api.info.ClassGrowthRingInfo;
import classign.election.kingvar.come.electionclasssign.api.info.ClassHonorInfo;
import classign.election.kingvar.come.electionclasssign.api.info.ClassInfo;
import classign.election.kingvar.come.electionclasssign.api.info.EncourageTalkInfo;
import classign.election.kingvar.come.electionclasssign.api.info.HeartbeatCmd;
import classign.election.kingvar.come.electionclasssign.api.info.ProClassAndSchoolNotice;
import classign.election.kingvar.come.electionclasssign.api.info.ProClassSchedule;
import classign.election.kingvar.come.electionclasssign.api.info.ProIsCodeSuccess;
import classign.election.kingvar.come.electionclasssign.api.info.ProMessageContent;
import classign.election.kingvar.come.electionclasssign.api.info.ProMyDream;
import classign.election.kingvar.come.electionclasssign.api.info.ProToadyStar;
import classign.election.kingvar.come.electionclasssign.api.info.RewardInfo;
import classign.election.kingvar.come.electionclasssign.api.info.ThisWeekDutyInfo;
import classign.election.kingvar.come.electionclasssign.api.info.ThisWeekProjectInfo;
import classign.election.kingvar.come.electionclasssign.api.info.TitleInfo;
import classign.election.kingvar.come.electionclasssign.api.info.TodayDutyInfo;
import classign.election.kingvar.come.electionclasssign.api.info.VersionUpdateInfo;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;
import rx.Observable;

import static classign.election.kingvar.come.electionclasssign.api.RetrofitService.CACHE_CONTROL_NETWORK;

/**
 * Created by long on 2016/8/22.
 * API 接口
 */
public interface INewsApi {

    /**
     * 获取心跳信息
     * http://120.76.155.123:8081/brand/api/heartBeat?imei=4882FR
     *getHeartbeatInfo
     * @param imei 设备唯一序列号
     * @return
     */
    @Headers(CACHE_CONTROL_NETWORK)
    @GET("heartBeat")
    Observable<HeartbeatCmd> getHeartbeatCmd(@Query("imei") String imei);

    /**
     * 获取班级登陆基本信息
     * http://120.76.155.123:8081/brand/api/imeiLogiin?imei=6410503444044026078e
     *getAllStudentInfo
     * @param imei 设备唯一序列号
     * @return
     */
    @Headers(CACHE_CONTROL_NETWORK)
    @GET("imeiLogiin")
    Observable<BaseClassInfo> getBaseClassInfo(@Query("imei") String imei);


    /**
     * 获取学生全部信息
     * http://192.168.1.40:8081/brand/api/stuList?clsid=7
     *getAllStudentInfo
     * @param clsid 班级id
     * @return
     */
    @Headers(CACHE_CONTROL_NETWORK)
    @GET("stuList")
    Observable<AllStudentInfo> getAllStudentInfo(@Query("clsid") String clsid);
    /**
     * 获取考勤信息
     * http://127.0.0.1:8081/brand/api/EducationInfo?clsid=7&tid=2
     *
     * @param clsid 班级id
     * @param tid
     * @return
     */
    @Headers(CACHE_CONTROL_NETWORK)
    @GET("EducationInfo")
    Observable<AttendanceInfo> getNewsList(@Query("clsid") String clsid, @Query("tid") String tid);

    /**
     * 获取班级荣誉信息
     * http://localhost:8081/brand/api/honour?clsid=7
     *
     * @param clsid 班级id
     * @return
     */
    @Headers(CACHE_CONTROL_NETWORK)
    @GET("honour")
    Observable<ClassHonorInfo> getClassHonor(@Query("clsid") String clsid);
    /**
     * 获取班级年轮信息
     * http://localhost:8081/brand/api/clsRing?clsid=7
     *
     * @param clsid 班级id
     * @return
     */
    @Headers(CACHE_CONTROL_NETWORK)
    @GET("clsRing")
    Observable<ClassGrowthRingInfo> getClassGrowthRing(@Query("clsid") String clsid);

    /**
     * 获取班级风采信息
     * http://localhost:8081/brand/api/clsInfo?clsid=7
     *
     * @param clsid 班级id
     * @return
     */
    @Headers(CACHE_CONTROL_NETWORK)
    @GET("clsInfo")
    Observable<ClassInfo> getClassInfo(@Query("clsid") String clsid);

    /**
     * 获取考勤标题
     * eg: http://127.0.0.1:8081/brand/api/EducationList?schoolid=6
     *
     * @param schoolid 校园iD
     * @return
     */
    @Headers(CACHE_CONTROL_NETWORK)
    @GET("EducationList")
    Observable<TitleInfo> getAttendance(@Query("schoolid") String schoolid
            , @Query("clsid")String clsid);

    /**
     * 获取版本更新
     * http://localhost:8081/brand/api/VertionUpdate
     * @return
     */
    @Headers(CACHE_CONTROL_NETWORK)
    @GET("VertionUpdate")
    Observable<VersionUpdateInfo> getVersionInfo();

    /**
     * 获取今日值日信息
     * http://localhost:8081/brand/api/VertionUpdate
     * @return
     */
    @Headers(CACHE_CONTROL_NETWORK)
    @GET("onDuty")
    Observable<TodayDutyInfo> getTodayDutyInfo(@Query("clsid") String classid);

    /**
     * 获取本周值日信息
     * http://120.76.155.123:8081/brand/api/weekDuty?clsid=7
     * @return
     */
    @Headers(CACHE_CONTROL_NETWORK)
    @GET("weekDuty")
    Observable<ThisWeekDutyInfo> getThisWeekDutyInfo(@Query("clsid") String classid);

    /**
     * 获取本周值日信息
     * http://120.76.155.123:8081/brand/api/weekCourse?clsid=7
     * @return
     */
    @Headers(CACHE_CONTROL_NETWORK)
    @GET("weekCourse")
    Observable<ThisWeekProjectInfo> getThisWeekProjectInfo(@Query("clsid") String classid);

    /**
     * 获取班级和校园奖项信息
     * http://127.0.0.1:8081/brand/api/EducationInfo?clsid=7&tid=2
     *
     * @param type 班级id
     * @param studentid
     * @return
     */
    @Headers(CACHE_CONTROL_NETWORK)
    @GET("myWinning")
    Observable<RewardInfo> getRewardInfo(@Query("type") String type, @Query("studentid") String studentid);

    /**
     * 获取今日之星信息
     * http://120.76.155.123:8081/brand/api/todayStar?clsid=7
     *
     * @param clsid
     * @return
     */
    @Headers(CACHE_CONTROL_NETWORK)
    @GET("todayStar")
    Observable<ProToadyStar> getTodayStarInfo(@Query("clsid") String clsid);

    /**
     * 获取鼓励的话信息
     * http://120.76.155.123:8081/brand/api/myEncourage?studentid=2
     *
     * @param studentid
     * @return
     */
    @Headers(CACHE_CONTROL_NETWORK)
    @GET("myEncourage")
    Observable<EncourageTalkInfo> getEncourageInfo(@Query("studentid") String studentid);

    /**
     * 获取全部课程信息
     * http://120.76.155.123:8081/brand/api/weekCourse?clsid=51
     *
     * @param clsid 班级id
     * @return
     */
    @Headers(CACHE_CONTROL_NETWORK)
    @GET("weekCourse")
    Observable<AllProjectInfo> getAllProject(@Query("clsid") String clsid);

    /**
     * 获取公告的相关信息
     *  //http://localhost:8081/brand/api/notice?clsid=7&schoolid=6
     * @param clsid
     * @param schoolid
     * @return
     */
    @Headers(CACHE_CONTROL_NETWORK)
    @GET("notice")
    Observable<ProClassAndSchoolNotice> getNoticeInfo(@Query("clsid") String clsid, @Query("schoolid") String schoolid);

    /**
     * 获取日程管理信息
     * http://120.76.155.123:8081/brand/api/schedule?clsid=7
     *
     * @param clsid 班级id
     * @return
     */
    @Headers(CACHE_CONTROL_NETWORK)
    @GET("schedule")
    Observable<ProClassSchedule> getClassSchedule(@Query("clsid") String clsid);

    /**
     * 获取我的梦想
     * http://120.76.155.123:8081/brand/api/myDream?studentid=24
     * @param mStudentid
     * @return
     */
    @Headers(CACHE_CONTROL_NETWORK)
    @GET("myDream")
    Observable<ProMyDream> getMyDreamInfo(@Query("studentid")String mStudentid);

    /**
     * 获取留言的内容
     * @param studentid
     * @return
     */
    @GET("msgList")
    Observable<ProMessageContent> getMessageContent(@Query("studentid")int studentid);

    /**
     * 发送留言
     * @param studentId
     * @param messagetID
     * @param content
     * @return
     */
    @GET("msgPublish")
    Observable<ProIsCodeSuccess> sendMessageContent(@Query("studentid") int studentId, @Query("tostudentid") int messagetID, @Query("content") String content);
}
