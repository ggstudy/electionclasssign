package classign.election.kingvar.come.electionclasssign.module.main.studentmien.schoolreward;

import java.util.List;

import classign.election.kingvar.come.electionclasssign.api.info.RewardInfo;
import classign.election.kingvar.come.electionclasssign.module.base.IBaseView;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/5/16 0016 11:12
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public interface ISchoolRewardView extends IBaseView{
    void loadData( List<RewardInfo.WinningEntity> winningBeanList);
}
