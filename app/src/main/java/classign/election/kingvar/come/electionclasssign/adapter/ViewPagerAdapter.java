package classign.election.kingvar.come.electionclasssign.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import classign.election.kingvar.come.electionclasssign.module.main.moraleducation.eight.TodayAttendanceEightFragment;
import classign.election.kingvar.come.electionclasssign.module.main.moraleducation.five.TodayAttendanceFiveFragment;
import classign.election.kingvar.come.electionclasssign.module.main.moraleducation.four.TodayAttendanceFourFragment;
import classign.election.kingvar.come.electionclasssign.module.main.moraleducation.one.TodayAttendanceOneFragment;
import classign.election.kingvar.come.electionclasssign.module.main.moraleducation.seven.TodayAttendanceSevenFragment;
import classign.election.kingvar.come.electionclasssign.module.main.moraleducation.six.TodayAttendanceSixFragment;
import classign.election.kingvar.come.electionclasssign.module.main.moraleducation.three.TodayAttendanceThreeFragment;
import classign.election.kingvar.come.electionclasssign.module.main.moraleducation.two.TodayAttendanceTwoFragment;

/**
 * Created by long on 2016/6/2.
 * ViewPager适配器
 */
public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    private static final String TAG = "ViewPagerAdapter";
    List<String> mTitles;
    List<String> mId;
    List<Fragment> fragments;
    private TodayAttendanceOneFragment oneFragment;
    private TodayAttendanceTwoFragment twoFragment;
    private TodayAttendanceThreeFragment threeFragment;
    private TodayAttendanceFourFragment fourFragment;
    private TodayAttendanceFiveFragment fiveFragment;
    private TodayAttendanceSixFragment sixFragment;
    private TodayAttendanceSevenFragment sevenFragment;
    private TodayAttendanceEightFragment eightFragment;

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
        fragments = new ArrayList<>();
        mTitles = new ArrayList<String>();
        mId = new ArrayList<String>();
    }

    @Override
    public Fragment getItem(int position) {

        if (position == 1) {
            if (twoFragment == null) {
                twoFragment = TodayAttendanceTwoFragment.newInstance(mId.get(position));
            }
            return twoFragment;
        } else if (position == 2) {
            if (threeFragment == null) {
                threeFragment = TodayAttendanceThreeFragment.newInstance(mId.get(position));
            }
            return threeFragment;
        } else if (position == 3) {
            if (fourFragment == null) {
                fourFragment = TodayAttendanceFourFragment.newInstance(mId.get(position));
            }
            return fourFragment;
        } else if (position == 4) {
            if (fiveFragment == null) {
                fiveFragment = TodayAttendanceFiveFragment.newInstance(mId.get(position));
            }
            return fiveFragment;

        } else if (position == 5) {
            if (sixFragment == null) {
                sixFragment = TodayAttendanceSixFragment.newInstance(mId.get(position));
            }
            return sixFragment;

        } else if (position == 6) {
            if (sevenFragment == null) {
                sevenFragment = TodayAttendanceSevenFragment.newInstance(mId.get(position));
            }
            return sevenFragment;

        } else if (position == 7) {
            if (eightFragment == null) {
                eightFragment = TodayAttendanceEightFragment.newInstance(mId.get(position));
            }
            return eightFragment;
        } else if (position == 8) {

        } else if (position == 9) {

        }
        if (oneFragment == null) {
            oneFragment = TodayAttendanceOneFragment.newInstance(mId.get(position));
        }
        return oneFragment;
    }


    @Override
    public int getCount() {
        return mTitles.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTitles.get(position);
    }

    @Override
    public int getItemPosition(Object object) {
        return PagerAdapter.POSITION_NONE;
    }

    public void setItemsTitle(List<String> mTitles, List<String> mId) {
        this.mTitles = mTitles;
        this.mId = mId;
        notifyDataSetChanged();
    }

    public void setItems(List<Fragment> fragments, List<String> mTitles) {
        this.fragments = fragments;
        this.mTitles = mTitles;
        notifyDataSetChanged();
    }

    public void setItems(List<Fragment> fragments, String[] mTitles) {
        this.fragments = fragments;
        this.mTitles = Arrays.asList(mTitles);
        notifyDataSetChanged();
    }

    public void addItem(Fragment fragment, String title) {
        fragments.add(fragment);
        mTitles.add(title);
        notifyDataSetChanged();
    }

    public void delItem(int position) {
        mTitles.remove(position);
        fragments.remove(position);
        notifyDataSetChanged();
    }

    public int delItem(String title) {
        int index = mTitles.indexOf(title);
        if (index != -1) {
            delItem(index);
        }
        return index;
    }

    public void swapItems(int fromPos, int toPos) {
        Collections.swap(mTitles, fromPos, toPos);
        Collections.swap(fragments, fromPos, toPos);
        notifyDataSetChanged();
    }

    public void modifyTitle(int position, String title) {
        mTitles.set(position, title);
        notifyDataSetChanged();
    }


}
