package classign.election.kingvar.come.electionclasssign.module.main.studentmien.classreward;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import classign.election.kingvar.come.electionclasssign.R;
import classign.election.kingvar.come.electionclasssign.adapter.RecyclerAdapter;
import classign.election.kingvar.come.electionclasssign.adapter.RecyclerViewHolder;
import classign.election.kingvar.come.electionclasssign.api.info.RewardInfo;
import classign.election.kingvar.come.electionclasssign.base.BaseProtocol;
import classign.election.kingvar.come.electionclasssign.injector.components.DaggerRewardComponent;
import classign.election.kingvar.come.electionclasssign.injector.modules.ClassRewardModule;
import classign.election.kingvar.come.electionclasssign.module.base.BaseFragment;
import classign.election.kingvar.come.electionclasssign.module.base.IBasePresenter;
import classign.election.kingvar.come.electionclasssign.tools.bean.User;
import classign.election.kingvar.come.electionclasssign.tools.views.DividerItemDecoration;

/**
 * 班级奖励
 * Created by Administrator on 2016/12/20 0020.
 */

public class ClassRewardFragment extends BaseFragment<IBasePresenter> implements IRewardView {
    private static final String TAG = "ClassRewardFragment";
    private static final int FINSH = 1;
    private static final String TYPE = "type";
    @BindView(R.id.rv_school_reward)
    RecyclerView rv_school_reward;

    @BindView(R.id.tv_student_name)
    TextView tv_student_name; //姓名

    @BindView(R.id.iv_student_head)
    ImageView iv_student_head;  //头像
    private RecyclerAdapter mRecyclerAdapter;

    @Override
    protected int attachLayoutRes() {
        return R.layout.fragment_school_reward;
    }
    @Override
    protected void initInjector() {
      DaggerRewardComponent.builder()
                .applicationComponent(getAppComponent())
                .classRewardModule(new ClassRewardModule(this,"1",User.getStudentId(mContext)))
                .build()
                .inject(this);
    }

    @Override
    protected void initViews() {
        rv_school_reward.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        rv_school_reward.addItemDecoration(new DividerItemDecoration(mContext, LinearLayoutManager.HORIZONTAL, 1, getResources().getColor(R.color.bg_get_reward)));
    }

    @Override
    public void updateViews(boolean isRefresh) {
//        Logger.e("mPresenter" + mPresenter);
        mPresenter.getData(isRefresh);
    }


    @Override
    public void loadData(final List<RewardInfo.WinningEntity> winning) {
        Glide.with(getActivity()).load(BaseProtocol.IMG_BASE + User.getTodayStarLogo(getActivity()))
                .error(R.drawable.default_pic).into(iv_student_head);
        tv_student_name.setText(User.getTodayStarName(mContext));
        if (mRecyclerAdapter == null) {
            mRecyclerAdapter = new RecyclerAdapter(R.layout.school_reward_item, winning) {
                @Override
                protected void onBindData(RecyclerViewHolder holder, int position, Object item) {
                    holder.setText(R.id.tv_get_reward_content, winning.get(position).getDetail());
                    holder.setText(R.id.tv_get_reward_time_content, winning.get(position).getTitle());
                }
            };
            rv_school_reward.setAdapter(mRecyclerAdapter);
        } else {
            mRecyclerAdapter.setItemDate(winning);
        }
    }
}
