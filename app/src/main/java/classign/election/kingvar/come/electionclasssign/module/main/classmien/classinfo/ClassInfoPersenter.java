package classign.election.kingvar.come.electionclasssign.module.main.classmien.classinfo;

import android.util.Log;

import com.orhanobut.logger.Logger;

import java.util.ArrayList;

import classign.election.kingvar.come.electionclasssign.api.RetrofitService;
import classign.election.kingvar.come.electionclasssign.api.info.ClassInfo;
import classign.election.kingvar.come.electionclasssign.module.base.IBasePresenter;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/16 0016 15:05
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public class ClassInfoPersenter implements IBasePresenter {
    private static final String TAG = "AttendanceRecoderPersen";
    private final IClassInfoView mView;
    private final String mClsid;

    public ClassInfoPersenter(ClassInfoFragment classInfoFragment, String clsid) {
        mView = classInfoFragment;
        mClsid = clsid;
        Log.i(TAG, "provideMainPresenter: mClsid = " + mClsid);
        getData(true);
    }


    @Override
    public void getData(boolean isRefresh) {
        Log.i(TAG, "getData: mClsid =  " + mClsid);

        RetrofitService.getClassInfo(mClsid)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ClassInfo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Logger.e(e.toString());
                    }

                    @Override
                    public void onNext(ClassInfo classInfo) {
                        if (classInfo.getCode() == 0) {
                            if (classInfo.getInfo() != null) {
                                mView.loadData(classInfo.getInfo());
                            }
                            mView.loadClassroomDate((ArrayList<ClassInfo.ClassroomEntity>) classInfo.getClassroom());
                            mView.loadCommitteeDate((ArrayList<ClassInfo.CommitteeEntity>) classInfo.getCommittee());
                        }
                    }
                });

    }

    @Override
    public void getMoreData() {

    }
}
