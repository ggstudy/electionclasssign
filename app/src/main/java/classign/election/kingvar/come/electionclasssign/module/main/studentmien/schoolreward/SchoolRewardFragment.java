package classign.election.kingvar.come.electionclasssign.module.main.studentmien.schoolreward;


import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import classign.election.kingvar.come.electionclasssign.R;
import classign.election.kingvar.come.electionclasssign.adapter.RecyclerAdapter;
import classign.election.kingvar.come.electionclasssign.adapter.RecyclerViewHolder;
import classign.election.kingvar.come.electionclasssign.api.info.ProReward;
import classign.election.kingvar.come.electionclasssign.api.info.RewardInfo;
import classign.election.kingvar.come.electionclasssign.base.BaseProtocol;
import classign.election.kingvar.come.electionclasssign.injector.components.DaggerSchooolRewardComponent;
import classign.election.kingvar.come.electionclasssign.injector.modules.SchoolRewardModule;
import classign.election.kingvar.come.electionclasssign.module.base.BaseFragment;
import classign.election.kingvar.come.electionclasssign.module.base.IBasePresenter;
import classign.election.kingvar.come.electionclasssign.tools.bean.User;
import classign.election.kingvar.come.electionclasssign.tools.views.DividerItemDecoration;

/**
 * 校园及校外奖励
 * Created by Administrator on 2016/12/20 0020.
 */

public class SchoolRewardFragment extends BaseFragment<IBasePresenter> implements ISchoolRewardView{
	private static final String TAG = "SchoolRewardFragment";
	private static final int FINSH = 1;
	private List<ProReward.WinningBean> winningBeen;
	@BindView(R.id.rv_school_reward)
	RecyclerView rv_school_reward;
	private RecyclerAdapter mRecyclerAdapter;
	@BindView(R.id.tv_student_name)
	TextView tv_student_name; //姓名
	@BindView(R.id.iv_student_head)
	ImageView iv_student_head;  //头像

	@Override
	protected int attachLayoutRes() {
		return R.layout.fragment_school_reward;
	}

	@Override
	protected void initInjector() {
		DaggerSchooolRewardComponent.builder()
				.applicationComponent(getAppComponent())
				.schoolRewardModule(new SchoolRewardModule(this,"2",User.getStudentId(getActivity())))
				.build()
				.inject(this);
	}

	@Override
	protected void initViews() {
		rv_school_reward.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
		rv_school_reward.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.HORIZONTAL, 1, getResources().getColor(R.color.bg_get_reward)));
	}

	@Override
	public void updateViews(boolean isRefresh) {
		mPresenter.getData(true);
	}

	@Override
	public void loadData(List<RewardInfo.WinningEntity> winningBeanList) {
		Glide.with(getActivity()).load(BaseProtocol.IMG_BASE + User.getTodayStarLogo(getActivity()))
				.error(R.drawable.default_pic).into(iv_student_head);
		tv_student_name.setText(User.getTodayStarName(getActivity()));
		if (mRecyclerAdapter == null) {
			mRecyclerAdapter = new RecyclerAdapter<RewardInfo.WinningEntity>(R.layout.school_reward_item, winningBeanList) {
				@Override
				protected void onBindData(RecyclerViewHolder holder, int position, RewardInfo.WinningEntity item) {
					holder.setText(R.id.tv_get_reward_content, item.getDetail());
					holder.setText(R.id.tv_get_reward_time_content, item.getTitle());
				}
			};
			rv_school_reward.setAdapter(mRecyclerAdapter);
		} else {
			mRecyclerAdapter.setItemDate(winningBeanList);
		}
	}
}
