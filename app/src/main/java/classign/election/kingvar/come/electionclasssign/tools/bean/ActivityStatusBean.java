package classign.election.kingvar.come.electionclasssign.tools.bean;

import android.content.Context;

import classign.election.kingvar.come.electionclasssign.tools.utils.SPUtil;

/**
 * Created by JYQ on 2017/1/11 0011.
 */

public class ActivityStatusBean {

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public  boolean status = false;

}
