package classign.election.kingvar.come.electionclasssign.api.info;

/**
 * Created by pengyixing on 2017/5/7.
 */

import java.util.List;

/**
 * @author pyx
 * @version $Rev$
 * @time 2017/5/7
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public class HeartbeatCmd {
    /**
     * code : 2
     * cmd : [{"date":1492078564000,"imei":"4882FR","id":1,"type":0}]
     */

    private int code;
    private String imei;
    private List<CmdEntity> cmd;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public List<CmdEntity> getCmd() {
        return cmd;
    }

    public void setCmd(List<CmdEntity> cmd) {
        this.cmd = cmd;
    }

    public static class CmdEntity {
        /**
         * date : 1492078564000
         * imei : 4882FR
         * id : 1
         * type : 0
         */

        private long date;
        private String imei;
        private int id;
        private int type;

        public long getDate() {
            return date;
        }

        public void setDate(long date) {
            this.date = date;
        }

        public String getImei() {
            return imei;
        }

        public void setImei(String imei) {
            this.imei = imei;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }
    }
}
