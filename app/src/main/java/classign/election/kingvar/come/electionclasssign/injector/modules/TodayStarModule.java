package classign.election.kingvar.come.electionclasssign.injector.modules;

import classign.election.kingvar.come.electionclasssign.injector.PerFragment;
import classign.election.kingvar.come.electionclasssign.module.base.IBasePresenter;
import classign.election.kingvar.come.electionclasssign.module.main.studentmien.todaystar.TodayStarFragment;
import classign.election.kingvar.come.electionclasssign.module.main.studentmien.todaystar.TodayStarPresenter;
import dagger.Module;
import dagger.Provides;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/29 0029 17:24
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */
@Module
public class TodayStarModule {
    private final TodayStarFragment starFragment;
    private final String mClassid;
    public TodayStarModule(TodayStarFragment starFragment, String classid) {
        this.starFragment = starFragment;
        this.mClassid = classid;

    }

    @PerFragment
    @Provides
    public IBasePresenter provideMainPresenter(){
        return new TodayStarPresenter(starFragment,mClassid);
    }
}
