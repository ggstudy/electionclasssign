package classign.election.kingvar.come.electionclasssign.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioGroup;

import classign.election.kingvar.come.electionclasssign.R;
import classign.election.kingvar.come.electionclasssign.base.BaseActivity;
import classign.election.kingvar.come.electionclasssign.module.main.classmien.ClassMienActivity;
import classign.election.kingvar.come.electionclasssign.module.main.studentmien.StudentMienActivity;

/**
 * Created by Administrator on 2016/12/26 0026.
 */
public class MenuActivity extends BaseActivity implements RadioGroup.OnCheckedChangeListener {

    private RadioGroup radio_gp_menu_up;
    private RadioGroup radio_gp_menu_down;
    private ImageView iv_close_btn;

    @Override
    public void onRoot(Bundle savedInstanceState) {
        setContentView(R.layout.activity_menu);
    }

    @Override
    protected void initInjector() {

    }

    @Override
    protected void initView() {
        radio_gp_menu_up = (RadioGroup) findViewById(R.id.radio_gp_menu_up);
        radio_gp_menu_down = (RadioGroup) findViewById(R.id.radio_gp_menu_down);
        iv_close_btn = (ImageView) findViewById(R.id.iv_close_btn);

    }

    @Override
    public void initData() {

    }

    @Override
    public void initListener() {
        radio_gp_menu_up.setOnCheckedChangeListener(this);
        radio_gp_menu_down.setOnCheckedChangeListener(this);
        iv_close_btn.setOnClickListener(this);

    }

    @Override
    public void WidgetClick(View view) {
        switch (view.getId()) {
            case R.id.iv_close_btn:
                finish();
                break;

            default:
                break;
        }

    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (group.getId()) {
            case R.id.radio_gp_menu_up:
                switch (checkedId) {
                    case R.id.radio_btn_class_album:
                        Intent intent = new Intent(this, ClassAlbumActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.radio_btn_class_mien:
                        intent = new Intent(this, ClassMienActivity.class);
                        intent.putExtra("isBig", "isBig");
                        startActivity(intent);
                        break;
                    case R.id.radio_btn_persion_mien:
                        intent = new Intent(this, StudentMienActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.radio_btn_class_duty:
                        intent = new Intent(this, ThisWeekDutyActivity.class);
                        startActivity(intent);
                        break;
                }
                break;
            case R.id.radio_gp_menu_down:
                switch (checkedId) {
                    case R.id.radio_btn_school_date:
                        break;
                    case R.id.radio_btn_class_attendance:
                        Intent intent = new Intent(this, ToadyAttendanceActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.radio_btn_course_info:
                        intent = new Intent(this, ThisWeekProjectActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.radio_btn_setting:

                        break;
                }
                break;
        }
        finish();
    }
}
