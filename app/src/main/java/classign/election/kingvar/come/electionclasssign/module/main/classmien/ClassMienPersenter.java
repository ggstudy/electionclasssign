package classign.election.kingvar.come.electionclasssign.module.main.classmien;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import classign.election.kingvar.come.electionclasssign.R;
import classign.election.kingvar.come.electionclasssign.module.main.classmien.classgrowth.ClassGrowthRingFragment;
import classign.election.kingvar.come.electionclasssign.module.main.classmien.classhonor.ClassHonourFragment;
import classign.election.kingvar.come.electionclasssign.module.main.classmien.classinfo.ClassInfoFragment;
import classign.election.kingvar.come.electionclasssign.module.main.weekdays.IMainActivityPresenter;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/7/18 0018 15:00
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public class ClassMienPersenter implements IMainActivityPresenter {

    private ClassInfoFragment classInfoFragment;
    private ClassHonourFragment classHonourFragment;
    private ClassGrowthRingFragment classGrowthRingFragment;

    private final ClassMienActivity mienActivity;

    public ClassMienPersenter(ClassMienActivity classMienActivity) {
        this.mienActivity = classMienActivity;
    }

    @Override
    public void getData(boolean isRefresh) {

    }

    @Override
    public void getMoreData() {

    }

    @Override
    public void showClassStatus(Context context, int checkedId, FragmentManager manager, String mIsBig) {
        FragmentTransaction transaction =manager.beginTransaction();
        if (checkedId == R.id.radio_btn_class_info) {
            if (classInfoFragment == null) {
                classInfoFragment = ClassInfoFragment.newInstance(mIsBig);
            }
            transaction.replace(R.id.fl_class_status_content, classInfoFragment);
        } else if (checkedId == R.id.radio_btn_class_honour) {
            if (classHonourFragment == null) {
                classHonourFragment = new ClassHonourFragment();
            }
            transaction.replace(R.id.fl_class_status_content, classHonourFragment);
        } else if (checkedId == R.id.radio_btn_class_growth_ring) {
            if (classGrowthRingFragment == null) {
                classGrowthRingFragment = new ClassGrowthRingFragment();
            }
            transaction.replace(R.id.fl_class_status_content, classGrowthRingFragment);
        }
        transaction.commitAllowingStateLoss();
    }

    @Override
    public void showStudentMien(Context context, int checkedId, FragmentManager manager) {

    }
}
