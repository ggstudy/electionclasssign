package classign.election.kingvar.come.electionclasssign.module.main.punchcard;

import classign.election.kingvar.come.electionclasssign.module.base.IBasePresenter;


/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/7/7 0007 15:29
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public interface PunchCardPresenter extends IBasePresenter {
    void getMessageContent(int studentid);

    void sendMessageContent(int studentId, int messagetID, String content);

    void postUpLoadData(String imei, String schoolid, String date, String data);
}
