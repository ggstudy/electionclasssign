package classign.election.kingvar.come.electionclasssign.injector.modules;

import android.util.Log;

import classign.election.kingvar.come.electionclasssign.injector.PerFragment;
import classign.election.kingvar.come.electionclasssign.module.base.IBasePresenter;
import classign.election.kingvar.come.electionclasssign.module.main.moraleducation.four.AttendanceRecoderFourPersenter;
import classign.election.kingvar.come.electionclasssign.module.main.moraleducation.four.TodayAttendanceFourFragment;
import dagger.Module;
import dagger.Provides;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/16 0016 15:28
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */
@Module
public class AttendanceRecoderFourModule {
    private static final String TAG = "AttendanceRecoderModule";
    private final TodayAttendanceFourFragment mTodayAttendanceFragment;
    private final  String mClsid;
    private final  String mTid;
    public AttendanceRecoderFourModule(TodayAttendanceFourFragment fragment, String clsid, String tid) {
        this.mTodayAttendanceFragment = fragment;
        this.mClsid = clsid;
        this.mTid = tid;
    }



    @PerFragment
    @Provides
    public IBasePresenter provideMainPresenter() {
        Log.i(TAG, "provideMainPresenter: 1231654646");
        Log.i(TAG, "provideMainPresenter: mClsid = " + mClsid);
        Log.i(TAG, "provideMainPresenter: mTid = " + mTid);
        return new AttendanceRecoderFourPersenter(mTodayAttendanceFragment,mClsid,mTid);
    }

}
