package classign.election.kingvar.come.electionclasssign.activity;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import java.util.List;

import butterknife.BindView;
import classign.election.kingvar.come.electionclasssign.R;
import classign.election.kingvar.come.electionclasssign.api.info.AllProjectInfo;
import classign.election.kingvar.come.electionclasssign.module.base.BaseActivity;
import classign.election.kingvar.come.electionclasssign.adapter.RecyclerAdapter;
import classign.election.kingvar.come.electionclasssign.adapter.RecyclerViewHolder;
import classign.election.kingvar.come.electionclasssign.tools.utils.SearchEveryDayProjectUtil;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/28 0028 11:58
 * @des ${本周课程界面}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public class ThisWeekProjectActivity extends BaseActivity implements View.OnClickListener {
    private static final String TAG = "ThisWeekProjectActivity";
    @BindView(R.id.iv_close_btn)
    ImageView mIvCloseBtn;

    @BindView(R.id.rv_this_week_project)
    RecyclerView mRvThisWeekProject;
    private RecyclerAdapter mRecyclerAdapter;

    @Override
    protected int attachLayoutRes() {
        return R.layout.activity_this_week_project;
    }

    @Override
    protected void initInjector() {
    }

    @Override
    protected void initViews() {
        mRvThisWeekProject.setLayoutManager(new GridLayoutManager(this, 8));
        final List<AllProjectInfo.Entity> allProject = SearchEveryDayProjectUtil.getAllProject();
        if (mRecyclerAdapter == null) {
            mRecyclerAdapter = new RecyclerAdapter(R.layout.item_week_project, allProject) {
                @Override
                protected void onBindData(RecyclerViewHolder holder, int position, Object item) {
                    holder.setText(R.id.tv_project_name, allProject.get(position).getCourse());
                    holder.setText(R.id.tv_teacher_name, allProject.get(position).getTname());
                }
            };
            mRvThisWeekProject.setAdapter(mRecyclerAdapter);
        } else {
            mRecyclerAdapter.setItemDate(allProject);
        }
        mIvCloseBtn.setOnClickListener(this);
    }

    @Override
    protected void updateViews(boolean isRefresh) {
    }

    @Override
    public void onClick(View v) {
        finish();
    }
}
