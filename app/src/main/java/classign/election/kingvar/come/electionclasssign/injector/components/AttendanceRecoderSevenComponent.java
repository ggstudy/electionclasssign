package classign.election.kingvar.come.electionclasssign.injector.components;


import classign.election.kingvar.come.electionclasssign.injector.PerFragment;
import classign.election.kingvar.come.electionclasssign.injector.modules.AttendanceRecoderSevenModule;
import classign.election.kingvar.come.electionclasssign.module.main.moraleducation.seven.TodayAttendanceSevenFragment;
import dagger.Component;

/**
 * Created by long on 2016/8/19.
 */
@PerFragment
@Component(dependencies = ApplicationComponent.class, modules = {AttendanceRecoderSevenModule.class})
public interface AttendanceRecoderSevenComponent {
  void inject(TodayAttendanceSevenFragment fragment);
}