package classign.election.kingvar.come.electionclasssign.api.info;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/20 0020 20:33
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public class TitleInfo implements Parcelable{


    /**
     * code : 0
     * data : [{"id":1,"title":"打卡记录"},{"id":2,"title":"课间操"},{"id":3,"title":"眼保健操"},{"id":6,"title":"公共场所卫生"}]
     * sum : 98
     */

    private int code;
    private int sum;
    private List<DataEntity> data;

    protected TitleInfo(Parcel in) {
        code = in.readInt();
        sum = in.readInt();
    }

    public static final Creator<TitleInfo> CREATOR = new Creator<TitleInfo>() {
        @Override
        public TitleInfo createFromParcel(Parcel in) {
            return new TitleInfo(in);
        }

        @Override
        public TitleInfo[] newArray(int size) {
            return new TitleInfo[size];
        }
    };

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }

    public List<DataEntity> getData() {
        return data;
    }

    public void setData(List<DataEntity> data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(code);
        dest.writeInt(sum);
    }

    public static class DataEntity implements Parcelable {
        /**
         * id : 1
         * title : 打卡记录
         */

        private int id;
        private String title;

        protected DataEntity(Parcel in) {
            id = in.readInt();
            title = in.readString();
        }

        public static final Creator<DataEntity> CREATOR = new Creator<DataEntity>() {
            @Override
            public DataEntity createFromParcel(Parcel in) {
                return new DataEntity(in);
            }

            @Override
            public DataEntity[] newArray(int size) {
                return new DataEntity[size];
            }
        };

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(id);
            dest.writeString(title);
        }
    }
}
