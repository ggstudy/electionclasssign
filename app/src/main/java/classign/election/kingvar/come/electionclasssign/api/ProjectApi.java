package classign.election.kingvar.come.electionclasssign.api;


import classign.election.kingvar.come.electionclasssign.api.info.ProTemperature;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by long on 2016/8/22.
 * API 接口
 */
public interface ProjectApi {

    /**
     * http://api.avatardata.cn/Weather/Query?key=ad4cff6691e245aaabc18f082fb62f3e&cityname=深圳
     *http://wthrcdn.etouch.cn/weather_mini?city=深圳
     * @param city
     * @return
     */
    @GET("weather_mini")
    Observable<ProTemperature> getTemperatureInfo(@Query("city") String city);
}
