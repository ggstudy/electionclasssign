package classign.election.kingvar.come.electionclasssign.module.main.classmien.classhonor;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import butterknife.BindView;
import classign.election.kingvar.come.electionclasssign.R;
import classign.election.kingvar.come.electionclasssign.adapter.ClassHonorPagerAdapter;
import classign.election.kingvar.come.electionclasssign.api.info.ClassHonorInfo;
import classign.election.kingvar.come.electionclasssign.injector.components.DaggerClassHonorComponent;
import classign.election.kingvar.come.electionclasssign.injector.modules.ClassHonorsModule;
import classign.election.kingvar.come.electionclasssign.module.base.BaseFragment;
import classign.election.kingvar.come.electionclasssign.module.base.IBasePresenter;
import classign.election.kingvar.come.electionclasssign.tools.utils.ToastUtils;
import classign.election.kingvar.come.electionclasssign.tools.views.ZoomOutPageTransformer;

/**
 * 班级荣誉fragment
 * Created by Administrator on 2016/12/21 0021.
 */

public class ClassHonourFragment extends BaseFragment<IBasePresenter> implements IClassHonorView, View.OnClickListener, ViewPager.OnPageChangeListener {
    private static final String TAG = "ClassHonourFragment";
    private static String ClassModel = "classmodel";
    private static String ISBIG = "mIsBig";
    @BindView(R.id.id_viewpager)
    ViewPager mViewPager;
    @BindView(R.id.class_model_viewpager)
    ViewPager mClassModelViewPager;
    @BindView(R.id.relative_class_honour)
    RelativeLayout mRelativecClassHonour;

    @BindView(R.id.iv_button_right)
    ImageView mIvButtonRight;

    @BindView(R.id.iv_button_left)
    ImageView mIvButtonLeft;

    private int mTotalSize;   //总图片页数
    private int mCurrentViewID = 0;         //当前页面
    private String classModel;  //是否是上课标志(如果为null 则是标准模式, 如果不为null则为上课模式)
    private ClassHonorPagerAdapter mClassHonorPagerAdapter;

    @Override
    protected int attachLayoutRes() {
        return R.layout.fragment_class_honour;
    }

    @Override
    protected void initInjector() {
        DaggerClassHonorComponent.builder()
                .applicationComponent(getAppComponent())
                .classHonorsModule(new ClassHonorsModule(this))
                .build()
                .inject(this);
    }

    @Override
    protected void initViews() {

        //设置Page间间距
//        mViewPager.setPageMargin(5);
        //设置Page缓存页数值
        if (getArguments() != null) {
            classModel = getArguments().getString(ClassModel);
        }
        if (classModel == null) {
            mViewPager.setOffscreenPageLimit(3);
            mViewPager.setPageTransformer(true, new ZoomOutPageTransformer());
        } else {
            mViewPager.setVisibility(View.GONE);
            mRelativecClassHonour.setVisibility(View.VISIBLE);
        }
//
    }

    @Override
    public void updateViews(boolean isRefresh) {
//        mPresenter.getData(isRefresh);
        mIvButtonRight.setOnClickListener(this);
        mIvButtonLeft.setOnClickListener(this);
        mViewPager.setOnPageChangeListener(this);
        mClassModelViewPager.setOnPageChangeListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        initInjector();
    }

    @Override
    public void loadClassHonorData(ClassHonorInfo classHonorInfo) {
        mTotalSize = classHonorInfo.getHonour().size();
        mClassHonorPagerAdapter = new ClassHonorPagerAdapter(classHonorInfo.getHonour(), getActivity());
        if (classModel == null) {
            mViewPager.setAdapter(mClassHonorPagerAdapter);
        } else {
            mClassModelViewPager.setAdapter(mClassHonorPagerAdapter);
        }
    }

    public static Fragment newInstance(String classModel) {
        ClassHonourFragment classHonourFragment = new ClassHonourFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ClassModel, classModel);
        classHonourFragment.setArguments(bundle);
        return classHonourFragment;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_button_left:
                if(mCurrentViewID != 0){
                    mCurrentViewID--;
                    mClassModelViewPager.setCurrentItem(mCurrentViewID, true);
                }
                break;
            case R.id.iv_button_right:
                if(mCurrentViewID != mTotalSize-1){
                    mCurrentViewID++;
                    mClassModelViewPager.setCurrentItem(mCurrentViewID, true);
                }
                break;

        }

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        mCurrentViewID = position;
        if (mCurrentViewID ==0){
            ToastUtils.showToast("已经是第一张照片");
        }else if (mCurrentViewID == mTotalSize-1){
            ToastUtils.showToast("已经是最后一张照片");
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
