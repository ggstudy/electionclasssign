package classign.election.kingvar.come.electionclasssign.api.info;

/**
 * Created by Administrator on 2017/1/4 0004.
 */
public class ProMyDream  {

	/**
	 * code : 0
	 * dream : {"studentid":"24","date":1483372800000,"img":"20170105100016669757.png","name":"杜平","logo":"20170105100016669757.png","id":4,"detail":"我的梦想是成为爱迪生一样的发明家"}
	 */

	private int code;
	private DreamBean dream;

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public DreamBean getDream() {
		return dream;
	}

	public void setDream(DreamBean dream) {
		this.dream = dream;
	}

	public static class DreamBean {
		/**
		 * studentid : 24
		 * date : 1483372800000
		 * img : 20170105100016669757.png
		 * name : 杜平
		 * logo : 20170105100016669757.png
		 * id : 4
		 * detail : 我的梦想是成为爱迪生一样的发明家
		 */

		private String studentid;
		private long date;
		private String img;
		private String name;
		private String logo;
		private int id;
		private String detail;

		public String getStudentid() {
			return studentid;
		}

		public void setStudentid(String studentid) {
			this.studentid = studentid;
		}

		public long getDate() {
			return date;
		}

		public void setDate(long date) {
			this.date = date;
		}

		public String getImg() {
			return img;
		}

		public void setImg(String img) {
			this.img = img;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getLogo() {
			return logo;
		}

		public void setLogo(String logo) {
			this.logo = logo;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getDetail() {
			return detail;
		}

		public void setDetail(String detail) {
			this.detail = detail;
		}
	}
}
