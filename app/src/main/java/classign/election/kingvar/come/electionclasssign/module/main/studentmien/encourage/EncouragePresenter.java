package classign.election.kingvar.come.electionclasssign.module.main.studentmien.encourage;

import classign.election.kingvar.come.electionclasssign.api.RetrofitService;
import classign.election.kingvar.come.electionclasssign.api.info.EncourageTalkInfo;
import classign.election.kingvar.come.electionclasssign.module.base.IBasePresenter;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/4/1 0001 16:44
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public class EncouragePresenter implements IBasePresenter {
    private final IEncourageView mView;
    private final String mStudentid;

    public EncouragePresenter(EncourageTalkFragment encourageTalkFragment, String mStudentid) {
        this.mView = encourageTalkFragment;
        this.mStudentid = mStudentid;
    }

    @Override
    public void getData(boolean isRefresh) {
        RetrofitService.getEncourageInfo(mStudentid)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<EncourageTalkInfo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(EncourageTalkInfo encourageTalkInfo) {
                        if (encourageTalkInfo.getCode() == 0) {
                            mView.loadData(encourageTalkInfo.getEncourage());
                        }
                    }
                });
    }

    @Override
    public void getMoreData() {

    }
}
