package classign.election.kingvar.come.electionclasssign.module.main.moraleducation.four;


import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.List;

import butterknife.BindView;
import classign.election.kingvar.come.electionclasssign.R;
import classign.election.kingvar.come.electionclasssign.api.info.AttendanceInfo;
import classign.election.kingvar.come.electionclasssign.injector.components.DaggerAttendanceRecoderFourComponent;
import classign.election.kingvar.come.electionclasssign.injector.modules.AttendanceRecoderFourModule;
import classign.election.kingvar.come.electionclasssign.module.base.BaseFragment;
import classign.election.kingvar.come.electionclasssign.module.main.IAttendanceRecoderView;
import classign.election.kingvar.come.electionclasssign.adapter.RecyclerAdapter;
import classign.election.kingvar.come.electionclasssign.adapter.RecyclerViewHolder;
import classign.election.kingvar.come.electionclasssign.tools.bean.User;
import classign.election.kingvar.come.electionclasssign.tools.views.DividerItemDecoration;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/21 0021 14:09
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public class TodayAttendanceFourFragment extends BaseFragment  implements IAttendanceRecoderView {
    private static final String NEWS_TYPE_KEY = "title_four_fragment";
    private String mTitleId;

    @BindView(R.id.rv_attendance_four_list)
    RecyclerView mRvAttendanceFourList;
    private RecyclerAdapter mRecyclerAdapter;


    public static TodayAttendanceFourFragment newInstance(String titleId) {
        TodayAttendanceFourFragment fragment = new TodayAttendanceFourFragment();
        Bundle bundle = new Bundle();
        bundle.putString(NEWS_TYPE_KEY, titleId);
        fragment.setArguments(bundle);
        return fragment;
    }
    @Override
    protected int attachLayoutRes() {
        return R.layout.fragment_attendance_four;
    }

    @Override
    protected void initInjector() {
        if (getArguments() != null) {
            mTitleId = getArguments().getString(NEWS_TYPE_KEY);
        }
        DaggerAttendanceRecoderFourComponent.builder()
                .applicationComponent(getAppComponent())
                .attendanceRecoderFourModule(new AttendanceRecoderFourModule(this, User.getClassId(mContext), mTitleId))
                .build()
                .inject(this);
    }

    @Override
    protected void initViews() {
        mRvAttendanceFourList.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        mRvAttendanceFourList.addItemDecoration(new DividerItemDecoration(mContext, LinearLayoutManager.HORIZONTAL, 1, getResources().getColor(R.color.bg_gray)));
    }

    @Override
    protected void updateViews(boolean isRefresh) {
        mPresenter.getData(isRefresh);

    }


    @Override
    public void loadData(AttendanceInfo attendanceInfo) {
        final List<AttendanceInfo.DataEntity> dataEntities = attendanceInfo.getData();
        if (mRecyclerAdapter == null) {
            mRecyclerAdapter = new RecyclerAdapter(R.layout.item_two_fragment, dataEntities) {
                @Override
                protected void onBindData(RecyclerViewHolder holder, int position, Object item) {
                    holder.setText(R.id.tv_key, dataEntities.get(position).getReason());
                    holder.setText(R.id.tv_value, dataEntities.get(position).getNum() + "");
                }
            };
            mRvAttendanceFourList.setAdapter(mRecyclerAdapter);
        } else {
            mRecyclerAdapter.notifyDataSetChanged();
        }
    }
}
