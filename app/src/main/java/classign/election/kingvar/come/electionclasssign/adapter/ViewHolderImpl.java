package classign.election.kingvar.come.electionclasssign.adapter;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.SparseArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import classign.election.kingvar.come.electionclasssign.autolayout.utils.AutoUtils;


/**
 * Created by jay on 2015/11/23.
 */
public class ViewHolderImpl {

	private SparseArray<View> mCacheViews = new SparseArray<View>();

	View mItemView;

	public ViewHolderImpl(View itemView) {
		mItemView = itemView;
	}

	public View getItemView() {
		return mItemView;
	}

	public <T extends View> T findViewById(int viewId) {
		View target = mCacheViews.get(viewId);
		if (target == null) {
			target = mItemView.findViewById(viewId);
			mCacheViews.put(viewId, target);
		}
		AutoUtils.autoSize(target);

		return (T) target;
	}

	public <T extends View> T findViewById(int viewId, int pos) {
		View target = mCacheViews.get((viewId * pos + pos));
		if (target == null) {
			target = mItemView.findViewById(viewId);
			mCacheViews.put((viewId * pos + pos), target);
		}
		AutoUtils.autoSize(target);

		return (T) target;
	}

	public void setEditText(int viewId, int stringId) {
		EditText editText = findViewById(viewId);
		editText.setText(stringId);
	}

	public void setEditText(int viewId, String string, String tag) {
		EditText editText = findViewById(viewId);
		if (editText.getTag() != null && editText.getTag().equals(tag)) {
			editText.setText(string);
		}
	}

	public void setEditText(int viewId, String string) {
		EditText editText = findViewById(viewId);
		editText.setText(string);
	}

	public void setEditHint(int viewId, int stringId) {
		EditText editText = findViewById(viewId);
		editText.setHint(stringId);
	}

	public void setEditHint(int viewId, String str) {
		EditText editText = findViewById(viewId);
		editText.setHint(str);
	}

	public void setText(int viewId, int stringId) {
		TextView textView = findViewById(viewId);
		textView.setText(stringId);
	}

	public void setText(int viewId, String text) {
		TextView textView = findViewById(viewId);
		textView.setText(text);
	}

	public void setTextColor(int viewId, int color) {
		TextView textView = findViewById(viewId);
		textView.setTextColor(color);
	}

	public void setBackgroundResource(int viewId, int resId) {
		View target = findViewById(viewId);
		target.setBackgroundResource(resId);
	}

	public void setBackgroundDrawable(int viewId, Drawable drawable) {
		View target = findViewById(viewId);
		target.setBackgroundDrawable(drawable);
	}

	@TargetApi(16)
	public void setBackground(int viewId, Drawable drawable) {
		View target = findViewById(viewId);
		target.setBackgroundDrawable(drawable);
	}

	public void setImageBitmap(int viewId, Bitmap bitmap) {
		ImageView imageView = findViewById(viewId);
		imageView.setImageBitmap(bitmap);
	}

	public void setImageResource(int viewId, int resId) {
		ImageView imageView = findViewById(viewId);
		imageView.setImageResource(resId);
	}

	public void setImageDrawable(int viewId, Drawable drawable) {
		ImageView imageView = findViewById(viewId);
		imageView.setImageDrawable(drawable);
	}

	public void setImageDrawable(int viewId, Uri uri) {
		ImageView imageView = findViewById(viewId);
		imageView.setImageURI(uri);
	}

	@TargetApi(16)
	public void setImageAlpha(int viewId, float alpha) {
		ImageView imageView = findViewById(viewId);
		imageView.setAlpha(alpha);
	}

	public void setOnClickListener(int viewId,
			View.OnClickListener clickListener) {
		View view = findViewById(viewId);
		view.setOnClickListener(clickListener);
	}

	public void setOnTouchListener(int viewId,
			View.OnTouchListener touchListener) {
		View view = findViewById(viewId);
		view.setOnTouchListener(touchListener);
	}

	public void setOnLongClickListener(int viewId,
			View.OnLongClickListener longClickListener) {
		View view = findViewById(viewId);
		view.setOnLongClickListener(longClickListener);
	}

	public void setOnItemClickListener(int viewId,
			AdapterView.OnItemClickListener itemClickListener) {
		AdapterView view = findViewById(viewId);
		view.setOnItemClickListener(itemClickListener);
	}

	public void setOnItemSelectedClickListener(int viewId,
			AdapterView.OnItemSelectedListener listener) {
		AdapterView view = findViewById(viewId);
		view.setOnItemSelectedListener(listener);
	}

}
