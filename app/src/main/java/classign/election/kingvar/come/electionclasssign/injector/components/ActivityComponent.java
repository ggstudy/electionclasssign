package classign.election.kingvar.come.electionclasssign.injector.components;

import android.app.Activity;

import classign.election.kingvar.come.electionclasssign.injector.PerActivity;
import classign.election.kingvar.come.electionclasssign.injector.modules.ActivityModule;
import dagger.Component;

/**
 * Created by long on 2016/8/19.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    Activity getActivity();
}
