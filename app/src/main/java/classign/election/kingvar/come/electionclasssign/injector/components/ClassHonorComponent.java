package classign.election.kingvar.come.electionclasssign.injector.components;


import classign.election.kingvar.come.electionclasssign.injector.PerFragment;
import classign.election.kingvar.come.electionclasssign.injector.modules.ClassHonorsModule;
import classign.election.kingvar.come.electionclasssign.module.main.classmien.classhonor.ClassHonourFragment;
import dagger.Component;

/**
 * Created by long on 2016/8/19.
 */
@PerFragment
@Component(dependencies = ApplicationComponent.class, modules = {ClassHonorsModule.class})
public interface ClassHonorComponent {
  void inject(ClassHonourFragment fragment);
}