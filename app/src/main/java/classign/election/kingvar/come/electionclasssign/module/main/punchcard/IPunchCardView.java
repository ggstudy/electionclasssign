package classign.election.kingvar.come.electionclasssign.module.main.punchcard;

import java.util.List;

import classign.election.kingvar.come.electionclasssign.api.info.ProMessageContent;
import classign.election.kingvar.come.electionclasssign.module.base.IBaseView;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/7/7 0007 15:35
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */
public interface IPunchCardView extends IBaseView{
    void loadMessageData(List<ProMessageContent.DataEntity> dataEntities);

    void sendMessageSuccess();

    void postUploadDataFail();

    void postUploadDataSuccess();

    void loadTimeTableData(List<ProMessageContent.TListEntity> tList);
}
