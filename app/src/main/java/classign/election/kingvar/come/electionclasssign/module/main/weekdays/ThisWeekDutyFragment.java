package classign.election.kingvar.come.electionclasssign.module.main.weekdays;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import classign.election.kingvar.come.electionclasssign.R;
import classign.election.kingvar.come.electionclasssign.api.info.ThisWeekDutyInfo;
import classign.election.kingvar.come.electionclasssign.module.base.BaseFragment;
import classign.election.kingvar.come.electionclasssign.adapter.RecyclerAdapter;
import classign.election.kingvar.come.electionclasssign.adapter.RecyclerViewHolder;


/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/29 0029 11:08
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public class ThisWeekDutyFragment extends BaseFragment {
    private static final String WEEK_OF_DAY = "week_of_day";

    @BindView(R.id.rv_this_week_duty)
    RecyclerView mRvThisWeekDuty;
    @BindView(R.id.tv_group_leader)
    TextView mTvGroupLeader;
    private List<ThisWeekDutyInfo.Entity.StusEntity> stusEntities;
    private RecyclerAdapter mRecyclerAapter;

    @Override
    protected int attachLayoutRes() {
        return R.layout.fragment_this_week_duty;
    }

    @Override
    protected void initInjector() {
        if (getArguments() != null) {
            ThisWeekDutyInfo.Entity entity = getArguments().getParcelable(WEEK_OF_DAY);
            if (entity != null) {
                mTvGroupLeader.setText("组长 :" + entity.getLeader());
                stusEntities = entity.getStus();
            }
        }

    }

    @Override
    protected void initViews() {
        mRvThisWeekDuty.setLayoutManager(new GridLayoutManager(mContext, 3));

    }

    @Override
    protected void updateViews(boolean isRefresh) {
        if (stusEntities != null && stusEntities.size() > 0) {

            if (mRecyclerAapter == null) {

                mRecyclerAapter = new RecyclerAdapter(R.layout.week_duty_item, stusEntities) {
                    @Override
                    protected void onBindData(RecyclerViewHolder holder, int position, Object item) {
                        holder.setText(R.id.tv_duty_name, stusEntities.get(position).getSname());
                    }

                };
                mRvThisWeekDuty.setAdapter(mRecyclerAapter);
            } else {
                mRecyclerAapter.setItemDate(stusEntities);
            }
        }

    }

    public static ThisWeekDutyFragment newInstance(ThisWeekDutyInfo weekDutyInfo, int position) {
        ThisWeekDutyFragment thisWeekDutyFragment = new ThisWeekDutyFragment();
        Bundle bundle = new Bundle();
        if (position == 0 && weekDutyInfo.get_$1s() != null) {
            bundle.putParcelable(WEEK_OF_DAY, weekDutyInfo.get_$1s());
        } else if (position == 1 && weekDutyInfo.get_$2s() != null) {
            bundle.putParcelable(WEEK_OF_DAY, weekDutyInfo.get_$2s());
        } else if (position == 2 && weekDutyInfo.get_$3s() != null) {
            bundle.putParcelable(WEEK_OF_DAY, weekDutyInfo.get_$3s());
        } else if (position == 3 && weekDutyInfo.get_$4s() != null) {
            bundle.putParcelable(WEEK_OF_DAY, weekDutyInfo.get_$4s());
        } else if (position == 4 && weekDutyInfo.get_$5s() != null) {
            bundle.putParcelable(WEEK_OF_DAY, weekDutyInfo.get_$5s());
        } else if (position == 5 && weekDutyInfo.get_$6s() != null) {
            bundle.putParcelable(WEEK_OF_DAY, weekDutyInfo.get_$6s());
        } else if (position == 6 && weekDutyInfo.get_$7s() != null) {
            bundle.putParcelable(WEEK_OF_DAY, weekDutyInfo.get_$7s());
        }
        thisWeekDutyFragment.setArguments(bundle);
        return thisWeekDutyFragment;
    }
}
