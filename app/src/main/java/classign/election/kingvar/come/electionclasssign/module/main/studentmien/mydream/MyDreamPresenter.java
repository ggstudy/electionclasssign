package classign.election.kingvar.come.electionclasssign.module.main.studentmien.mydream;

import classign.election.kingvar.come.electionclasssign.api.RetrofitService;
import classign.election.kingvar.come.electionclasssign.api.info.ProMyDream;
import classign.election.kingvar.come.electionclasssign.module.base.IBasePresenter;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/4/1 0001 16:44
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public class MyDreamPresenter implements IBasePresenter {
    private final IMyDreamView mView;
    private final String mStudentid;

    public MyDreamPresenter(MyDreamFragment starFragment, String mStudentid) {
        this.mView = starFragment;
        this.mStudentid = mStudentid;
    }

    @Override
    public void getData(boolean isRefresh) {
        RetrofitService.getMyDreamInfo(mStudentid)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ProMyDream>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(ProMyDream proMyDream) {
                        if (proMyDream.getCode()==0){
                            mView.loadData(proMyDream.getDream());
                        }
                    }
                });
    }

    @Override
    public void getMoreData() {

    }

}
