package classign.election.kingvar.come.electionclasssign.tools.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {
	public static String getDate(String lastTime) {
		// TODO Auto-generated method stub
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd  HH:mm");
		Date date = new Date(Long.parseLong(lastTime));
		return format.format(date);
	}

	public static String getDateY(String lastTime) {
		// TODO Auto-generated method stub
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date(Long.parseLong(lastTime));
		return format.format(date);
	}

	public static String getDateT(String lastTime) {
		// TODO Auto-generated method stub
		SimpleDateFormat format = new SimpleDateFormat("HH:mm");
		Date date = new Date(Long.parseLong(lastTime));
		return format.format(date);
	}

	public static String getDateYl(String lastTime) {
		// TODO Auto-generated method stub
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		Date date = new Date(Long.parseLong(lastTime));
		return format.format(date);
	}

	public static String getDateM(String lastTime) {
		// TODO Auto-generated method stub
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM");
		Date date = new Date(Long.parseLong(lastTime));
		return format.format(date);
	}

	public static String getDateMh(String lastTime) {
		// TODO Auto-generated method stub
		SimpleDateFormat format = new SimpleDateFormat("MM-dd  HH:mm");
		Date date = new Date(Long.parseLong(lastTime));
		return format.format(date);
	}

	/**
	 * 时间转换为时间戳
	 *
	 * @return
	 */
	public static Long changeTime() {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String time = "2016-09-27 10:00:00";
		Date date = null;
		try {
			date = format.parse(time);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date.getTime();
	}
	/** * 获取指定日期是星期几
	 * 参数为null时表示获取当前日期是星期几
	 * @param date
	 * @return
	 */
	public static String getWeekOfDate(Date date) {
		String[] weekOfDays = {"星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"};
//		int[] weekOfDays = {0, 1, 2, 3, 4, 5, 6};
		Calendar calendar = Calendar.getInstance();
		if(date != null){
			calendar.setTime(date);
		}
		int w = calendar.get(Calendar.DAY_OF_WEEK) - 1;
		if (w < 0){
			w = 0;
		}
		return weekOfDays[w];
	}
	/** * 获取指定日期是 小时时间
	 * 参数为null时表示获取当前日期是星期几
	 * @param date
	 * @return
	 */
	public static int getHourOfDate(Date date) {
//		String[] weekOfDays = {"星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"};
		int[] weekOfDays = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,0};
		Calendar calendar = Calendar.getInstance();
		if(date != null){
			calendar.setTime(date);
		}
		int w = calendar.get(Calendar.HOUR_OF_DAY) - 1;
		if (w < 0){
			w = 0;
		}
		return weekOfDays[w];
	}
	public static int getYearByTimeStamp(long timeStamp){
		String date = timeStampToDate(timeStamp);
		String year = date.substring(0, 4);
		return Integer.parseInt(year);
	}

	public static int getMonthByTimeStamp(long timeStamp){
		String date = timeStampToDate(timeStamp);
		String month = date.substring(5, 7);
		return Integer.parseInt(month);
	}

	public static int getDayByTimeStamp(long timeStamp){
		String date = timeStampToDate(timeStamp);
		String day = date.substring(8, 10);
		return Integer.parseInt(day);
	}

	public static int getHourByTimeStamp(long timeStamp){
		String date = timeStampToDate(timeStamp);
		String hour = date.substring(11, 13);
		return Integer.parseInt(hour);
	}

	public static int getMinuteByTimeStamp(long timeStamp){
		String date = timeStampToDate(timeStamp);
		String minute = date.substring(14, 16);
		return Integer.parseInt(minute);
	}
	public static String getMinuteTime(long timeStamp){
		String date = timeStampToDate(timeStamp);
		String minute = date.substring(14, 16);
		return minute;
	}

	public static int getSecondByTimeStamp(long timeStamp){
		String date = timeStampToDate(timeStamp);
		String second = date.substring(17, 19);
		return Integer.parseInt(second);
	}
	public static String timeStampToDate(long timeStamp){
		Date             date = new Date(timeStamp);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dateStr = simpleDateFormat.format(date);
		return dateStr;
	}

}
