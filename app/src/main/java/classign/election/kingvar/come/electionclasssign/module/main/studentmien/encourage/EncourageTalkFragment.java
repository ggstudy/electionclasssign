package classign.election.kingvar.come.electionclasssign.module.main.studentmien.encourage;


import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import classign.election.kingvar.come.electionclasssign.R;
import classign.election.kingvar.come.electionclasssign.adapter.RecyclerAdapter;
import classign.election.kingvar.come.electionclasssign.adapter.RecyclerViewHolder;
import classign.election.kingvar.come.electionclasssign.api.info.EncourageTalkInfo;
import classign.election.kingvar.come.electionclasssign.base.BaseProtocol;
import classign.election.kingvar.come.electionclasssign.injector.components.DaggerEncourageTalkInfoComponent;
import classign.election.kingvar.come.electionclasssign.injector.modules.EncourageModule;
import classign.election.kingvar.come.electionclasssign.module.base.BaseFragment;
import classign.election.kingvar.come.electionclasssign.module.base.IBasePresenter;
import classign.election.kingvar.come.electionclasssign.tools.bean.User;

/**
 * 鼓励的话
 * Created by Administrator on 2016/12/20 0020.
 */

public class EncourageTalkFragment extends BaseFragment<IBasePresenter> implements IEncourageView {
    private static final String TAG = "EncourageTalkFragment";
    @BindView(R.id.rv_encourage_talk)
    RecyclerView rv_encourage_talk;

    private static final int FINSH = 1;
    private RecyclerAdapter mRecyclerAdapter;

    @Override
    protected int attachLayoutRes() {
        return R.layout.fragment_encourage_talk;
    }

    @Override
    protected void initInjector() {
        DaggerEncourageTalkInfoComponent.builder()
                .applicationComponent(getAppComponent())
                .encourageModule(new EncourageModule(this, User.getStudentId(mContext)))
                .build().inject(this);
    }

    @Override
    protected void initViews() {

    }

    @Override
    public void updateViews(boolean isRefresh) {
        mPresenter.getData(true);
    }

    @Override
    public void loadData(final List<EncourageTalkInfo.EncourageBean> encourageBeanList) {
        rv_encourage_talk.setLayoutManager(new GridLayoutManager(mContext, 2));

        if (mRecyclerAdapter == null) {
            mRecyclerAdapter = new RecyclerAdapter<EncourageTalkInfo.EncourageBean>(R.layout.encourage_talk_item, encourageBeanList) {
                @Override
                protected void onBindData(RecyclerViewHolder holder, int position, EncourageTalkInfo.EncourageBean item) {
                    holder.setText(R.id.tv_encourage_talk, item.getDetail());
                    Glide.with(mContext).load(BaseProtocol.IMG_BASE + item.getLogo())
                            .error(R.drawable.miyue).centerCrop().into((ImageView) holder.getItemView().findViewById(R.id.iv_encourage_head)
                    );
                }
            };
            rv_encourage_talk.setAdapter(mRecyclerAdapter);
        } else {
            mRecyclerAdapter.setItemDate(encourageBeanList);
        }
    }
}
