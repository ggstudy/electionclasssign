package classign.election.kingvar.come.electionclasssign.module.main.classmien.classinfo;

import java.util.ArrayList;
import java.util.List;

import classign.election.kingvar.come.electionclasssign.api.info.ClassInfo;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/16 0016 14:56
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public interface IClassInfoView {
    /**
     * 显示数据
     * @param infoEntities
     */
    void loadData(List<ClassInfo.InfoEntity> infoEntities);

    void loadClassroomDate(ArrayList<ClassInfo.ClassroomEntity> classroom);
    void loadCommitteeDate(ArrayList<ClassInfo.CommitteeEntity> committeeEntities);

}
