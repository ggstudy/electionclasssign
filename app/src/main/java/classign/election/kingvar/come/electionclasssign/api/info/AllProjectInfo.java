package classign.election.kingvar.come.electionclasssign.api.info;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/29 0029 20:54
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public class AllProjectInfo {

    /**
     * 3s : [{"start":"06:45:00","course":"英语","tname":"gasry1989","logo":"","end":"07:30:00"},{"start":"","course":"","tname":"","logo":"","end":""},{"start":"","course":"","tname":"","logo":"","end":""},{"start":"","course":"","tname":"","logo":"","end":""},{"start":"","course":"","tname":"","logo":"","end":""},{"start":"","course":"","tname":"","logo":"","end":""},{"start":"","course":"","tname":"","logo":"","end":""},{"start":"","course":"","tname":"","logo":"","end":""}]
     * 2s : [{"start":"08:30:00","course":"英语","tname":"gasry1989","logo":"","end":"09:15:00"},{"start":"","course":"","tname":"","logo":"","end":""},{"start":"","course":"","tname":"","logo":"","end":""},{"start":"","course":"","tname":"","logo":"","end":""},{"start":"","course":"","tname":"","logo":"","end":""},{"start":"","course":"","tname":"","logo":"","end":""},{"start":"","course":"","tname":"","logo":"","end":""},{"start":"","course":"","tname":"","logo":"","end":""}]
     * 1s : [{"start":"19:00:00","course":"英语","tname":"gasry1989","logo":"","end":"20:00:00"},{"start":"09:25:55","course":"语文","tname":"李老师","logo":"2017010410282092402.jpg","end":"10:10:00"},{"start":"10:30:00","course":"语文","tname":"李老师","logo":"2017010410282092402.jpg","end":"11:15:00"},{"start":"11:25:00","course":"数学","tname":"提拉米苏1","logo":"","end":"12:00:00"},{"start":"14:00:00","course":"数学","tname":"提拉米苏1","logo":"","end":"14:45:25"},{"start":"14:55:00","course":"英语","tname":"gasry1989","logo":"","end":"15:40:00"},{"start":"15:50:00","course":"英语","tname":"gasry1989","logo":"","end":"16:35:00"},{"start":"","course":"","tname":"","logo":"","end":""}]
     * code : 0
     * night : [{"week":""},{"week":""},{"week":"英语"},{"week":""},{"week":""},{"week":""},{"week":""}]
     * 7s : [{"start":"06:45:00","course":"英语","tname":"gasry1989","logo":"","end":"07:30:00"},{"start":"","course":"","tname":"","logo":"","end":""},{"start":"","course":"","tname":"","logo":"","end":""},{"start":"","course":"","tname":"","logo":"","end":""},{"start":"","course":"","tname":"","logo":"","end":""},{"start":"","course":"","tname":"","logo":"","end":""},{"start":"","course":"","tname":"","logo":"","end":""},{"start":"","course":"","tname":"","logo":"","end":""}]
     * 6s : [{"start":"08:30:00","course":"语文","tname":"李老师","logo":"2017010410282092402.jpg","end":"09:15:00"},{"start":"","course":"","tname":"","logo":"","end":""},{"start":"","course":"","tname":"","logo":"","end":""},{"start":"","course":"","tname":"","logo":"","end":""},{"start":"","course":"","tname":"","logo":"","end":""},{"start":"","course":"","tname":"","logo":"","end":""},{"start":"","course":"","tname":"","logo":"","end":""},{"start":"","course":"","tname":"","logo":"","end":""}]
     * 5s : [{"start":"06:45:00","course":"英语","tname":"gasry1989","logo":"","end":"07:30:00"},{"start":"","course":"","tname":"","logo":"","end":""},{"start":"","course":"","tname":"","logo":"","end":""},{"start":"","course":"","tname":"","logo":"","end":""},{"start":"","course":"","tname":"","logo":"","end":""},{"start":"","course":"","tname":"","logo":"","end":""},{"start":"","course":"","tname":"","logo":"","end":""},{"start":"","course":"","tname":"","logo":"","end":""}]
     * 4s : [{"start":"06:45:00","course":"英语","tname":"gasry1989","logo":"","end":"07:30:00"},{"start":"","course":"","tname":"","logo":"","end":""},{"start":"","course":"","tname":"","logo":"","end":""},{"start":"","course":"","tname":"","logo":"","end":""},{"start":"","course":"","tname":"","logo":"","end":""},{"start":"","course":"","tname":"","logo":"","end":""},{"start":"","course":"","tname":"","logo":"","end":""},{"start":"","course":"","tname":"","logo":"","end":""}]
     * morning : [{"week":"英语"},{"week":""},{"week":"英语"},{"week":"英语"},{"week":"英语"},{"week":"英语"},{"week":""}]
     */

    private int code;
    @SerializedName("3s")
    private List<Entity> _$3s;
    @SerializedName("2s")
    private List<Entity> _$2s;
    @SerializedName("1s")
    private List<Entity> _$1s;
    @SerializedName("7s")
    private List<Entity> _$7s;
    @SerializedName("6s")
    private List<Entity> _$6s;
    @SerializedName("5s")
    private List<Entity> _$5s;
    @SerializedName("4s")
    private List<Entity> _$4s;
    private List<NightEntity> night;
    private List<NightEntity> morning;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<Entity> get_$3s() {
        return _$3s;
    }

    public void set_$3s(List<Entity> _$3s) {
        this._$3s = _$3s;
    }

    public List<Entity> get_$2s() {
        return _$2s;
    }

    public void set_$2s(List<Entity> _$2s) {
        this._$2s = _$2s;
    }

    public List<Entity> get_$1s() {
        return _$1s;
    }

    public void set_$1s(List<Entity> _$1s) {
        this._$1s = _$1s;
    }


    public List<Entity> get_$7s() {
        return _$7s;
    }

    public void set_$7s(List<Entity> _$7s) {
        this._$7s = _$7s;
    }

    public List<Entity> get_$6s() {
        return _$6s;
    }

    public void set_$6s(List<Entity> _$6s) {
        this._$6s = _$6s;
    }

    public List<Entity> get_$5s() {
        return _$5s;
    }

    public void set_$5s(List<Entity> _$5s) {
        this._$5s = _$5s;
    }

    public List<Entity> get_$4s() {
        return _$4s;
    }

    public void set_$4s(List<Entity> _$4s) {
        this._$4s = _$4s;
    }

    public List<NightEntity> getMorning() {
        return morning;
    }

    public void setMorning(List<NightEntity> morning) {
        this.morning = morning;
    }

    public List<NightEntity> getNight() {
        return night;
    }

    public void setNight(List<NightEntity> night) {
        this.night = night;
    }

    public static class Entity {
        /**
         * start : 06:45:00
         * course : 英语
         * tname : gasry1989
         * logo :
         * num :1
         * end : 07:30:00
         */

        private String start;
        private String course;
        private String tname;

        private String num;
        private String logo;
        private String end;

        public String getStart() {
            return start;
        }

        public void setStart(String start) {
            this.start = start;
        }

        public String getCourse() {
            return course;
        }

        public void setCourse(String course) {
            this.course = course;
        }

        public String getTname() {
            return tname;
        }

        public void setTname(String tname) {
            this.tname = tname;
        }

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

        public String getNum() {
            return num;
        }

        public void setNum(String num) {
            this.num = num;
        }


        public String getEnd() {
            return end;
        }

        public void setEnd(String end) {
            this.end = end;
        }
    }

    public static class NightEntity {
        /**
         * week :
         */

        private String week;

        public String getWeek() {
            return week;
        }

        public void setWeek(String week) {
            this.week = week;
        }
    }

    public static class MorningEntity {
        /**
         * week : 英语
         */

        private String week;

        public String getWeek() {
            return week;
        }

        public void setWeek(String week) {
            this.week = week;
        }
    }
}
