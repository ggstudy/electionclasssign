package classign.election.kingvar.come.electionclasssign.base;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.trello.rxlifecycle.components.support.RxFragmentActivity;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import butterknife.ButterKnife;
import classign.election.kingvar.come.electionclasssign.activity.MyApplication;
import classign.election.kingvar.come.electionclasssign.broadcastreceiver.CommentEvent;
import classign.election.kingvar.come.electionclasssign.injector.components.ApplicationComponent;
import classign.election.kingvar.come.electionclasssign.module.base.IBasePresenter;
import classign.election.kingvar.come.electionclasssign.tools.utils.ActivityStackUtil;
import classign.election.kingvar.come.electionclasssign.tools.utils.NetUtil;


/**
 * Created by xinxin on 2016/1/10.
 */
public abstract class BaseActivity<T extends IBasePresenter> extends RxFragmentActivity implements
		View.OnClickListener {
	/**
	 * 网络类型
	 */
	private int netMobile;
	@Inject
	protected T mNewPresenter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		onRoot(savedInstanceState);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		ActivityStackUtil.add(this);
		ButterKnife.bind(this);
		inspectNet();
		initInjector();

		initView();
		initData();
		initListener();
	}

	/**
	 * 初始化时判断有没有网络
	 */

	public void inspectNet() {

		this.netMobile = NetUtil.getNetWorkState(BaseActivity.this);
		EventBus.getDefault().post(new CommentEvent(CommentEvent.NETWORkSTATE,netMobile));
//		return isNetConnect();
	}
/*
	*//**
	 * 判断有无网络 。
	 *
	 * @return true 有网, false 没有网络.
	 *//*
	public boolean isNetConnect() {
		if (netMobile == NetUtil.NETWORK_WIFI) {
			ToastUtils.showToast("当前网络良好");
			return true;
		} else if (netMobile == NetUtil.NETWORK_MOBILE) {
			ToastUtils.showToast("当前网络良好");
			return true;
		} else if (netMobile == NetUtil.NETWORK_NONE) {
			ToastUtils.showToast("当前无网络");
			return false;

		}
		return false;
	}*/



	@Override
	public void onClick(View v) {
		WidgetClick(v);
	}

	/**
	 * 获取 ApplicationComponent
	 *
	 * @return ApplicationComponent
	 */
	protected ApplicationComponent getAppComponent() {
		return MyApplication.getAppComponent();
	}
	@Override
	protected void onDestroy() {
		super.onDestroy();
		ActivityStackUtil.remove(this);
	}

	// 设置内容
	public abstract void onRoot(Bundle savedInstanceState);

	/**
	 * Dagger 注入
	 */
	protected abstract void initInjector();
	// 初始化控件
	protected abstract void initView();

	// 初始化数据
	public abstract void initData();


	// 初始化控件点击事件
	public abstract void initListener();

	// 控件点击事件
	public abstract void WidgetClick(View view);

}