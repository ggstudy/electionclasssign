package classign.election.kingvar.come.electionclasssign.module.main.studentmien;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import classign.election.kingvar.come.electionclasssign.R;
import classign.election.kingvar.come.electionclasssign.base.BaseActivity;
import classign.election.kingvar.come.electionclasssign.injector.components.DaggerStudentMienComponent;
import classign.election.kingvar.come.electionclasssign.injector.modules.StudentMienModule;
import classign.election.kingvar.come.electionclasssign.module.main.weekdays.IMainActivityPresenter;

import static classign.election.kingvar.come.electionclasssign.R.id.iv_close_btn;

/**
 * 学生风采 大界面
 * Created by Administrator on 2016/12/29 0029.
 */
public class StudentMienActivity extends BaseActivity<IMainActivityPresenter> implements StudentMienInterfaceView, RadioGroup.OnCheckedChangeListener {
    private FragmentManager manager;
    private RadioGroup rg_student_mien_content; //student_mien 的Radiogroup
    private ArrayList<Integer> radio_btn_arrayList = new ArrayList();
    private TextView tv_title;  //学生风采标题
    private RadioButton radio_btn_one;
    private RadioButton radio_btn_two;
    private RadioButton radio_btn_three;
    private RadioButton radio_btn_four;
    private RadioButton radio_btn_five;
    @BindView(iv_close_btn)
    ImageView mCloseBtn;

    @Override
    public void onRoot(Bundle savedInstanceState) {
        setContentView(R.layout.activity_student_mien_layout);
    }

    @Override
    protected void initInjector() {
        DaggerStudentMienComponent.builder().applicationComponent(getAppComponent())
                .studentMienModule(new StudentMienModule(this)).build().inject(this);
    }

    @Override
    protected void initView() {
        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_title.setTextSize(30);
        rg_student_mien_content = (RadioGroup) findViewById(R.id.rg_student_mien_content);
        radio_btn_one = (RadioButton) findViewById(R.id.radio_btn_one);
        radio_btn_two = (RadioButton) findViewById(R.id.radio_btn_two);
        radio_btn_three = (RadioButton) findViewById(R.id.radio_btn_three);
        radio_btn_four = (RadioButton) findViewById(R.id.radio_btn_four);
        radio_btn_five = (RadioButton) findViewById(R.id.radio_btn_five);
    }

    @Override
    public void initData() {
        setDrawableSize();
        mCloseBtn.setVisibility(View.VISIBLE);
        manager = this.getSupportFragmentManager();
        radio_btn_arrayList.add(R.id.radio_btn_one);
        radio_btn_arrayList.add(R.id.radio_btn_two);
        radio_btn_arrayList.add(R.id.radio_btn_three);
        radio_btn_arrayList.add(R.id.radio_btn_four);
        radio_btn_arrayList.add(R.id.radio_btn_five);
        mNewPresenter.showStudentMien(this, R.id.radio_btn_one, manager);

    }

    /**
     * 设置 图片的大小和位置
     */
    private void setDrawableSize() {
        Drawable drawable_radio_btn_one = getResources().getDrawable(R.drawable.radio_btn_one);
        Drawable drawable_radio_btn_two = getResources().getDrawable(R.drawable.radio_btn_two);
        Drawable drawable_radio_btn_three = getResources().getDrawable(R.drawable.radio_btn_three);
        Drawable drawable_radio_btn_four = getResources().getDrawable(R.drawable.radio_btn_four);
        Drawable drawable_radio_btn_five = getResources().getDrawable(R.drawable.radio_btn_five);
        drawable_radio_btn_one.setBounds(0, 0, 100, 100);
        drawable_radio_btn_two.setBounds(0, 0, 100, 100);
        drawable_radio_btn_three.setBounds(0, 0, 100, 100);
        drawable_radio_btn_four.setBounds(0, 0, 100, 100);
        drawable_radio_btn_five.setBounds(0, 0, 100, 100);
        radio_btn_one.setCompoundDrawables(drawable_radio_btn_one, null, null, null);
        radio_btn_two.setCompoundDrawables(drawable_radio_btn_two, null, null, null);
        radio_btn_three.setCompoundDrawables(drawable_radio_btn_three, null, null, null);
        radio_btn_four.setCompoundDrawables(drawable_radio_btn_four, null, null, null);
        radio_btn_five.setCompoundDrawables(drawable_radio_btn_five, null, null, null);
    }

    @Override
    public void initListener() {
        rg_student_mien_content.setOnCheckedChangeListener(this);
        mCloseBtn.setOnClickListener(this);

    }

    @Override
    public void WidgetClick(View view) {
        switch (view.getId()) {
            case iv_close_btn:
                finish();
                break;

            default:
                break;
        }
    }

    @Override
    public void setStudentMien(int checkId) {
        if (checkId == radio_btn_arrayList.get(0)) {
            tv_title.setText(getString(R.string.today_star));
        } else if (checkId == radio_btn_arrayList.get(1)) {
            tv_title.setText(getString(R.string.class_reward));
        } else if (checkId == radio_btn_arrayList.get(2)) {
            tv_title.setText(getString(R.string.school_reward));
        } else if (checkId == radio_btn_arrayList.get(3)) {
            tv_title.setText(getString(R.string.my_dream));
        } else if (checkId == radio_btn_arrayList.get(4)) {
            tv_title.setText(getString(R.string.encourage_talk));
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        mNewPresenter.showStudentMien(this, checkedId, manager);

    }
}
