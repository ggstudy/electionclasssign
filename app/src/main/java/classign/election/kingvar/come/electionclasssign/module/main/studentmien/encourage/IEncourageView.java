package classign.election.kingvar.come.electionclasssign.module.main.studentmien.encourage;

import java.util.List;

import classign.election.kingvar.come.electionclasssign.api.info.EncourageTalkInfo;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/16 0016 14:56
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public interface IEncourageView {
    /**
     * 显示数据
     * @param encourageBeanList     选中栏目
     */
    void loadData(List<EncourageTalkInfo.EncourageBean> encourageBeanList);

}
