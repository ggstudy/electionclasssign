package classign.election.kingvar.come.electionclasssign.injector.modules;

import classign.election.kingvar.come.electionclasssign.activity.MainActivity;
import classign.election.kingvar.come.electionclasssign.injector.PerActivity;
import classign.election.kingvar.come.electionclasssign.module.main.MainActivityPersenter;
import classign.election.kingvar.come.electionclasssign.module.main.weekdays.IMainActivityPresenter;
import classign.election.kingvar.come.electionclasssign.tools.bean.User;
import dagger.Module;
import dagger.Provides;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/16 0016 15:28
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */
@Module
public class MainActivityModule {
    private final MainActivity mMainActivity;

    public MainActivityModule(MainActivity MainActivity) {
        this.mMainActivity = MainActivity;
    }

    @PerActivity
    @Provides
    public IMainActivityPresenter provideTodayProjectPresenter() {
        return new MainActivityPersenter(mMainActivity, User.getClassId(mMainActivity),User.getImei(mMainActivity));
    }


}
