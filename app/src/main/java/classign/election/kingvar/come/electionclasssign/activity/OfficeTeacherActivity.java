package classign.election.kingvar.come.electionclasssign.activity;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import classign.election.kingvar.come.electionclasssign.R;
import classign.election.kingvar.come.electionclasssign.api.info.ClassInfo;
import classign.election.kingvar.come.electionclasssign.module.base.BaseActivity;
import classign.election.kingvar.come.electionclasssign.adapter.RecyclerAdapter;
import classign.election.kingvar.come.electionclasssign.adapter.RecyclerViewHolder;
import classign.election.kingvar.come.electionclasssign.tools.views.DividerItemDecoration;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/29 0029 14:10
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public class OfficeTeacherActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.btn_close)
    Button mBtnClose;
    @BindView(R.id.tv_close)
    TextView mTvClose;
    @BindView(R.id.rv_take_office_teacher)
    RecyclerView mRvTakeOfficeTeacher;
    @BindView(R.id.rv_class_committee)
    RecyclerView mRvClassCommittee;

    private ArrayList<ClassInfo.ClassroomEntity> classroom;
    private ArrayList<ClassInfo.CommitteeEntity> committeeEntities;
    private RecyclerAdapter mRecycleClassroomAdapter;
    private RecyclerAdapter mCommitteeAdapter;

    @Override
    protected int attachLayoutRes() {
        return R.layout.activity_office_teacher;
    }

    @Override
    protected void initInjector() {

        classroom = getIntent().getParcelableArrayListExtra("classroom");
        committeeEntities = getIntent().getParcelableArrayListExtra("committeeEntities");
        mRvTakeOfficeTeacher.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mRvTakeOfficeTeacher.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.HORIZONTAL, 2, getResources().getColor(R.color.bg_gray)));
        mRvClassCommittee.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mRvClassCommittee.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.HORIZONTAL, 2, getResources().getColor(R.color.bg_gray)));
    }

    @Override
    protected void initViews() {

        mBtnClose.setOnClickListener(this);
        mTvClose.setOnClickListener(this);
    }

    @Override
    protected void updateViews(boolean isRefresh) {
        if (classroom != null && classroom.size() > 0) {

            if (mRecycleClassroomAdapter == null) {
                mRecycleClassroomAdapter = new RecyclerAdapter(R.layout.item_office_teacher, classroom) {
                    @Override
                    protected void onBindData(RecyclerViewHolder holder, int position, Object item) {
                        holder.setText(R.id.tv_key, classroom.get(position).getName());
                        holder.setText(R.id.tv_value, classroom.get(position).getTitle());

                    }
                };
                mRvTakeOfficeTeacher.setAdapter(mRecycleClassroomAdapter);
            } else {
                mRecycleClassroomAdapter.setItemDate(classroom);
            }
        }
        if (committeeEntities != null && committeeEntities.size() > 0) {
            if (mCommitteeAdapter == null) {
                mCommitteeAdapter = new RecyclerAdapter(R.layout.item_office_teacher, committeeEntities) {
                    @Override
                    protected void onBindData(RecyclerViewHolder holder, int position, Object item) {
                        holder.setText(R.id.tv_key, committeeEntities.get(position).getName());
                        holder.setText(R.id.tv_value, committeeEntities.get(position).getTitle());

                    }
                };
                mRvClassCommittee.setAdapter(mCommitteeAdapter);
            } else {
                mCommitteeAdapter.setItemDate(committeeEntities);
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_close:
            case R.id.btn_close:
                finish();
                break;
        }
    }
}
