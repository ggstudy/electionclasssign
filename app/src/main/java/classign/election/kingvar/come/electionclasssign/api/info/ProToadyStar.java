package classign.election.kingvar.come.electionclasssign.api.info;

/**
 * Created by Administrator on 2017/1/4 0004.
 */
public class ProToadyStar {

	/**
	 * code : 0
	 * info : {"studentid":"24","name":"杜平","logo":"20170105100016669757.png","memo":"夫学须志也，才须学也，非学无以广才，非志无以成学。&mdash;&mdash;诸葛亮","id":1}
	 */

	private int code;
	private InfoBean info;

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public InfoBean getInfo() {
		return info;
	}

	public void setInfo(InfoBean info) {
		this.info = info;
	}

	public static class InfoBean {
		/**
		 * studentid : 24
		 * name : 杜平
		 * logo : 20170105100016669757.png
		 * memo : 夫学须志也，才须学也，非学无以广才，非志无以成学。&mdash;&mdash;诸葛亮
		 * id : 1
		 */

		private String studentid;
		private String name;
		private String logo;
		private String memo;
		private int id;

		public String getStudentid() {
			return studentid;
		}

		public void setStudentid(String studentid) {
			this.studentid = studentid;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getLogo() {
			return logo;
		}

		public void setLogo(String logo) {
			this.logo = logo;
		}

		public String getMemo() {
			return memo;
		}

		public void setMemo(String memo) {
			this.memo = memo;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}
	}
}
