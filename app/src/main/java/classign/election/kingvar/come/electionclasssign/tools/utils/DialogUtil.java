package classign.election.kingvar.come.electionclasssign.tools.utils;

import android.app.Dialog;
import android.content.Context;

import classign.election.kingvar.come.electionclasssign.R;
import classign.election.kingvar.come.electionclasssign.tools.views.WaitingDialog;

/**
 * Created by Administrator on 2016/12/9 0009.
 */

public class DialogUtil {
	public static Dialog creatLoadingProgressDialog(Context context) {
		WaitingDialog progressDialog = new WaitingDialog(context, R.style.dialog);
		progressDialog.setCancelable(true);
		progressDialog.setCanceledOnTouchOutside(false);
		return progressDialog;
	}
}
