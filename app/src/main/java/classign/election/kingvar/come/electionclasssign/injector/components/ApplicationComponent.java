package classign.election.kingvar.come.electionclasssign.injector.components;

import android.content.Context;

import javax.inject.Singleton;

import classign.election.kingvar.come.electionclasssign.greendao.DaoSession;
import classign.election.kingvar.come.electionclasssign.injector.modules.ApplicationModule;
import classign.election.kingvar.come.electionclasssign.rxbus.RxBus;
import dagger.Component;

/**
 * Created by long on 2016/8/19.
 * Application Component
 */
@Singleton
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {

//    void inject(BaseActivity baseActivity);
    // provide
    Context getContext();
    RxBus getRxBus();
    DaoSession getDaoSession();
}
