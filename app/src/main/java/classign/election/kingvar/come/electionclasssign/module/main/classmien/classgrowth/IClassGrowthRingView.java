package classign.election.kingvar.come.electionclasssign.module.main.classmien.classgrowth;

import java.util.List;

import classign.election.kingvar.come.electionclasssign.api.info.ClassGrowthRingInfo;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/16 0016 14:56
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public interface IClassGrowthRingView {
    /**
     * 显示数据
     * @param clsringEntityList     选中栏目
     */
    void loadClassGrowthRingData(List<ClassGrowthRingInfo.ClsringEntity> clsringEntityList);

}
