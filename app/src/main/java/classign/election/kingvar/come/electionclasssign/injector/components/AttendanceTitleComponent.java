package classign.election.kingvar.come.electionclasssign.injector.components;


import classign.election.kingvar.come.electionclasssign.activity.ToadyAttendanceActivity;
import classign.election.kingvar.come.electionclasssign.injector.PerActivity;
import classign.election.kingvar.come.electionclasssign.injector.modules.AttendanceTitleModule;
import dagger.Component;

/**
 * Created by long on 2016/8/19.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {AttendanceTitleModule.class})
public interface AttendanceTitleComponent {
  void inject(ToadyAttendanceActivity activity);
}