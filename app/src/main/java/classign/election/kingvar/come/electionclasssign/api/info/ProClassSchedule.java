package classign.election.kingvar.come.electionclasssign.api.info;

import java.util.List;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/1/13 0013 17:06
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public class ProClassSchedule {

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<ScheduleEntity> getSchedule() {
        return schedule;
    }

    public void setSchedule(List<ScheduleEntity> schedule) {
        this.schedule = schedule;
    }

    /**
     * schedule : [{"date":"2017-02-01","clsid":"7","name":"九年级八班","id":1,"time":1483414206000,"title":"元旦","detal":"元旦放假三天"}]
     * code : 0
     */

    private int code;
    private List<ScheduleEntity> schedule;

    public static class ScheduleEntity {
        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getClsid() {
            return clsid;
        }

        public void setClsid(String clsid) {
            this.clsid = clsid;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public long getTime() {
            return time;
        }

        public void setTime(long time) {
            this.time = time;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDetal() {
            return detal;
        }

        public void setDetal(String detal) {
            this.detal = detal;
        }

        /**
         * date : 2017-02-01
         * clsid : 7
         * name : 九年级八班
         * id : 1
         * time : 1483414206000
         * title : 元旦
         * detal : 元旦放假三天
         */

        private String date;
        private String clsid;
        private String name;
        private int id;
        private long time;
        private String title;
        private String detal;
    }
}
