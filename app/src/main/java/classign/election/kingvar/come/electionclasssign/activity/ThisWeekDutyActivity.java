package classign.election.kingvar.come.electionclasssign.activity;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import classign.election.kingvar.come.electionclasssign.R;
import classign.election.kingvar.come.electionclasssign.adapter.DutyViewPagerAdapter;
import classign.election.kingvar.come.electionclasssign.api.info.ThisWeekDutyInfo;
import classign.election.kingvar.come.electionclasssign.injector.components.DaggerThisWeekDutyComponent;
import classign.election.kingvar.come.electionclasssign.module.base.BaseActivity;
import classign.election.kingvar.come.electionclasssign.module.main.weekdays.IThisWeekDutyView;
import classign.election.kingvar.come.electionclasssign.injector.modules.ThisWeekDutyModule;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/28 0028 20:22
 * @des ${本周值日表}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public class ThisWeekDutyActivity extends BaseActivity implements View.OnClickListener ,IThisWeekDutyView{
    @BindView(R.id.iv_close_btn)
    ImageView mIvCloseBtn;
    @BindView(R.id.view_pager)
    ViewPager mViewPager;
    @BindView(R.id.tab_layout)
    TabLayout mTabLayout;
    @Inject
    DutyViewPagerAdapter pagerAdapter;

    @Override
    protected int attachLayoutRes() {
        return R.layout.activity_this_week_duty;
    }

    @Override
    protected void initInjector() {
        ArrayList   mTitles = new ArrayList<String>();
        mTitles.add("周一");
        mTitles.add("周二");
        mTitles.add("周三");
        mTitles.add("周四");
        mTitles.add("周五");
        mTitles.add("周六");
        mTitles.add("周日");
        DaggerThisWeekDutyComponent.builder()
                .applicationComponent(getAppComponent())
                .thisWeekDutyModule(new ThisWeekDutyModule(this,mTitles))
                .build()
                .inject(this);


    }

    @Override
    protected void initViews() {
        mIvCloseBtn.setOnClickListener(this);
    }

    @Override
    protected void updateViews(boolean isRefresh) {

    }

    @Override
    public void onClick(View v) {
        finish();
    }

    @Override
    public void LoadWeekDutyDate(ThisWeekDutyInfo weekDutyInfo) {
        pagerAdapter.setItems(weekDutyInfo);
        mViewPager.setAdapter(pagerAdapter);
        mTabLayout.setupWithViewPager(mViewPager);
    }
}
