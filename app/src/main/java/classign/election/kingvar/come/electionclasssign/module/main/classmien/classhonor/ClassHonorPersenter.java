package classign.election.kingvar.come.electionclasssign.module.main.classmien.classhonor;

import android.util.Log;

import classign.election.kingvar.come.electionclasssign.api.RetrofitService;
import classign.election.kingvar.come.electionclasssign.api.info.ClassHonorInfo;
import classign.election.kingvar.come.electionclasssign.module.base.IBasePresenter;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/16 0016 15:05
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public class ClassHonorPersenter implements IBasePresenter {
    private static final String TAG = "ClassHonorPersenter";
    private final IClassHonorView mView;
    private final String mClsid;

    public ClassHonorPersenter(ClassHonourFragment classHonourFragment, String clsid) {
        mView = classHonourFragment;
        mClsid = clsid;
        getData(true);
    }


    @Override
    public void getData(boolean isRefresh) {
        Log.i(TAG, "getData: mClsid =  " + mClsid);
        RetrofitService.getClassHonorInfo(mClsid)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ClassHonorInfo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(ClassHonorInfo classHonorInfo) {
                        if (classHonorInfo.getCode() == 0) {
                            mView.loadClassHonorData(classHonorInfo);
                        }
                    }
                });
    }

    @Override
    public void getMoreData() {

    }
}
