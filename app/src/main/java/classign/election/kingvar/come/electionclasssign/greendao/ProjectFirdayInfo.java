package classign.election.kingvar.come.electionclasssign.greendao;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Unique;
import org.greenrobot.greendao.annotation.Generated;

import classign.election.kingvar.come.electionclasssign.api.info.AllProjectInfo;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/4/11 0011 17:42
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

@Entity
public class ProjectFirdayInfo extends AllProjectInfo.Entity{

    @Id(autoincrement = true)
    private Long id;
    private String tag;
    @Unique
    private String num;

    private String course;
    private String tname;
    private String logo;
    private String start;
    private String end;
    @Generated(hash = 782876249)
    public ProjectFirdayInfo(Long id, String tag, String num, String course,
            String tname, String logo, String start, String end) {
        this.id = id;
        this.tag = tag;
        this.num = num;
        this.course = course;
        this.tname = tname;
        this.logo = logo;
        this.start = start;
        this.end = end;
    }
    @Generated(hash = 1411951720)
    public ProjectFirdayInfo() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getTag() {
        return this.tag;
    }
    public void setTag(String tag) {
        this.tag = tag;
    }
    public String getNum() {
        return this.num;
    }
    public void setNum(String num) {
        this.num = num;
    }
    public String getCourse() {
        return this.course;
    }
    public void setCourse(String course) {
        this.course = course;
    }
    public String getTname() {
        return this.tname;
    }
    public void setTname(String tname) {
        this.tname = tname;
    }
    public String getLogo() {
        return this.logo;
    }
    public void setLogo(String logo) {
        this.logo = logo;
    }
    public String getStart() {
        return this.start;
    }
    public void setStart(String start) {
        this.start = start;
    }
    public String getEnd() {
        return this.end;
    }
    public void setEnd(String end) {
        this.end = end;
    }

    @Override
    public String toString() {
        return "ProjectFirdayInfo{" +
                "id=" + id +
                ", tag='" + tag + '\'' +
                ", num='" + num + '\'' +
                ", course='" + course + '\'' +
                ", tname='" + tname + '\'' +
                ", logo='" + logo + '\'' +
                ", start='" + start + '\'' +
                ", end='" + end + '\'' +
                '}';
    }
}
