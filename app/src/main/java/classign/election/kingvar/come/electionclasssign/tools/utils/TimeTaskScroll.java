package classign.election.kingvar.come.electionclasssign.tools.utils;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.ListView;

import java.util.List;
import java.util.TimerTask;

import classign.election.kingvar.come.electionclasssign.adapter.ListAdapter;


/**
 * Created by Administrator on 2016/12/22 0022.
 */
public class TimeTaskScroll extends TimerTask {
    private static final String TAG = "TimeTaskScroll";
    ListView lv_scroll;
    List list;
    List timeList;
    int type;
    ListAdapter mListAdapter;

    public TimeTaskScroll(Context context, ListView lv_scroll, List list, List timeList, int type) {
        this.lv_scroll = lv_scroll;
        this.list = list;
        this.timeList = timeList;
        this.type = type;

        if (mListAdapter == null) {
            mListAdapter = new ListAdapter(context, list, timeList, type);
            lv_scroll.setAdapter(mListAdapter);
        } else {
            mListAdapter.setDateList(list, timeList, type);
        }
        Log.i(TAG, "TimeTaskScroll: list = " + list.size());

    }

    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            lv_scroll.smoothScrollBy(49, 0);
        }
    };

    @Override
    public void run() {
        Message msg = handler.obtainMessage();
        handler.sendMessage(msg);
    }
}
