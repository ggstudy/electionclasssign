package classign.election.kingvar.come.electionclasssign.activity;

import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;

import butterknife.BindView;
import classign.election.kingvar.come.electionclasssign.R;
import classign.election.kingvar.come.electionclasssign.adapter.Adapter;
import classign.election.kingvar.come.electionclasssign.module.base.BaseActivity;
import classign.election.kingvar.come.electionclasssign.tools.views.ZoomOutPageAlbumTransformer;

/**
 * 班级相册
 * Created by Administrator on 2016/12/23 0023.
 */
public class ClassAlbumActivity extends BaseActivity {
    @BindView(R.id.id_viewpager)
    ViewPager mViewPager;
    @BindView(R.id.iv_close_btn)
    ImageView mIvCloseBtn;


    @Override
    protected int attachLayoutRes() {
        return R.layout.activity_class_album;
    }

    @Override
    protected void initInjector() {

    }

    @Override
    protected void initViews() {
        mViewPager.setOffscreenPageLimit(3);
        mViewPager.setPageTransformer(true, new ZoomOutPageAlbumTransformer());
        mViewPager.setAdapter(new Adapter(this));
    }

    @Override
    protected void updateViews(boolean isRefresh) {
        mIvCloseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
