package classign.election.kingvar.come.electionclasssign.injector.components;


import classign.election.kingvar.come.electionclasssign.injector.PerActivity;
import classign.election.kingvar.come.electionclasssign.injector.modules.PunchCardModule;
import classign.election.kingvar.come.electionclasssign.module.main.punchcard.PunchCardActivity;
import dagger.Component;

/**
 * Created by long on 2016/8/19.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {PunchCardModule.class})
public interface PunchCardComponent {
  void inject(PunchCardActivity punchCardActivity);
}