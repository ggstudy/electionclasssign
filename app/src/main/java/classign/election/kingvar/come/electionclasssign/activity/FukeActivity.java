package classign.election.kingvar.come.electionclasssign.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.Window;
import android.view.WindowManager;

import classign.election.kingvar.come.electionclasssign.base.BaseActivity;
import classign.election.kingvar.come.electionclasssign.interfaces.OnCardSlotListener;

/**
 * Created by Administrator on 2016/12/26 0026.
 */

public abstract class FukeActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);//隐藏标题
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);//设置全屏

	}

	private static String content = "";
	private OnCardSlotListener listner;

	public void setOnCardSlotListener(OnCardSlotListener listener) {
		this.listner = listener;
	}

	@Override
	public boolean dispatchKeyEvent(KeyEvent event) {
		if (event.getAction() == KeyEvent.ACTION_UP)
			return true;
		if (KeyEvent.KEYCODE_ENTER == event.getKeyCode()) {

			if (listner != null)
				listner.onCardSlot(content);

			content = "";
			return true;
		}
		String str = String.valueOf((char) event.getUnicodeChar());
		if (!TextUtils.isEmpty(str))
			content += str;
		return true;
	}
}

