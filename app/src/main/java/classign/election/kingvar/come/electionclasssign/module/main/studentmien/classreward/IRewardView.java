package classign.election.kingvar.come.electionclasssign.module.main.studentmien.classreward;

import java.util.List;

import classign.election.kingvar.come.electionclasssign.api.info.RewardInfo;
import classign.election.kingvar.come.electionclasssign.module.base.IBaseView;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/16 0016 14:56
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public interface IRewardView extends IBaseView{
    /**
     * 显示数据
     * @param attendanceInfo     选中栏目
     */
    void loadData(List<RewardInfo.WinningEntity> winning );

}
