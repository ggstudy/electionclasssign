package classign.election.kingvar.come.electionclasssign.injector.components;


import classign.election.kingvar.come.electionclasssign.injector.PerFragment;
import classign.election.kingvar.come.electionclasssign.module.main.studentmien.todaystar.TodayStarFragment;
import classign.election.kingvar.come.electionclasssign.injector.modules.TodayStarModule;
import dagger.Component;

/**
 * Created by long on 2016/8/19.
 */
@PerFragment
@Component(dependencies = ApplicationComponent.class, modules = {TodayStarModule.class})
public interface TodayStarComponent {
  void inject(TodayStarFragment fragment);
}