package classign.election.kingvar.come.electionclasssign.module.main.weekdays;

import classign.election.kingvar.come.electionclasssign.api.info.ThisWeekDutyInfo;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/30 0030 10:30
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public interface IThisWeekDutyView {
    /**
     * 回调本周值日信息
     * @param weekDutyInfo
     */
    void  LoadWeekDutyDate(ThisWeekDutyInfo weekDutyInfo);
}
