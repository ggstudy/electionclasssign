package classign.election.kingvar.come.electionclasssign.module.main.classmien;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioGroup;

import java.util.ArrayList;

import butterknife.BindView;
import classign.election.kingvar.come.electionclasssign.R;
import classign.election.kingvar.come.electionclasssign.base.BaseActivity;
import classign.election.kingvar.come.electionclasssign.injector.components.DaggerClassMienComponent;
import classign.election.kingvar.come.electionclasssign.injector.modules.ClassMienActivityModule;
import classign.election.kingvar.come.electionclasssign.module.main.weekdays.IMainActivityPresenter;

/**
 * Created by Administrator on 2017/1/5 0005.
 */
public class ClassMienActivity extends BaseActivity<IMainActivityPresenter> implements RadioGroup.OnCheckedChangeListener {
    private FragmentManager manager;
    private RadioGroup rg_class_status_content; //student_mien 的Radiogroup
    private ArrayList<Integer> radio_btn_arrayList = new ArrayList();
    private String isBig;

    @BindView(R.id.empty_view)
    View mEmpthView;
    @BindView(R.id.iv_close_btn)
    ImageView mIvCloseBtn;

    @Override
    public void onRoot(Bundle savedInstanceState) {
        setContentView(R.layout.activity_class_mien_layout);
    }

    @Override
    protected void initInjector() {
        DaggerClassMienComponent.builder().applicationComponent(getAppComponent())
                .classMienActivityModule(new ClassMienActivityModule(this)).build().inject(this);
    }

    @Override
    protected void initView() {
        rg_class_status_content = (RadioGroup) findViewById(R.id.rg_class_status_content);
    }

    @Override
    public void initData() {
        isBig = getIntent().getStringExtra("isBig");
        if (isBig != null && "isBig".equals(isBig)) {
            mEmpthView.setVisibility(View.VISIBLE);
        }
        manager = this.getSupportFragmentManager();
        radio_btn_arrayList.add(R.id.radio_btn_class_info);
        radio_btn_arrayList.add(R.id.radio_btn_class_honour);
        radio_btn_arrayList.add(R.id.radio_btn_class_growth_ring);
        mNewPresenter.showClassStatus(this, R.id.radio_btn_class_info, manager,isBig);

    }

    @Override
    public void initListener() {
        rg_class_status_content.setOnCheckedChangeListener(this);
        mIvCloseBtn.setOnClickListener(this);
    }

    @Override
    public void WidgetClick(View view) {
        finish();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        mNewPresenter.showClassStatus(this, checkedId, manager,null);

    }
}
