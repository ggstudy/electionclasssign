package classign.election.kingvar.come.electionclasssign.injector.modules;

import classign.election.kingvar.come.electionclasssign.injector.PerFragment;
import classign.election.kingvar.come.electionclasssign.module.base.IBasePresenter;
import classign.election.kingvar.come.electionclasssign.module.main.studentmien.classreward.ClassRewardFragment;
import classign.election.kingvar.come.electionclasssign.module.main.studentmien.classreward.ClassRewardPresenter;
import dagger.Module;
import dagger.Provides;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/29 0029 17:24
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */
@Module
public class ClassRewardModule {
    private final ClassRewardFragment mClassRewardFragment;
    private final String mType;
    private final String mStudentid;
    public ClassRewardModule(ClassRewardFragment classRewardFragment,String type,String studentid) {
        this.mClassRewardFragment = classRewardFragment;
        this.mType = type;
        this.mStudentid = studentid;

    }

    @PerFragment
    @Provides
    public IBasePresenter provideMainPresenter(){
        return new ClassRewardPresenter(mClassRewardFragment,mType,mStudentid);
    }
}
