package classign.election.kingvar.come.electionclasssign.module.main.moraleducation.three;

import android.util.Log;

import classign.election.kingvar.come.electionclasssign.api.RetrofitService;
import classign.election.kingvar.come.electionclasssign.api.info.AttendanceInfo;
import classign.election.kingvar.come.electionclasssign.module.base.IBasePresenter;
import classign.election.kingvar.come.electionclasssign.module.main.IAttendanceRecoderView;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/16 0016 15:05
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public class AttendanceRecoderThreePersenter implements IBasePresenter {
    private static final String TAG = "AttendanceRecoderPersen";
    private final IAttendanceRecoderView mView;
    private final String mClsid;
    private final String mTid;

    public AttendanceRecoderThreePersenter(TodayAttendanceThreeFragment threeFragment, String clsid, String tid) {
        mView = threeFragment;
        mClsid = clsid;
        mTid = tid;
        Log.i(TAG, "provideMainPresenter: mClsid = " + mClsid);
        Log.i(TAG, "provideMainPresenter: mTid = " + mTid);
    }


    @Override
    public void getData(boolean isRefresh) {
        Log.i(TAG, "getData: mClsid =  " + mClsid +  mTid);
        RetrofitService.getAttendanceRecorder(mClsid,mTid)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<AttendanceInfo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(AttendanceInfo attendanceInfo) {
                        if (attendanceInfo.getCode() == 0) {
                            mView.loadData(attendanceInfo);

                        }
                    }
                });
    }

    @Override
    public void getMoreData() {

    }
}
