package classign.election.kingvar.come.electionclasssign.injector.components;


import classign.election.kingvar.come.electionclasssign.injector.PerFragment;
import classign.election.kingvar.come.electionclasssign.module.main.classmien.classgrowth.ClassGrowthRingFragment;
import classign.election.kingvar.come.electionclasssign.injector.modules.ClassGrowthRingModule;
import dagger.Component;

/**
 * Created by long on 2016/8/19.
 */
@PerFragment
@Component(dependencies = ApplicationComponent.class, modules = {ClassGrowthRingModule.class})
public interface ClassGrowthRingComponent {
  void inject(ClassGrowthRingFragment fragment);
}