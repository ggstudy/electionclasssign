package classign.election.kingvar.come.electionclasssign.injector.components;


import classign.election.kingvar.come.electionclasssign.injector.PerFragment;
import classign.election.kingvar.come.electionclasssign.injector.modules.EncourageModule;
import classign.election.kingvar.come.electionclasssign.module.main.studentmien.encourage.EncourageTalkFragment;
import dagger.Component;

/**
 * Created by long on 2016/8/19.
 */
@PerFragment
@Component(dependencies = ApplicationComponent.class, modules = {EncourageModule.class})
public interface EncourageTalkInfoComponent {
  void inject(EncourageTalkFragment fragment);
}