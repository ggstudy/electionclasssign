package classign.election.kingvar.come.electionclasssign.adapter;

import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

import classign.election.kingvar.come.electionclasssign.R;

/**
 * Created by JYQ on 2017/1/11 0011.
 */
public class MyPagerAdapter extends PagerAdapter {

	private List<ImageView> list;
	FragmentActivity activity;
	public MyPagerAdapter(FragmentActivity activity, List<ImageView> list) {
		this.activity =activity;
		this.list = list;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == object;
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		View view = LayoutInflater.from(activity).inflate(R.layout.layout_monthly_bills_item, null);
//		container.addView(list.get(position));
		container.addView(view);
		return view;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		container.removeView((View) object);
		object = null;
	}

	/**
	 * 设置该页内容所占屏幕的宽度
	 */
	/*@Override
	public float getPageWidth(int position) {
		//return 1.f; 默认返回1,代表该position占据了ViewPager的一整页,范围(0,1]
		return 0.8f;
	}*/
}
