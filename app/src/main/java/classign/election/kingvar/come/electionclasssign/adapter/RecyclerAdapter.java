package classign.election.kingvar.come.electionclasssign.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by jay on 2015/11/23.
 */
public abstract class RecyclerAdapter<D> extends RecyclerView.Adapter<RecyclerViewHolder> {
	private static final String TAG = "RecyclerAdapter";
	private   List<D> mDataSet ;

	private int mItemLayoutId;

	protected OnItemClickListener mOnItemClickListener;
	private OnItemLongClickListener mOnItemLongClickListener;
	private OnItemTouchListener mOnItemTouchListener;

	public RecyclerAdapter() {
	}

	public RecyclerAdapter(int itemLayoutId) {
		mItemLayoutId = itemLayoutId;
	}

	public RecyclerAdapter(int itemLayoutId, List<D> mDataSet) {
		mItemLayoutId = itemLayoutId;
		this.mDataSet = mDataSet;
	}
	public void setItemDate(List<D> mDataSet) {
		this.mDataSet = mDataSet;
		notifyDataSetChanged();
	}
	public void addItem(D item) {
		mDataSet.add(item);
		notifyDataSetChanged();
	}

	public void addItems(List<D> data) {
		mDataSet.addAll(data);
		notifyDataSetChanged();
	}

	public void remove(int position) {
		mDataSet.remove(position);
		notifyDataSetChanged();
	}

	public void remove(D item) {
		mDataSet.remove(item);
		notifyDataSetChanged();
	}

	public void clear() {
		mDataSet.clear();
		notifyDataSetChanged();
	}

	public void addNewItems(List<D> data) {
		mDataSet.clear();
		mDataSet.addAll(data);
		notifyDataSetChanged();
	}

	public D getItem(int position) {
		return mDataSet.get(position);
	}

	@Override
	public int getItemCount() {
		return mDataSet.size();
	}

	/**
	 *  设置不同类型 Item 的 resId
	 * @param viewType
	 * @return
	 */
	public int getItemLayout(int viewType) {
		return mItemLayoutId;
	}

	@Override
	public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		return new RecyclerViewHolder(inflateItemView(parent, viewType));
	}

	private View inflateItemView(ViewGroup parent, int viewType) {
		int itemLayout = getItemLayout(viewType);
		Context context = parent.getContext();
		return LayoutInflater.from(context).inflate(itemLayout, parent, false);
	}

	@Override
	public void onBindViewHolder(RecyclerViewHolder holder, int position) {
		final D item = getItem(position);

		onBindData(holder, position, item);
		setupItemClickListener(holder, position);
		setupItemTouchListener(holder, position);
		setupItemLongClickListener(holder,position);
	}

	private void setupItemLongClickListener(RecyclerViewHolder holder, final int position) {
		holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				if (mOnItemLongClickListener!=null){
					mOnItemLongClickListener.onItemLongClick(position);
				}
				return false;
			}
		});
	}

	protected void setupItemClickListener(RecyclerViewHolder holder,
			final int position) {
		holder.itemView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mOnItemClickListener != null) {
					mOnItemClickListener.onItemClick(position);
				}
			}
		});
	}

	protected void setupItemTouchListener(RecyclerViewHolder holder,
			final int position) {
		holder.itemView.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (mOnItemTouchListener != null) {
					mOnItemTouchListener.onItemTouch(v, event, position);
				}
				return false;
			}
		});
	}



    /**
	 * 设置点击事件
	 * 
	 */
	public interface OnItemClickListener {
		void onItemClick(int position);
	}
	public void setOnItemClickListener(OnItemClickListener listener) {
		this.mOnItemClickListener = listener;
	}

	/**
	 * 触摸事件Listener
	 */

	public interface OnItemTouchListener {
		void onItemTouch(View v, MotionEvent event, int position);
	}
	public void setOnItemTouchListener(OnItemTouchListener onItemTouchListener) {
		mOnItemTouchListener = onItemTouchListener;
	}

	/**
	 * 长按事件Listener
	 */
	public interface OnItemLongClickListener {
		void onItemLongClick(int position);
	}

	public void setOnItemLongTouchListener(OnItemLongClickListener OnItemLongClickListener) {
		this.mOnItemLongClickListener = OnItemLongClickListener;
	}

	protected abstract void onBindData(RecyclerViewHolder holder, int position,
			D item);

}
