package classign.election.kingvar.come.electionclasssign.injector.modules;

import classign.election.kingvar.come.electionclasssign.injector.PerActivity;
import classign.election.kingvar.come.electionclasssign.module.main.classmien.ClassMienActivity;
import classign.election.kingvar.come.electionclasssign.module.main.classmien.ClassMienPersenter;
import classign.election.kingvar.come.electionclasssign.module.main.weekdays.IMainActivityPresenter;
import dagger.Module;
import dagger.Provides;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/16 0016 15:28
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */
@Module
public class ClassMienActivityModule {
    private final ClassMienActivity classMienActivity;

    public ClassMienActivityModule(ClassMienActivity classMienActivity) {
        this.classMienActivity = classMienActivity;
    }

    @PerActivity
    @Provides
    public IMainActivityPresenter provideTodayProjectPresenter() {
        return new ClassMienPersenter(classMienActivity);
    }


}
