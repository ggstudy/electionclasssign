package classign.election.kingvar.come.electionclasssign.module.main.studentmien.classreward;

import classign.election.kingvar.come.electionclasssign.api.RetrofitService;
import classign.election.kingvar.come.electionclasssign.api.info.RewardInfo;
import classign.election.kingvar.come.electionclasssign.module.base.IBasePresenter;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/29 0029 17:29
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public class ClassRewardPresenter implements IBasePresenter {
    private final IRewardView mView;
    private final String mType;
    private final String mStudentid;

    public ClassRewardPresenter(ClassRewardFragment mClassRewardFragment, String mType, String mStudentid) {
        this.mView = mClassRewardFragment;
        this.mType = mType;
        this.mStudentid = mStudentid;
    }

    @Override
    public void getData(boolean isRefresh) {
        RetrofitService.getRewardInfo(mType, mStudentid)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<RewardInfo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(RewardInfo rewardInfo) {
                        if (rewardInfo.getCode() == 0) {
                            mView.loadData(rewardInfo.getWinning());
                        }
                    }
                });
    }

    @Override
    public void getMoreData() {

    }
}
