package classign.election.kingvar.come.electionclasssign.autolayout.attr;

import android.view.View;

/**
 * Created by jyq on 16/12/4.
 */
public class PaddingBottomAttr extends AutoAttr {
	public PaddingBottomAttr(int pxVal, int baseWidth, int baseHeight) {
		super(pxVal, baseWidth, baseHeight);
	}

	@Override
	protected int attrVal() {
		return Attrs.PADDING_BOTTOM;
	}

	@Override
	protected boolean defaultBaseWidth() {
		return false;
	}

	@Override
	protected void execute(View view, int val) {
		int l = view.getPaddingLeft();
		int t = view.getPaddingTop();
		int r = view.getPaddingRight();
		int b = val;
		view.setPadding(l, t, r, b);

	}
}
