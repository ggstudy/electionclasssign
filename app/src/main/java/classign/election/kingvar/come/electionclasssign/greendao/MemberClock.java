package classign.election.kingvar.come.electionclasssign.greendao;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/1 0001 15:06
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */
@Entity
public class MemberClock {
    @Id(autoincrement = true)
    private Long id;
    private int memType;
    private int memID;
    private long time;
    @Generated(hash = 1115696827)
    public MemberClock(Long id, int memType, int memID, long time) {
        this.id = id;
        this.memType = memType;
        this.memID = memID;
        this.time = time;
    }
    @Generated(hash = 699460354)
    public MemberClock() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public int getMemType() {
        return this.memType;
    }
    public void setMemType(int memType) {
        this.memType = memType;
    }
    public int getMemID() {
        return this.memID;
    }
    public void setMemID(int memID) {
        this.memID = memID;
    }
    public long getTime() {
        return this.time;
    }
    public void setTime(long time) {
        this.time = time;
    }
    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (o == this) {
            return true;
        }
        if (getClass() != o.getClass()) {
            return false;
        }
        MemberClock memberClock = (MemberClock) o;
        return (memID==memberClock.memID);
    }

    @Override
    public int hashCode() {
        return memID+memType;
    }

    @Override
    public String toString() {
        return "MemberClock{" +
                "id=" + id +
                ", memType=" + memType +
                ", memID=" + memID +
                ", time=" + time +
                '}';
    }
}
