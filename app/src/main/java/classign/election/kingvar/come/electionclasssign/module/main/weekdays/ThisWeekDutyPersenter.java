package classign.election.kingvar.come.electionclasssign.module.main.weekdays;

import classign.election.kingvar.come.electionclasssign.activity.ThisWeekDutyActivity;
import classign.election.kingvar.come.electionclasssign.api.RetrofitService;
import classign.election.kingvar.come.electionclasssign.api.info.ThisWeekDutyInfo;
import classign.election.kingvar.come.electionclasssign.module.base.IBasePresenter;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/29 0029 19:54
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public class ThisWeekDutyPersenter implements IBasePresenter {
    private final IThisWeekDutyView mView;
    private final String mClassId;

    public ThisWeekDutyPersenter(ThisWeekDutyActivity mThisWeekDutyActivity, String classId) {
        this.mView = mThisWeekDutyActivity;
        this.mClassId = classId;
        getData(true);

    }

    @Override
    public void getData(boolean isRefresh) {
        RetrofitService.getThisWeekDutyInfo(mClassId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ThisWeekDutyInfo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(ThisWeekDutyInfo weekDutyInfo) {
                        if (weekDutyInfo.getCode() == 0) {
                            mView.LoadWeekDutyDate(weekDutyInfo);

                        }
                    }
                });

    }

    @Override
    public void getMoreData() {

    }
}
