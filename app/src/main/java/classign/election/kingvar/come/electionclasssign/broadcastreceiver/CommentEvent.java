package classign.election.kingvar.come.electionclasssign.broadcastreceiver;

import android.support.annotation.IntDef;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/7/7 0007 10:03
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public class CommentEvent {

    /**
     * 频道事件：添加、删除和交换位置
     */
    public static final int NETWORkSTATE = 301;


    @Retention(RetentionPolicy.SOURCE)
    @Target(ElementType.PARAMETER)
    @IntDef({NETWORkSTATE})
    public @interface ChannelEventType {
    }

    public int netWorkState;
    public int eventType;

    public CommentEvent(@ChannelEventType int eventType, int netWorkState) {
        this.eventType = eventType;
        this.netWorkState = netWorkState;
    }

}
