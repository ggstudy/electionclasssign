package classign.election.kingvar.come.electionclasssign.api.info;

import java.util.List;

/**
 * Created by Administrator on 2017/1/5 0005.
 */
public class ProReward {

	/**
	 * code : 0
	 * winning : [{"studentid":"24","date":1483372800000,"name":"杜平","logo":"20170105100016669757.png","id":2,"detail":"班主任小助手","title":"2016年下学期"}]
	 */

	private int code;
	private List<WinningBean> winning;

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public List<WinningBean> getWinning() {
		return winning;
	}

	public void setWinning(List<WinningBean> winning) {
		this.winning = winning;
	}

	public static class WinningBean {
		/**
		 * studentid : 24
		 * date : 1483372800000
		 * name : 杜平
		 * logo : 20170105100016669757.png
		 * id : 2
		 * detail : 班主任小助手
		 * title : 2016年下学期
		 */

		private String studentid;
		private long date;
		private String name;
		private String logo;
		private int id;
		private String detail;
		private String title;

		public String getStudentid() {
			return studentid;
		}

		public void setStudentid(String studentid) {
			this.studentid = studentid;
		}

		public long getDate() {
			return date;
		}

		public void setDate(long date) {
			this.date = date;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getLogo() {
			return logo;
		}

		public void setLogo(String logo) {
			this.logo = logo;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getDetail() {
			return detail;
		}

		public void setDetail(String detail) {
			this.detail = detail;
		}

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}
	}
}
