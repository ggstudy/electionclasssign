package classign.election.kingvar.come.electionclasssign.api.info;

import java.util.List;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/1 0001 18:44
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public class ProMessageContent {
    /**
     * code : 0
     * data : [{"name":"哈哈","logo":"20170105100016669757.png","time":1488368104000,"content":"fdfdfdfd"},{"name":"玥儿","logo":"","time":1488523805000,"content":"超级兔子墨迹"},{"name":"玥儿","logo":"","time":1488523832000,"content":"mats h"},{"name":"玥儿","logo":"","time":1489458627000,"content":"如风达方法"},{"name":"小妮","logo":"2017070709075778124.png","time":1489459063000,"content":"人多发发发"},{"name":"小妮","logo":"2017070709075778124.png","time":1489459311000,"content":"人多方法"},{"name":"玥儿","logo":"","time":1489460610000,"content":"额点点滴滴"},{"name":"小妮","logo":"2017070709075778124.png","time":1489460637000,"content":"额是多大的"},{"name":"小妮","logo":"2017070709075778124.png","time":1489460653000,"content":"请问"},{"name":"小妮","logo":"2017070709075778124.png","time":1490168684000,"content":"GPS"}]
     * tList : [{"time":1499651556000},{"time":null},{"time":null},{"time":null},{"time":null},{"time":null},{"time":null},{"time":null},{"time":null},{"time":null}]
     */

    private int code;
    private List<DataEntity> data;
    private List<TListEntity> tList;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<DataEntity> getData() {
        return data;
    }

    public void setData(List<DataEntity> data) {
        this.data = data;
    }

    public List<TListEntity> getTList() {
        return tList;
    }

    public void setTList(List<TListEntity> tList) {
        this.tList = tList;
    }

    public static class DataEntity {
        /**
         * name : 哈哈
         * logo : 20170105100016669757.png
         * time : 1488368104000
         * content : fdfdfdfd
         */

        private String name;
        private String logo;
        private long time;
        private String content;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

        public long getTime() {
            return time;
        }

        public void setTime(long time) {
            this.time = time;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }

    public static class TListEntity {
        /**
         * time : 1499651556000
         */

        private long time;

        public long getTime() {
            return time;
        }

        public void setTime(long time) {
            this.time = time;
        }
    }

}
