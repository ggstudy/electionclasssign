package classign.election.kingvar.come.electionclasssign.greendao;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Unique;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by JYQ on 2017/1/11 0011.
 */
@Entity
public class StudentInfo {
    @Id(autoincrement = true)
    private Long id;
    @Unique
    private int studentid;
    private String no;
    private String registerID;
    private String name;
    private String logo;
    private String memo;
    private String schoolDuty;
    private String duty;
    private int tag;

    @Generated(hash = 718656705)
    public StudentInfo(Long id, int studentid, String no, String registerID,
                       String name, String logo, String memo, String schoolDuty, String duty,
                       int tag) {
        this.id = id;
        this.studentid = studentid;
        this.no = no;
        this.registerID = registerID;
        this.name = name;
        this.logo = logo;
        this.memo = memo;
        this.schoolDuty = schoolDuty;
        this.duty = duty;
        this.tag = tag;
    }

    @Generated(hash = 2016856731)
    public StudentInfo() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getStudentid() {
        return this.studentid;
    }

    public void setStudentid(int studentid) {
        this.studentid = studentid;
    }

    public String getNo() {
        return this.no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getRegisterID() {
        return this.registerID;
    }

    public void setRegisterID(String registerID) {
        this.registerID = registerID;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return this.logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getMemo() {
        return this.memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getSchoolDuty() {
        return this.schoolDuty;
    }

    public void setSchoolDuty(String schoolDuty) {
        this.schoolDuty = schoolDuty;
    }

    public String getDuty() {
        return this.duty;
    }

    public void setDuty(String duty) {
        this.duty = duty;
    }

    public int getTag() {
        return this.tag;
    }

    public void setTag(int tag) {
        this.tag = tag;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (o == this) {
            return true;
        }
        if (getClass() != o.getClass()) {
            return false;
        }
        StudentInfo member = (StudentInfo) o;
        return (studentid == member.studentid);
    }

    @Override
    public int hashCode() {
        return studentid + registerID.hashCode();
    }

    @Override
    public String toString() {
        return "StudentInfo{" +
                "id=" + id +
                ", studentid=" + studentid +
                ", no='" + no + '\'' +
                ", registerID='" + registerID + '\'' +
                ", name='" + name + '\'' +
                ", logo='" + logo + '\'' +
                ", memo='" + memo + '\'' +
                ", schoolDuty='" + schoolDuty + '\'' +
                ", duty='" + duty + '\'' +
                ", tag=" + tag +
                '}';
    }
}
