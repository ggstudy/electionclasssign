package classign.election.kingvar.come.electionclasssign.api.info;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/22 0022 19:52
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public class TodayDutyInfo implements Parcelable{

    /**
     * onduty : {"date":"2017-03-22","clsid":"7","leader":"彭易星","stus":[{"sname":"理想"},{"sname":"老师"}],"id":2}
     * code : 0
     */

    private OndutyEntity onduty;
    private int code;

    protected TodayDutyInfo(Parcel in) {
        code = in.readInt();
    }

    public static final Creator<TodayDutyInfo> CREATOR = new Creator<TodayDutyInfo>() {
        @Override
        public TodayDutyInfo createFromParcel(Parcel in) {
            return new TodayDutyInfo(in);
        }

        @Override
        public TodayDutyInfo[] newArray(int size) {
            return new TodayDutyInfo[size];
        }
    };

    public OndutyEntity getOnduty() {
        return onduty;
    }

    public void setOnduty(OndutyEntity onduty) {
        this.onduty = onduty;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(code);
    }

    public static class OndutyEntity implements Parcelable {
        /**
         * date : 2017-03-22
         * clsid : 7
         * leader : 彭易星
         * stus : [{"sname":"理想"},{"sname":"老师"}]
         * id : 2
         */

        private String date;
        private String clsid;
        private String leader;
        private int id;
        private List<StusEntity> stus;

        protected OndutyEntity(Parcel in) {
            date = in.readString();
            clsid = in.readString();
            leader = in.readString();
            id = in.readInt();
        }

        public static final Creator<OndutyEntity> CREATOR = new Creator<OndutyEntity>() {
            @Override
            public OndutyEntity createFromParcel(Parcel in) {
                return new OndutyEntity(in);
            }

            @Override
            public OndutyEntity[] newArray(int size) {
                return new OndutyEntity[size];
            }
        };

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getClsid() {
            return clsid;
        }

        public void setClsid(String clsid) {
            this.clsid = clsid;
        }

        public String getLeader() {
            return leader;
        }

        public void setLeader(String leader) {
            this.leader = leader;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public List<StusEntity> getStus() {
            return stus;
        }

        public void setStus(List<StusEntity> stus) {
            this.stus = stus;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(date);
            dest.writeString(clsid);
            dest.writeString(leader);
            dest.writeInt(id);
        }

        public static class StusEntity implements Parcelable{
            /**
             * sname : 理想
             */

            private String sname;

            protected StusEntity(Parcel in) {
                sname = in.readString();
            }

            public static final Creator<StusEntity> CREATOR = new Creator<StusEntity>() {
                @Override
                public StusEntity createFromParcel(Parcel in) {
                    return new StusEntity(in);
                }

                @Override
                public StusEntity[] newArray(int size) {
                    return new StusEntity[size];
                }
            };

            public String getSname() {
                return sname;
            }

            public void setSname(String sname) {
                this.sname = sname;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(sname);
            }
        }
    }
}
