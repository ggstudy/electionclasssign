package classign.election.kingvar.come.electionclasssign.injector.modules;

import classign.election.kingvar.come.electionclasssign.injector.PerFragment;
import classign.election.kingvar.come.electionclasssign.module.base.IBasePresenter;
import classign.election.kingvar.come.electionclasssign.module.main.studentmien.mydream.MyDreamFragment;
import classign.election.kingvar.come.electionclasssign.module.main.studentmien.mydream.MyDreamPresenter;
import dagger.Module;
import dagger.Provides;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/29 0029 17:24
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */
@Module
public class MyDreamModule {
    private final MyDreamFragment dreamFragment;
    private final String mStudentid;
    public MyDreamModule(MyDreamFragment dreamFragment, String studentid) {
        this.dreamFragment = dreamFragment;
        this.mStudentid = studentid;

    }

    @PerFragment
    @Provides
    public IBasePresenter provideMainPresenter(){
        return new MyDreamPresenter(dreamFragment,mStudentid);
    }
}
