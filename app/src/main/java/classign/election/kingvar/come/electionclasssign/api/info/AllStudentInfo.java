package classign.election.kingvar.come.electionclasssign.api.info;

import java.util.List;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/31 0031 15:08
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public class AllStudentInfo {


    /**
     * code : 0
     * info : [{"studentid":10,"no":"201701111018","registerID":"0006049334","name":"彭同学","logo":"","memo":""},{"studentid":2,"no":"2016122301","registerID":"3177758625","schoolDuty":"学生会部长","name":"哈哈","logo":"20170105100016669757.png","memo":"夫学须志也，才须学也，非学无以广才，非志无以成学。&mdash;&mdash;诸葛亮","duty":"班长,学习委员"},{"studentid":4,"no":"201701050958","name":"缘分","logo":"20170105100037103688.png","memo":"勤奋是学习的枝叶，当然很苦，智慧是学习的花朵，当然香郁。"},{"studentid":5,"no":"201701050959","name":"孙颖慧","logo":"20170105100129711553.jpg","memo":"习惯改变命运，细节铸就终身。"}]
     */

    private int code;
    private List<InfoBean> info;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<InfoBean> getInfo() {
        return info;
    }

    public void setInfo(List<InfoBean> info) {
        this.info = info;
    }

    public static class InfoBean {
        /**
         * studentid : 10
         * no : 201701111018
         * registerID : 0006049334
         * name : 彭同学
         * logo :
         * memo :
         * schoolDuty : 学生会部长
         * duty : 班长,学习委员
         */

        private int studentid;
        private String no;
        private String registerID;
        private String name;
        private String logo;
        private String memo;
        private String schoolDuty;
        private String duty;

        public int getStudentid() {
            return studentid;
        }

        public void setStudentid(int studentid) {
            this.studentid = studentid;
        }

        public String getNo() {
            return no;
        }

        public void setNo(String no) {
            this.no = no;
        }

        public String getRegisterID() {
            return registerID;
        }

        public void setRegisterID(String registerID) {
            this.registerID = registerID;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

        public String getMemo() {
            return memo;
        }

        public void setMemo(String memo) {
            this.memo = memo;
        }

        public String getSchoolDuty() {
            return schoolDuty;
        }

        public void setSchoolDuty(String schoolDuty) {
            this.schoolDuty = schoolDuty;
        }

        public String getDuty() {
            return duty;
        }

        public void setDuty(String duty) {
            this.duty = duty;
        }
    }
}
