package classign.election.kingvar.come.electionclasssign.tools.views;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.TextView;

import classign.election.kingvar.come.electionclasssign.R;


/**
 * Created by Administrator on 2016/6/15 0015.
 */


//自定义 popupwindow   (显示弹窗 效果)
public class SelectPicPopupWindow extends PopupWindow {

	View mPopView; //popuwindow的 布局页面
	public TextView tv_video_babydiary_share;  //布局页面的分享
	public TextView tv_video_babydiary_delete;//布局页面的删除
	public TextView tv_video_babydiary_cancle;//布局页面的取消
	public TextView tv_look_big_pic;//查看大图

	public SelectPicPopupWindow(Context context, View.OnClickListener itemsOnClick) {
		super(context);
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mPopView = inflater.inflate(R.layout.babydiary_more_pop_window, null);

		tv_video_babydiary_share = (TextView) mPopView.findViewById(R.id.tv_video_babydiary_share);
		tv_video_babydiary_delete = (TextView) mPopView.findViewById(R.id.tv_video_babydiary_delete);
		tv_video_babydiary_cancle = (TextView) mPopView.findViewById(R.id.tv_video_babydiary_cancle);
		tv_look_big_pic = (TextView) mPopView.findViewById(R.id.tv_look_big_pic);


		//取消按钮
		tv_video_babydiary_cancle.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				//销毁弹出框
				dismiss();
			}
		});
		//设置按钮监听
		tv_video_babydiary_share.setOnClickListener(itemsOnClick);
		tv_video_babydiary_delete.setOnClickListener(itemsOnClick);
		tv_look_big_pic.setOnClickListener(itemsOnClick);

		//设置SelectPicPopupWindow的View
		this.setContentView(mPopView);
		//设置SelectPicPopupWindow弹出窗体的宽
		this.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
		//设置SelectPicPopupWindow弹出窗体的高
		this.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
		//设置SelectPicPopupWindow弹出窗体可点击
		this.setFocusable(true);
		//设置SelectPicPopupWindow弹出窗体动画效果
		this.setAnimationStyle(R.style.mypopwindow_anim_style);
		//实例化一个ColorDrawable颜色为半透明
		ColorDrawable dw = new ColorDrawable(Color.TRANSPARENT);
		//设置SelectPicPopupWindow弹出窗体的背景
		this.setBackgroundDrawable(dw);
		//mPopView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
		mPopView.setOnTouchListener(new View.OnTouchListener() {

			public boolean onTouch(View v, MotionEvent event) {

				int height = mPopView.findViewById(R.id.pop_layout).getTop();
				int y=(int) event.getY();
				if(event.getAction()==MotionEvent.ACTION_UP){
					if(y<height){
						dismiss();
					}
				}
				return true;
			}
		});

	}

}
