package classign.election.kingvar.come.electionclasssign.module.main;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.orhanobut.logger.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import classign.election.kingvar.come.electionclasssign.R;
import classign.election.kingvar.come.electionclasssign.activity.MainActivity;
import classign.election.kingvar.come.electionclasssign.activity.MyApplication;
import classign.election.kingvar.come.electionclasssign.adapter.RecyclerAdapter;
import classign.election.kingvar.come.electionclasssign.adapter.RecyclerViewHolder;
import classign.election.kingvar.come.electionclasssign.api.RetrofitService;
import classign.election.kingvar.come.electionclasssign.api.info.AllProjectInfo;
import classign.election.kingvar.come.electionclasssign.api.info.AllStudentInfo;
import classign.election.kingvar.come.electionclasssign.api.info.BaseClassInfo;
import classign.election.kingvar.come.electionclasssign.api.info.HeartbeatCmd;
import classign.election.kingvar.come.electionclasssign.api.info.ProClassAndSchoolNotice;
import classign.election.kingvar.come.electionclasssign.api.info.ProClassSchedule;
import classign.election.kingvar.come.electionclasssign.api.info.ProTemperature;
import classign.election.kingvar.come.electionclasssign.api.info.TodayDeYuInfo;
import classign.election.kingvar.come.electionclasssign.api.info.TodayDutyInfo;
import classign.election.kingvar.come.electionclasssign.api.info.TodayDutyInfo.OndutyEntity.StusEntity;
import classign.election.kingvar.come.electionclasssign.api.info.VersionUpdateInfo;
import classign.election.kingvar.come.electionclasssign.greendao.ProjectFirdayInfo;
import classign.election.kingvar.come.electionclasssign.greendao.ProjectFirdayInfoDao;
import classign.election.kingvar.come.electionclasssign.greendao.ProjectInfo;
import classign.election.kingvar.come.electionclasssign.greendao.ProjectInfoDao;
import classign.election.kingvar.come.electionclasssign.greendao.ProjectSaturdayInfo;
import classign.election.kingvar.come.electionclasssign.greendao.ProjectSaturdayInfoDao;
import classign.election.kingvar.come.electionclasssign.greendao.ProjectSundayInfo;
import classign.election.kingvar.come.electionclasssign.greendao.ProjectSundayInfoDao;
import classign.election.kingvar.come.electionclasssign.greendao.ProjectTuesdayInfo;
import classign.election.kingvar.come.electionclasssign.greendao.ProjectTuesdayInfoDao;
import classign.election.kingvar.come.electionclasssign.greendao.ProjectTursdayInfo;
import classign.election.kingvar.come.electionclasssign.greendao.ProjectTursdayInfoDao;
import classign.election.kingvar.come.electionclasssign.greendao.ProjectWednesdayInfo;
import classign.election.kingvar.come.electionclasssign.greendao.ProjectWednesdayInfoDao;
import classign.election.kingvar.come.electionclasssign.module.main.classmien.classgrowth.ClassGrowthRingFragment;
import classign.election.kingvar.come.electionclasssign.module.main.classmien.classhonor.ClassHonourFragment;
import classign.election.kingvar.come.electionclasssign.module.main.classmien.classinfo.ClassInfoFragment;
import classign.election.kingvar.come.electionclasssign.module.main.studentmien.classreward.ClassRewardFragment;
import classign.election.kingvar.come.electionclasssign.module.main.studentmien.encourage.EncourageTalkFragment;
import classign.election.kingvar.come.electionclasssign.module.main.studentmien.mydream.MyDreamFragment;
import classign.election.kingvar.come.electionclasssign.module.main.studentmien.schoolreward.SchoolRewardFragment;
import classign.election.kingvar.come.electionclasssign.module.main.studentmien.todaystar.TodayStarFragment;
import classign.election.kingvar.come.electionclasssign.module.main.weekdays.IMainActivityPresenter;
import classign.election.kingvar.come.electionclasssign.tools.bean.User;
import classign.election.kingvar.come.electionclasssign.tools.views.CustomShowDialog;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/29 0029 20:37
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public class MainActivityPersenter implements IMainActivityPresenter {
    private static final String TAG = "TodayProjectPersenter";
    private final MainActivityView mView;
    private String classId;
    private String schoolid;
    private String imei;
    private RecyclerAdapter mRecyclerAdapter;
    List<AllProjectInfo.NightEntity> entityList = new ArrayList<>();
    private RecyclerAdapter nightAdapter;

    private ClassInfoFragment classInfoFragment;
    private ClassHonourFragment classHonourFragment;
    private ClassGrowthRingFragment classGrowthRingFragment;

    private TodayStarFragment todayStarFragment;
    private ClassRewardFragment classRewardFragment;
    private EncourageTalkFragment encourageTalkFragment;
    private MyDreamFragment myDreamFragment;
    private SchoolRewardFragment schoolRewardFragment;

    public MainActivityPersenter(MainActivity mMainActivity, String classId, String imei) {
        this.mView = mMainActivity;
        this.classId = classId;
        this.imei = imei;
        getData(false);
    }

    @Override
    public void getData(boolean isRefresh) {
        RetrofitService.getBaseClassInfo(imei)
                .subscribe(new Subscriber<BaseClassInfo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Logger.e(e.toString());

                    }

                    @Override
                    public void onNext(BaseClassInfo baseClassInfo) {
                        mView.loadClassInfoData(baseClassInfo);
                        if (baseClassInfo.getCode() == 0) {
                            classId = baseClassInfo.getInfo().getClsid();
                            schoolid = String.valueOf(baseClassInfo.getInfo().getSchoolid());
                            getStudentAllInfoData();
                        }
                    }
                });
    }

    /**
     * 获取学校公告的信息
     */
    private void getNoticeInfoData() {
        RetrofitService.getNoticeInfo(classId, schoolid)
                .subscribe(new Subscriber<ProClassAndSchoolNotice>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Logger.e(e.toString());

                    }

                    @Override
                    public void onNext(ProClassAndSchoolNotice proClassAndSchoolNotice) {
                        if (proClassAndSchoolNotice.getCode() == 0) {
                            List<ProClassAndSchoolNotice.SchoolInfoEntity> schoolInfoEntityList = proClassAndSchoolNotice.getSchoolInfo();
                            mView.setSchoolNoticeList((ArrayList<ProClassAndSchoolNotice.SchoolInfoEntity>) schoolInfoEntityList, schoolInfoEntityList.size());
                            List<ProClassAndSchoolNotice.ClsNoticeEntity> clsNoticeEntityList = proClassAndSchoolNotice.getClsNotice();
                            mView.setClassNoticeList((ArrayList<ProClassAndSchoolNotice.ClsNoticeEntity>) clsNoticeEntityList, clsNoticeEntityList.size());
                        }
                    }
                });
    }

    /**
     * 获取学生全部的信息 保存到数据库
     */
    public void getStudentAllInfoData() {
        RetrofitService.getAllStudentInfo(classId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<AllStudentInfo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Logger.e(e.toString());

                    }

                    @Override
                    public void onNext(AllStudentInfo allStudentInfo) {
                        if (allStudentInfo.getCode() == 0) {
                            mView.loadAllStudentInfoData(allStudentInfo);
                            getVersionUpdateData();
                            getTodayDutyData();
                            getWeekProjectData();
                            getNoticeInfoData();
                            getClassScheduleInfoData();
                            getTemperatureInfoData();
                            getTodayDeYuInfoData();
                            getHeartBeatType();
                        }
                    }
                });
    }

    /**
     * 获取今日德育数据
     */
    private void getTodayDeYuInfoData() {
        RetrofitService.getTodayDeYuInfo(classId)
                .subscribe(new Subscriber<TodayDeYuInfo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Logger.e(e.toString());
                    }

                    @Override
                    public void onNext(TodayDeYuInfo todayDeYuInfo) {
                        Log.i(TAG, "onNext: today = " + todayDeYuInfo.getCode());
                        if (todayDeYuInfo.getCode() == 0) {
                            mView.setTodayDeYuInfoData(todayDeYuInfo);
                        }
                    }
                });
    }

    /**
     * 心跳
     */
    private void getHeartBeatType() {
        Observable.interval(30, 30, TimeUnit.SECONDS)
                .subscribe(new Subscriber<Long>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Logger.e(e.toString());
                    }

                    @Override
                    public void onNext(Long aLong) {
                        Log.i(TAG, "onNext: along  = " + aLong);
                        RetrofitService.getHeartbeatCmd(imei)
                                .flatMap(heartbeatCmd -> {
                                    if (heartbeatCmd.getCode() == 2) {
                                        if (heartbeatCmd.getCmd().size() == 0) {
                                            return Observable.empty();
                                        } else {
                                            return Observable.from(heartbeatCmd.getCmd());
                                        }
                                    } else {
                                        return Observable.empty();
                                    }
                                })
                                .subscribe(new Subscriber<HeartbeatCmd.CmdEntity>() {
                                    @Override
                                    public void onCompleted() {

                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        Logger.e(e.toString());
                                    }

                                    @Override
                                    public void onNext(HeartbeatCmd.CmdEntity cmdEntity) {
                                        int type = cmdEntity.getType();
                                        if (type == 0) {
                                            getNoticeInfoData();
                                            Log.i(TAG, "call: 更新公告");
                                        } else if (type == 1) {
                                            getTodayDutyData();
                                            Log.i(TAG, "call: 更新今日值日");
                                        } else if (type == 2) {
                                            getWeekProjectData();
                                            Log.i(TAG, "call: 更新本周课程");
                                        } else if (type == 3) {
                                            getTodayDeYuInfoData();
                                            Log.i(TAG, "call: 更新今日德育");
                                        }
                                    }
                                });
                    }
                });
    }

    /**
     * 获取当前温度
     */
    public void getTemperatureInfoData() {
        Log.i(TAG, "getTemperatureInfoData: code = " + User.getLocation((Context) mView));
        Observable.interval(0, 60, TimeUnit.MINUTES)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Long>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Long aLong) {
                        RetrofitService.getCurrentTemprature(User.getLocation((Context) mView))
                                .subscribe(new Subscriber<ProTemperature>() {
                                    @Override
                                    public void onCompleted() {

                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        Logger.e(e.toString());

                                    }

                                    @Override
                                    public void onNext(ProTemperature proTemperature) {
                                        Log.i(TAG, "onNext: proTemperature = " + proTemperature.getStatus());
                                        Log.i(TAG, "onNext: proTemperature = getDesc = " + proTemperature.getDesc());
                                        if (proTemperature.getStatus() == 1000) {
                                            List<ProTemperature.DataEntity.ForecastEntity> forecastEntityList = proTemperature.getData().getForecast();
                                            if (forecastEntityList != null) {
                                                mView.setTemperatureInfo(forecastEntityList, proTemperature.getData().getWendu());
                                            }
                                        }
                                    }
                                });
                    }
                });
    }


    /**
     * 获取班级日程管理信息
     */
    private void getClassScheduleInfoData() {
        RetrofitService.getClassSchedule(classId)
                .flatMap(new Func1<ProClassSchedule, Observable<List<ProClassSchedule.ScheduleEntity>>>() {
                    @Override
                    public Observable<List<ProClassSchedule.ScheduleEntity>> call(ProClassSchedule proClassSchedule) {
                        return Observable.just(proClassSchedule.getSchedule());
                    }
                })
                .subscribe(new Subscriber<List<ProClassSchedule.ScheduleEntity>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Logger.e(e.toString());
                    }

                    @Override
                    public void onNext(List<ProClassSchedule.ScheduleEntity> scheduleEntities) {
                        mView.setClassScheduleData(scheduleEntities);
                    }
                });

    }

    /**
     * 获取本周值日的全部数据
     */
    public void getWeekProjectData() {
        RetrofitService.getProjectInfo(classId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<AllProjectInfo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Logger.e(e.toString());

                    }

                    @Override
                    public void onNext(AllProjectInfo allProjectInfo) {
                        final ProjectInfoDao projectInfoDao = MyApplication.getAppComponent().getDaoSession().getProjectInfoDao();
                        final ProjectTuesdayInfoDao projectTuesdayInfoDao = MyApplication.getAppComponent().getDaoSession().getProjectTuesdayInfoDao();
                        final ProjectWednesdayInfoDao projectWednesdayInfoDao = MyApplication.getAppComponent().getDaoSession().getProjectWednesdayInfoDao();
                        final ProjectTursdayInfoDao projectTursdayInfoDao = MyApplication.getAppComponent().getDaoSession().getProjectTursdayInfoDao();
                        final ProjectFirdayInfoDao projectFirdayInfoDao = MyApplication.getAppComponent().getDaoSession().getProjectFirdayInfoDao();
                        final ProjectSaturdayInfoDao projectSaturdayInfoDao = MyApplication.getAppComponent().getDaoSession().getProjectSaturdayInfoDao();
                        final ProjectSundayInfoDao projectSundayInfoDao = MyApplication.getAppComponent().getDaoSession().getProjectSundayInfoDao();
                        List<AllProjectInfo.NightEntity> moringEntityList = allProjectInfo.getMorning();
                        List<AllProjectInfo.NightEntity> nightEntityList = allProjectInfo.getNight();
                        entityList.clear();
                        for (AllProjectInfo.NightEntity entity : moringEntityList) {
                            entityList.add(entity);
                        }
                        for (AllProjectInfo.NightEntity entity : nightEntityList) {
                            entityList.add(entity);
                        }
                        projectInfoDao.getSession().runInTx(new Runnable() {
                            @Override
                            public void run() {
                                for (int i = 0; i < allProjectInfo.get_$1s().size(); i++) {
                                    if (allProjectInfo.get_$1s().get(i).getNum() == null) {
                                        allProjectInfo.get_$1s().get(i).setNum(i + 1 + "");
                                    }
                                    ProjectInfo mProjectInfo_1 = new ProjectInfo(null
                                            , "1s"
                                            , allProjectInfo.get_$1s().get(i).getNum()
                                            , allProjectInfo.get_$1s().get(i).getCourse()
                                            , allProjectInfo.get_$1s().get(i).getTname()
                                            , allProjectInfo.get_$1s().get(i).getLogo()
                                            , allProjectInfo.get_$1s().get(i).getStart()
                                            , allProjectInfo.get_$1s().get(i).getEnd());
                                    projectInfoDao.insertOrReplace(mProjectInfo_1);
                                }
                            }
                        });
                        projectTuesdayInfoDao.getSession().runInTx(() -> {
                            for (int i = 0; i < allProjectInfo.get_$2s().size(); i++) {
                                if (allProjectInfo.get_$2s().get(i).getNum() == null || "".equals(allProjectInfo.get_$2s().get(i).getNum())) {
                                    allProjectInfo.get_$2s().get(i).setNum(i + 1 + "");
                                }

                                ProjectTuesdayInfo mProjectInfo_2 = new ProjectTuesdayInfo(null
                                        , "2s"
                                        , allProjectInfo.get_$2s().get(i).getNum()
                                        , allProjectInfo.get_$2s().get(i).getCourse()
                                        , allProjectInfo.get_$2s().get(i).getTname()
                                        , allProjectInfo.get_$2s().get(i).getLogo()
                                        , allProjectInfo.get_$2s().get(i).getStart()
                                        , allProjectInfo.get_$2s().get(i).getEnd());
                                projectTuesdayInfoDao.insertOrReplace(mProjectInfo_2);
                            }
                        });
                        projectWednesdayInfoDao.getSession().runInTx(() -> {
                            for (int i = 0; i < allProjectInfo.get_$3s().size(); i++) {
                                if (allProjectInfo.get_$3s().get(i).getNum() == null || "".equals(allProjectInfo.get_$3s().get(i).getNum())) {
                                    allProjectInfo.get_$3s().get(i).setNum(i + 1 + "");
                                }
                                ProjectWednesdayInfo mProjectInfo_3 = new ProjectWednesdayInfo(null
                                        , "3s"
                                        , allProjectInfo.get_$3s().get(i).getNum()
                                        , allProjectInfo.get_$3s().get(i).getCourse()
                                        , allProjectInfo.get_$3s().get(i).getTname()
                                        , allProjectInfo.get_$3s().get(i).getLogo()
                                        , allProjectInfo.get_$3s().get(i).getStart()
                                        , allProjectInfo.get_$3s().get(i).getEnd());
                                projectWednesdayInfoDao.insertOrReplace(mProjectInfo_3);
                            }
                        });

                        projectTursdayInfoDao.getSession().runInTx(() -> {
                            for (int i = 0; i < allProjectInfo.get_$4s().size(); i++) {
                                if (allProjectInfo.get_$4s().get(i).getNum() == null || "".equals(allProjectInfo.get_$4s().get(i).getNum())) {
                                    allProjectInfo.get_$4s().get(i).setNum(i + 1 + "");
                                }
                                ProjectTursdayInfo mProjectInfo_4 = new ProjectTursdayInfo(null
                                        , "4s"
                                        , allProjectInfo.get_$4s().get(i).getNum()
                                        , allProjectInfo.get_$4s().get(i).getCourse()
                                        , allProjectInfo.get_$4s().get(i).getTname()
                                        , allProjectInfo.get_$4s().get(i).getLogo()
                                        , allProjectInfo.get_$4s().get(i).getStart()
                                        , allProjectInfo.get_$4s().get(i).getEnd());
                                projectTursdayInfoDao.insertOrReplace(mProjectInfo_4);
                            }
                        });

                        projectFirdayInfoDao.getSession().runInTx(() -> {
                            for (int i = 0; i < allProjectInfo.get_$5s().size(); i++) {
                                if (allProjectInfo.get_$5s().get(i).getNum() == null || "".equals(allProjectInfo.get_$5s().get(i).getNum())) {
                                    allProjectInfo.get_$5s().get(i).setNum(i + 1 + "");
                                }
                                ProjectFirdayInfo mProjectInfo_5 = new ProjectFirdayInfo(null
                                        , "5s"
                                        , allProjectInfo.get_$5s().get(i).getNum()
                                        , allProjectInfo.get_$5s().get(i).getCourse()
                                        , allProjectInfo.get_$5s().get(i).getTname()
                                        , allProjectInfo.get_$5s().get(i).getLogo()
                                        , allProjectInfo.get_$5s().get(i).getStart()
                                        , allProjectInfo.get_$5s().get(i).getEnd());
                                projectFirdayInfoDao.insertOrReplace(mProjectInfo_5);
                            }
                        });
                        projectSaturdayInfoDao.getSession().runInTx(() -> {

                            for (int i = 0; i < allProjectInfo.get_$6s().size(); i++) {
                                if (allProjectInfo.get_$6s().get(i).getNum() == null || "".equals(allProjectInfo.get_$6s().get(i).getNum())) {
                                    allProjectInfo.get_$6s().get(i).setNum(i + 1 + "");
                                }

                                ProjectSaturdayInfo mProjectInfo_6 = new ProjectSaturdayInfo(null
                                        , "6s"
                                        , allProjectInfo.get_$6s().get(i).getNum()
                                        , allProjectInfo.get_$6s().get(i).getCourse()
                                        , allProjectInfo.get_$6s().get(i).getTname()
                                        , allProjectInfo.get_$6s().get(i).getLogo()
                                        , allProjectInfo.get_$6s().get(i).getStart()
                                        , allProjectInfo.get_$6s().get(i).getEnd());
                                projectSaturdayInfoDao.insertOrReplace(mProjectInfo_6);
                            }
                        });
                        projectSundayInfoDao.getSession().runInTx(() -> {
                            for (int i = 0; i < allProjectInfo.get_$7s().size(); i++) {
                                if (allProjectInfo.get_$7s().get(i).getNum() == null || "".equals(allProjectInfo.get_$7s().get(i).getNum())) {
                                    allProjectInfo.get_$7s().get(i).setNum(i + 1 + "");
                                }
                                ProjectSundayInfo mProjectInfo_7 = new ProjectSundayInfo(null
                                        , "7s"
                                        , allProjectInfo.get_$7s().get(i).getNum()
                                        , allProjectInfo.get_$7s().get(i).getCourse()
                                        , allProjectInfo.get_$7s().get(i).getTname()
                                        , allProjectInfo.get_$7s().get(i).getLogo()
                                        , allProjectInfo.get_$7s().get(i).getStart()
                                        , allProjectInfo.get_$7s().get(i).getEnd());
                                projectSundayInfoDao.insertOrReplace(mProjectInfo_7);
                            }
                        });
                        if (entityList != null && entityList.size() > 0) {
                            if (nightAdapter == null) {
                                nightAdapter = new RecyclerAdapter<AllProjectInfo.NightEntity>(R.layout.night_project_item, entityList) {
                                    @Override
                                    protected void onBindData(RecyclerViewHolder holder, int position, AllProjectInfo.NightEntity item) {
                                        holder.setText(R.id.tv_duty_name, entityList.get(position).getWeek());
                                    }
                                };
                            } else {
                                nightAdapter.setItemDate(entityList);
                            }
                        }
                        mView.loadTodayProjectData(nightAdapter);
                        mView.loadTodayProjectList();
                    }
                });
    }

    /**
     * 获取版本更新数据
     */
    public void getVersionUpdateData() {
        RetrofitService.getVersionUpdateInfo()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<VersionUpdateInfo>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Logger.e(e.toString());

                    }

                    @Override
                    public void onNext(VersionUpdateInfo versionUpdateInfo) {
                        if (versionUpdateInfo.getCode() == 0) {
                            int curVersion = getAppVersionCode((Context) mView);//老
                            int serverVerstion = versionUpdateInfo.getVertion(); //新
                            if (serverVerstion <= curVersion) {
                                return;
                            } else {
                                CustomShowDialog.ShowCustomDialog((Activity) mView, versionUpdateInfo.getContent(), versionUpdateInfo.getPath(), 0);
                            }
                        }
                    }
                });
    }

    /**
     * 获取今日值日数据
     */
    public void getTodayDutyData() {
        RetrofitService.getTodayDutyInfo(classId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<TodayDutyInfo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Logger.e(e.toString());

                    }

                    @Override
                    public void onNext(TodayDutyInfo todayDutyInfo) {
                        if (todayDutyInfo.getCode() == 0) {
                            setAdapter(todayDutyInfo);
                        }
                    }
                });

    }

    /**
     * 设置今日值日的adapter
     *
     * @param todayDutyInfo
     */
    private void setAdapter(TodayDutyInfo todayDutyInfo) {
        if (todayDutyInfo != null && todayDutyInfo.getOnduty().getStus().size() > 0) {
            if (mRecyclerAdapter == null) {
                mRecyclerAdapter = new RecyclerAdapter<StusEntity>(R.layout.today_duty_item, todayDutyInfo.getOnduty().getStus()) {
                    @Override
                    protected void onBindData(RecyclerViewHolder holder, int position, StusEntity item) {
                        holder.setText(R.id.tv_duty_name, item.getSname());
                    }
                };
                mView.loadTodayDutyData(todayDutyInfo, mRecyclerAdapter, true);
            } else {
                mView.loadTodayDutyData(todayDutyInfo, mRecyclerAdapter, false);
            }
        }
    }

    /**
     * 获取老版本  版本号
     *
     * @param context
     * @return
     */
    public static int getAppVersionCode(Context context) {
        try {
            PackageManager pm = context.getPackageManager();
            PackageInfo pi = pm.getPackageInfo(context.getPackageName(), 0);
            return pi.versionCode;

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public void getMoreData() {


    }

    @Override
    public void showClassStatus(Context mainActivity, int checkedId, FragmentManager manager, String mIsBig) {
        FragmentTransaction transaction = manager.beginTransaction();
        if (checkedId == R.id.radio_btn_class_info) {
            if (classInfoFragment == null) {
                classInfoFragment = ClassInfoFragment.newInstance(mIsBig);
            }
            transaction.replace(R.id.fl_class_status_content, classInfoFragment);
        } else if (checkedId == R.id.radio_btn_class_honour) {
            if (classHonourFragment == null) {
                classHonourFragment = new ClassHonourFragment();
            }
            transaction.replace(R.id.fl_class_status_content, classHonourFragment);
        } else if (checkedId == R.id.radio_btn_class_growth_ring) {
            if (classGrowthRingFragment == null) {
                classGrowthRingFragment = new ClassGrowthRingFragment();
            }
            transaction.replace(R.id.fl_class_status_content, classGrowthRingFragment);
        }
        transaction.commitAllowingStateLoss();
    }

    @Override
    public void showStudentMien(Context mainActivity, int checkedId, FragmentManager manager) {
        FragmentTransaction transaction = manager.beginTransaction();
        if (checkedId == R.id.radio_btn_one) {
            if (todayStarFragment == null) {
                todayStarFragment = new TodayStarFragment();
            }
            transaction.replace(R.id.fl_student_mien_content, todayStarFragment);
        } else if (checkedId == R.id.radio_btn_two) {
            if (classRewardFragment == null) {
                classRewardFragment = new ClassRewardFragment();
            }
            transaction.replace(R.id.fl_student_mien_content, classRewardFragment);
        } else if (checkedId == R.id.radio_btn_three) {
            if (schoolRewardFragment == null) {
                schoolRewardFragment = new SchoolRewardFragment();
            }

            transaction.replace(R.id.fl_student_mien_content, schoolRewardFragment);
        } else if (checkedId == R.id.radio_btn_four) {
            if (myDreamFragment == null) {
                myDreamFragment = new MyDreamFragment();
            }
            transaction.replace(R.id.fl_student_mien_content, myDreamFragment);
        } else if (checkedId == R.id.radio_btn_five) {
            if (encourageTalkFragment == null) {
                encourageTalkFragment = new EncourageTalkFragment();
            }

            transaction.replace(R.id.fl_student_mien_content, encourageTalkFragment);
        }
        transaction.commitAllowingStateLoss();
        mView.setStudentMien(checkedId);
    }
}
