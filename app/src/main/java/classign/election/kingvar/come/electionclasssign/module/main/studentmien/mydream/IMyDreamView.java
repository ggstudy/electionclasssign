package classign.election.kingvar.come.electionclasssign.module.main.studentmien.mydream;

import classign.election.kingvar.come.electionclasssign.api.info.ProMyDream;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/5/16 0016 11:12
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public interface IMyDreamView {
    void loadData(ProMyDream.DreamBean dreamBean);
}
