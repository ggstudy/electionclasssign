package classign.election.kingvar.come.electionclasssign.api.info;

import java.util.List;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/4/1 0001 16:50
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public class EncourageTalkInfo {

    /**
     * code : 0
     * encourage : [{"studentid":"24","date":1483372800000,"name":"李老师","logo":"20170104102841340801.jpg","id":1,"detail":"三八红旗手"},{"studentid":"24","date":1483372800000,"name":"李老师","logo":"20170104102841340801.jpg","id":2,"detail":"班主任小助手"},{"studentid":"24","date":1483372800000,"name":"李老师","logo":"20170104102841340801.jpg","id":3,"detail":"非常棒"}]
     */

    private int code;
    private List<EncourageBean> encourage;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<EncourageBean> getEncourage() {
        return encourage;
    }

    public void setEncourage(List<EncourageBean> encourage) {
        this.encourage = encourage;
    }

    public static class EncourageBean{
        /**
         * studentid : 24
         * date : 1483372800000
         * name : 李老师
         * logo : 20170104102841340801.jpg
         * id : 1
         * detail : 三八红旗手
         */

        private String studentid;
        private long date;
        private String name;
        private String logo;
        private int id;
        private String detail;

        public String getStudentid() {
            return studentid;
        }

        public void setStudentid(String studentid) {
            this.studentid = studentid;
        }

        public long getDate() {
            return date;
        }

        public void setDate(long date) {
            this.date = date;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getDetail() {
            return detail;
        }

        public void setDetail(String detail) {
            this.detail = detail;
        }
    }
}
