package classign.election.kingvar.come.electionclasssign.activity;

import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import classign.election.kingvar.come.electionclasssign.R;
import classign.election.kingvar.come.electionclasssign.module.base.BaseActivity;
import classign.election.kingvar.come.electionclasssign.api.info.ProClassAndSchoolNotice;
import classign.election.kingvar.come.electionclasssign.tools.utils.DateUtil;

/**
 * @author 鞠永强
 * @version $Rev$
 * @time 2017/3/24 0024 14:08
 * @des ${TODO}
 * @updateAuthor $Author$
 * @updateDate $Date$
 * @updateDes ${TODO}
 */

public class ScholNoticeDetailActivity extends BaseActivity implements View.OnClickListener {
    private static final String TAG = "ScholNoticeDetailActivi";
    @BindView(R.id.show_wv)
    WebView mShowWebView;
    @BindView(R.id.tv_notice_title)
    TextView mTvNoticeTitle;
    @BindView(R.id.iv_close_btn)
    ImageView mIvCloseBtn;
    @BindView(R.id.loading_main)
    LinearLayout mLoadingMain;
    @BindView(R.id.tv_time)
    TextView mTvTime;
    @BindView(R.id.tv_notice_type)
    TextView mTvNoticeType;

    private String type;
    private ArrayList<ProClassAndSchoolNotice.SchoolInfoEntity> schoolInfoEntityList;
    private ArrayList<ProClassAndSchoolNotice.ClsNoticeEntity> clsNoticeEntityList;

    @Override
    protected int attachLayoutRes() {
        return R.layout.activity_school_notice_detail;
    }

    @Override
    protected void initInjector() {
        showLoading();
        mLoadingMain.setVisibility(View.VISIBLE);
    }

    @Override
    protected void initViews() {
        DisplayMetrics metric = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metric);
        int width = metric.widthPixels; // 屏幕宽度（像素）
        float density = metric.density;
        Log.i(TAG, "initData: density" + density);

        String setting = "<head><style>img{width:" + width / density + " !important;}</style></head>";

        WebSettings settings = mShowWebView.getSettings();
        settings.setDefaultTextEncodingName("UTF-8");
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        settings.setBuiltInZoomControls(true);//在可缩放页面，支持放大缩小功能
        settings.setSupportZoom(true);
        settings.setDisplayZoomControls(false);
        settings.setDomStorageEnabled(true);
        mShowWebView.setBackgroundColor(0);
        mShowWebView.getBackground().setAlpha(0);
        type = getIntent().getStringExtra("type");
        int position = getIntent().getIntExtra("position", 0);
        schoolInfoEntityList = getIntent().getParcelableArrayListExtra("schoolInfoEntityList");
        clsNoticeEntityList = getIntent().getParcelableArrayListExtra("clsNoticeEntityList");
        if ("1".equals(type) && schoolInfoEntityList != null) {
            mTvNoticeType.setText("班级公告");
            if (schoolInfoEntityList.get(position % schoolInfoEntityList.size()).getTitle().length() > 35) {

                mTvNoticeTitle.setText(">> " + schoolInfoEntityList.get(position % schoolInfoEntityList.size()).getTitle().substring(0, 35)+"...");
            } else {
                mTvNoticeTitle.setText(">> " + schoolInfoEntityList.get(position % schoolInfoEntityList.size()).getTitle());

            }
            mTvTime.setText("[" + DateUtil.getMonthByTimeStamp(schoolInfoEntityList.get(position % schoolInfoEntityList.size()).getTime()) + "/"
                    + DateUtil.getDayByTimeStamp(schoolInfoEntityList.get(position % schoolInfoEntityList.size()).getTime()) + "]");
            mShowWebView.loadDataWithBaseURL(null, schoolInfoEntityList.get(position % schoolInfoEntityList.size()).getDetail(), "text/html", "utf-8", null);
        } else if ("2".equals(type) && clsNoticeEntityList != null) {
            mTvNoticeType.setText("校园公告");
            if (clsNoticeEntityList.get(position % clsNoticeEntityList.size()).getTitle().length() > 35) {

                mTvNoticeTitle.setText(">> " + clsNoticeEntityList.get(position % clsNoticeEntityList.size()).getTitle().substring(0, 35)+"...");
            } else {
                mTvNoticeTitle.setText(">> " + clsNoticeEntityList.get(position % clsNoticeEntityList.size()).getTitle());

            }
            mTvTime.setText("[" + DateUtil.getMonthByTimeStamp(clsNoticeEntityList.get(position % clsNoticeEntityList.size()).getTime()) + "/"
                    + DateUtil.getDayByTimeStamp(clsNoticeEntityList.get(position % clsNoticeEntityList.size()).getTime()) + "]");
            mShowWebView.loadDataWithBaseURL(null, clsNoticeEntityList.get(position % clsNoticeEntityList.size()).getDetail(), "text/html", "utf-8", null);
        }


    }

    @Override
    protected void updateViews(boolean isRefresh) {
        mIvCloseBtn.setOnClickListener(this);
        mShowWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                mLoadingMain.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onClick(View v) {
        finish();
    }
}
