package classign.election.kingvar.come.electionclasssign.module.main.classmien.classgrowth;

import android.support.v4.view.ViewPager;

import java.util.List;

import butterknife.BindView;
import classign.election.kingvar.come.electionclasssign.R;
import classign.election.kingvar.come.electionclasssign.adapter.ClassGrowthRingPagerAdapter;
import classign.election.kingvar.come.electionclasssign.api.info.ClassGrowthRingInfo;
import classign.election.kingvar.come.electionclasssign.injector.components.DaggerClassGrowthRingComponent;
import classign.election.kingvar.come.electionclasssign.injector.modules.ClassGrowthRingModule;
import classign.election.kingvar.come.electionclasssign.module.base.BaseFragment;
import classign.election.kingvar.come.electionclasssign.module.base.IBasePresenter;
import classign.election.kingvar.come.electionclasssign.tools.bean.User;

/**
 * 班级年轮fragment
 * Created by Administrator on 2016/12/21 0021.
 */

public class ClassGrowthRingFragment extends BaseFragment<IBasePresenter> implements IClassGrowthRingView {
    private static final String TAG = "ClassGrowthRingFragment";
    @BindView(R.id.view_pager_class_growth_ring)
    ViewPager view_pager_class_growth_ring;
    private ClassGrowthRingPagerAdapter classGrowthRingPagerAdapter;

    @Override
    protected int attachLayoutRes() {
        return R.layout.fragment_class_growth_ring;
    }

    @Override
    protected void initInjector() {
        DaggerClassGrowthRingComponent.builder()
                .applicationComponent(getAppComponent())
                .classGrowthRingModule(new ClassGrowthRingModule(this, User.getClassId(mContext)))
                .build()
                .inject(this);
    }

    @Override
    protected void initViews() {
        setUserVisibleHint(true);
    }

    @Override
    public void updateViews(boolean isRefresh) {
//        mPresenter.getData(isRefresh);
    }

    @Override
    public void onStart() {
        super.onStart();
        initInjector();
    }

    @Override
    public void loadClassGrowthRingData(final List<ClassGrowthRingInfo.ClsringEntity> clsringEntityList) {
        if (clsringEntityList!=null){

        if (classGrowthRingPagerAdapter == null) {
            classGrowthRingPagerAdapter = new ClassGrowthRingPagerAdapter(getFragmentManager(), clsringEntityList);
            view_pager_class_growth_ring.setAdapter(classGrowthRingPagerAdapter);
        } else {
            classGrowthRingPagerAdapter.setData(clsringEntityList);
        }
        }

    }
}
